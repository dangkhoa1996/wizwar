#version 330 core

layout (triangles) in;
layout (line_strip, max_vertices = 6) out;

layout (std140) uniform pv_mat {
  mat4 projection;
  mat4 view;
};

in vec2 texcoord[];
uniform sampler2D u_bump_tex;

uniform mat4 u_model;

void main()
{
    int n;
    vec3 normal;
    vec4 world_pos = vec4(1.0);
    for (n = 0; n < gl_in.length(); n++) {
      normal = texture(u_bump_tex, texcoord[n]).rgb * 2.0 - 1.0;
      world_pos = u_model * gl_in[n].gl_Position;
      gl_Position = projection * view * world_pos;
      EmitVertex();
      gl_Position = projection * view * vec4((world_pos.xyz + normal), 1.0);
      EmitVertex();
      EndPrimitive(); 
    }
}
