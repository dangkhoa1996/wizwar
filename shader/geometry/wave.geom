#version 330 core

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

#define PI 3.1415926538

in vec2 texcoord[];

out vec3 f_normal;
out vec3 f_tangent;
out vec3 f_bitangent;
out vec2 f_texcoord;
out vec3 f_frag_position;
out vec4 f_clipspace_pos;

layout (std140) uniform pv_mat {
    mat4 projection;
    mat4 view;
};

const int MAX_WAVES = 3;
const float TWO_PI = 2.0 * PI;
const float OO_TWO_PI = 1.0 / TWO_PI;

uniform mat4 u_model;

struct Wave {
    float freq;
    float phase;
    float amp;
    vec2 dir;
};

uniform Wave u_waves[MAX_WAVES];
uniform float u_steepness;

vec3 get_gerstner_wave(vec3 pos) {
    float x = 0.0;
    float y = 0.0;
    float z = 0.0;

    float bitangent_x = 0.0;
    float bitangent_y = 0.0;
    float bitangent_z = 0.0;

    float tangent_x = 0.0;
    float tangent_y = 0.0;
    float tangent_z = 0.0;

    float normal_x = 0.0;
    float normal_y = 0.0;
    float normal_z = 0.0;

    float Q = 0.0;
    float inner_term = 0.0; 
    float sin_term = 0.0;
    float cos_term = 0.0;

    for (int i = 0; i < MAX_WAVES; i++) {
        Wave wave = u_waves[i];
        inner_term = dot(pos.xz, wave.freq * wave.dir) + wave.phase;
        sin_term = sin(inner_term);
        cos_term = cos(inner_term);

        Q = u_steepness / (wave.freq * wave.amp * MAX_WAVES);

        // x += Q * wave.amp * wave.dir.x * cos_term;
        x += wave.amp * wave.dir.x *cos_term;
        y += wave.amp * sin_term;
        // z += Q * wave.amp * wave.dir.y * cos_term;
        z += wave.amp * wave.dir.y * cos_term;


        bitangent_x += u_steepness * wave.dir.x * wave.dir.x * sin_term / MAX_WAVES;
        bitangent_y += wave.dir.x * wave.freq * wave.amp * cos_term;
        bitangent_z -= u_steepness * wave.dir.x * wave.dir.y * sin_term / MAX_WAVES;

        tangent_x -= u_steepness * wave.dir.x * wave.dir.y * sin_term / MAX_WAVES;
        tangent_y += wave.dir.y * wave.freq * wave.amp * cos_term;
        tangent_z += u_steepness * wave.dir.y * wave.dir.y * sin_term / MAX_WAVES;

        normal_x -= wave.dir.x * wave.freq * wave.amp * cos_term;
        normal_y += u_steepness  * sin_term / MAX_WAVES;
        normal_z -= wave.dir.y * wave.freq * wave.amp * cos_term;
    }

    f_bitangent = vec3(1.0 - bitangent_x, bitangent_y, bitangent_z);
    f_tangent = vec3(tangent_x, tangent_y, 1.0 - tangent_z);
    f_normal = vec3(normal_x, 1.0 - normal_y , normal_z); // correcting for positive result 

    return vec3(pos.x + x, y, pos.z + z);
}

void main()
{
    int n;
    vec3 normal;
    // for only one wave
    vec4 world_pos = vec4(1.0);
    for (n = 0; n < gl_in.length(); n++) {
        // vec3 vertex_pos = vec3(gl_Position.x, get_wave_height(), gl_Position.z);
        world_pos = u_model * vec4(get_gerstner_wave(gl_in[n].gl_Position.xyz), 1.0);
        // world_pos = u_model * vec4(gl_in[n].gl_Position.xyz, 1.0);
        f_frag_position = world_pos.xyz;
        gl_Position = projection * view * world_pos;
        f_clipspace_pos = gl_Position;
        // get_gerstner_wave(world_pos.xyz);
        // gl_Position = projection * view * world_pos;
        f_texcoord = texcoord[n]; // look ok
        EmitVertex();
    }
    EndPrimitive();
}