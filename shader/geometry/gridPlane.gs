#version 330 core
layout (points) in;
layout (line_strip, max_vertices = 10) out;
in vec3 vfragPos[];
out vec3 fragPos;
uniform float size;
void main()
{
    // vec4 origin = gl_in[0].gl_Position;
    // float halfSize = size / 2;
    // vec4 startPoint = origin + vec4(halfSize, 0.0, -halfSize, 0.0);
    // gl_Position = startPoint;
    // EmitVertex();
    // float st = size / 100;
    // vec4 endPoint = origin + vec4(halfSize, 0.0, halfSize, 0.0);
    // gl_Position = endPoint;
    // EmitVertex();
    // EndPrimitive();
    fragPos = vfragPos[0];
}