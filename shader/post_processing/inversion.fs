#version 330
in vec2 texCoords;
out vec4 fragColor;

uniform sampler2D screenTexture;

void main()
{
    fragColor = vec4(vec3(1 - texture(screenTexture, texCoords)), 1.0);
}