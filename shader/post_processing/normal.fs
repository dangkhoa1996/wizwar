#version 330
in vec2 texCoords;
out vec4 fragColor;

uniform sampler2D screenTexture;

void main()
{
    float depthValue = texture(screenTexture, texCoords).r;
    fragColor = vec4(vec3(depthValue), 1.0);
}