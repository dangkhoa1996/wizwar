#version 330
in vec2 texCoords;
out vec4 fragColor;

uniform sampler2D screenTexture;

void main()
{
    fragColor = texture(screenTexture, texCoords);
    float average = 0.2126 * fragColor.r + 0.7152 * fragColor.g + 0.0722 * fragColor.b;
    fragColor = vec4(average, average, average, 1.0);
}