#version 330 core
out vec4 frag_color;

in vec2 f_texcoord;
in vec3 f_normal;
in vec3 f_fragpos;

uniform vec3 u_camera_position;
uniform vec4 m_clip_plane;
uniform sampler2D u_texture;

void main()
{
    frag_color = vec4(texture(u_texture, f_texcoord).rgb, 1.0);
}
