#version 330 core
uniform sampler2D u_texture;
uniform vec3 u_color;

in vec2 f_texcoord;
out vec4 out_color;

void main() {
  out_color = texture(u_texture, f_texcoord);
}