#version 330 core
struct DirLight
{
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct PointLight
{
    vec3 position;

    float constant;
    float linear;
    float quadratic;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};
#define NR_POINT_LIGHTS 4

struct Light
{
    vec3 direction;

    vec3 position;
    float cutOff;
    float outerCutOff;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
};

struct Material
{
    sampler2D diffuse;
    sampler2D specular;
    sampler2D emission;
    float shininess;
};

uniform Material material;
uniform Light light;
uniform DirLight dirLight;
uniform PointLight pointLights[NR_POINT_LIGHTS];

in vec3 fragPos;
in vec3 normal;
in vec2 texCoord;
out vec4 fragColor;

uniform vec3 viewPos;
uniform vec3 lightColor;
uniform vec3 objectColor;

uniform sampler2D textureSampler;
uniform mat4 view;

vec3 calcDirLight(DirLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(-light.direction);

    float diff = max(dot(normal, lightDir), 0.0);
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);

    vec3 ambient = light.ambient * vec3(texture(material.diffuse, texCoord));
    vec3 diffuse = light.diffuse * diff * vec3(texture(material.diffuse, texCoord));
    vec3 specular = light.specular * spec * vec3(texture(material.specular, texCoord));
    return (ambient + diffuse + specular);
}

vec3 calcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(light.position - fragPos);

    float diff = max(dot(normal, lightDir), 0.0);
    vec3 reflectDir = reflect(-lightDir, normal);
    
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);

    float d = length(light.position - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * d + light.quadratic * d * d);

    vec3 ambient = light.ambient * vec3(texture(material.diffuse, texCoord));
    vec3 diffuse = light.diffuse * diff * vec3(texture(material.diffuse, texCoord));
    vec3 specular = light.specular * spec * vec3(texture(material.specular, texCoord));

    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;

    return ambient + diffuse + specular;
}

vec3 calcSpotLight(Light light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(light.position - fragPos);

    float diff = max(dot(normal, lightDir), 0.0);
    vec3 reflectDir = reflect(-lightDir, normal);

    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);

    float d = length(light.position - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * d + light.quadratic * d * d);

    float theta = dot(lightDir, normalize(-light.direction));
    float epsilion = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilion, 0.0, 1.0);

    vec3 ambient = light.ambient * vec3(texture(material.diffuse, texCoord));
    vec3 diffuse = light.diffuse * diff * vec3(texture(material.diffuse, texCoord));
    vec3 specular = light.specular * spec * vec3(texture(material.specular, texCoord));

    ambient *= attenuation * intensity;
    diffuse *= attenuation * intensity;
    specular *= attenuation * intensity;

    return ambient + diffuse + specular;
}

// float near = 0.1;
// float far = 100.0;

// float linearizeDepth(float depth)
// {
//     float z = depth * 2 - 1.0;
//     return (2.0 * near * far) / (far + near - z * (far - near));
// }

void main()
{   
    // vec3 norm = normalize(normal);
    // vec3 viewDir = normalize(viewPos - fragPos);

    // vec3 result = calcDirLight(dirLight, norm, fragPos, viewDir);

    // for(int i = 0; i < NR_POINT_LIGHTS; i++)
    // {
    //     result += calcPointLight(pointLights[i], norm, fragPos, viewDir);
    // }
    // result += calcSpotLight(light, norm, fragPos, viewDir);

    // fragColor = vec4(vec3(linearizeDepth(gl_FragCoord.z) / far), 1.0);
    if (normal.x > 10) {
        fragColor = texture(textureSampler, texCoord);
    } else {
        fragColor = vec4(1.0, 0.0, 0.0, 1.0);
    }

    // if(gl_FragCoord.x < 400)
    // {
    //     fragColor = vec4(1.0, 0.0, 0.0, 1.0);
    // }
    // else 
    // {
    //     fragColor = vec4(0.45, 0.25, 0.78, 1.0);
    // }

}
