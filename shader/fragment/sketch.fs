#version 330 core
out vec4 fragColor;

in vec2 texCoords;
in vec3 normal;
in vec3 fragPos;

uniform sampler2D samplerTexture;

uniform vec3 lightPos;
uniform vec3 viewPos;
void main()
{
    vec3 color = texture(samplerTexture, texCoords).rgb;
    vec3 norm = normalize(normal);
    vec3 lightColor = vec3(0.6);
   
    vec3 ambient = 0.3 * color;
    vec3 lightDir = normalize(lightPos - fragPos);
    
    // diffuse
    float diff = max(dot(lightDir, norm), 0.0);
    vec3 diffuse = diff * lightColor;

    // specular
    vec3 viewDir = normalize(viewPos - fragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = 0.0;

    vec3 halfwayDir = normalize(lightDir + viewDir);
    spec = pow(max(dot(norm, halfwayDir), 0.0), 64.0);
    vec3 specular = spec * lightColor;

    vec3 lighting = (ambient + diffuse + specular) * color;

    fragColor = vec4(lighting, 1.0);
    
}