#version 330 core
out vec4 fragcolor;

in vec2 f_texcoord;
in vec3 f_normal;
in vec3 f_fragpos;

uniform sampler2D texture_ambient0;
uniform sampler2D texture_diffuse0;
uniform sampler2D texture_specular0;

// uniform vec3 ambientColor;
// uniform vec3 diffuseColor;
// uniform vec3 specularColor;
// uniform vec3 emissionColor;
// uniform float shininess;

void main()
{
    vec3 diffuse = texture(texture_diffuse0, f_texcoord).rgb;
    vec3 specular = texture(texture_specular0, f_texcoord).rgb;
    vec3 ambient = texture(texture_ambient0, f_texcoord).rgb;
    vec3 result = diffuse;
    fragcolor = vec4(result, 1.0);
}
