#version 330 core
out vec4 fragColor;

in VS_OUT {
    vec2 texCoords;
    vec3 tangentLightPos;
    vec3 tangentViewPos;
    vec3 tangentFragPos;
} fs_in;

uniform sampler2D normalMap;
uniform sampler2D diffuseTexture;
void main()
{
    vec3 color = texture(diffuseTexture, fs_in.texCoords).rgb;
    
    vec3 normal = texture(normalMap, fs_in.texCoords).rgb;
    normal = normalize(normal * 2.0 - 1.0);

    vec3 lightColor = vec3(0.5);
    vec3 ambient = 0.1 * color;

    vec3 lightDir = normalize(fs_in.tangentLightPos - fs_in.tangentFragPos);

    float diff = max(dot(lightDir, normal), 0.0);
    vec3 diffuse = diff * lightColor;

    vec3 viewDir = normalize(fs_in.tangentViewPos - fs_in.tangentFragPos);
    float spec = 0.0;

    vec3 halfwayDir = normalize(lightDir + viewDir);
    spec = pow(max(dot(normal, halfwayDir), 0.0), 64.0);
    vec3 specular = spec * lightColor;

    vec3 lighting = (ambient + diffuse + specular) * color;

    fragColor = vec4(lighting, 1.0);

    // fragColor = texture(diffuseTexture, fs_in.texCoords);
}