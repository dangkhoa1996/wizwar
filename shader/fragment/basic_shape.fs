#version 330 core

in vec3 f_normal;
in vec2 f_texcoord;
in vec3 f_color;
in vec3 f_frag_position;

out vec4 fragcolor;

// uniform sampler2D texture_data;
uniform vec3 u_camera_position;
// shadowing
// uniform sampler2DShadow depth_texture;

in VS_FS_INTERFACE {
    // vec4 shadow_coord;
    vec3 world_coord;
    vec3 eye_coord;
    vec3 normal;
} fragment;


struct DirectionalLight {
    vec3 ambient;
    vec3 color;
    vec3 direction;
    bool is_enabled;
};

struct PointLight {
    vec3 color;
    vec3 position;
    vec3 ambient;
    float constant_attenuation;
    float linear_attenuation;
    float quadratic_attenuation;
    bool is_enabled;
};

struct SpotLight {
    vec3 ambient;
    vec3 color;
    vec3 position;
    vec3 cone_direction;
    float spot_cos_cut_off;
    float spot_exponent;
    float constant_attenuation;
    float linear_attenuation;
    float quadratic_attenuation;
    bool is_enabled;
};

struct NumericalMaterial {
    vec3 emission;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
    float specular_strength; // configurable
};

struct TextureMaterial {
    sampler2D emission;
    sampler2D diffuse;
    sampler2D specular;
    sampler2D shininess;
    float specular_strength; // configurable
};

// there will be two kinds of material
// numerical one and texture based one


const int MAX_LIGHTS = 10;

uniform int directional_lights_size;
uniform DirectionalLight u_directional_lights[MAX_LIGHTS];

uniform int point_lights_size;
uniform PointLight u_point_lights[MAX_LIGHTS];

uniform int spot_lights_size;
uniform SpotLight u_spot_lights[MAX_LIGHTS];

uniform NumericalMaterial u_numerical_material;
uniform TextureMaterial u_texture_material;

uniform int u_mat_index;

uniform sampler2D u_tex;
uniform bool has_texture;
uniform bool has_light;
uniform vec4 u_clip_space;

vec3 get_scattered_light(vec3 light_ambient, vec3 light_color, float attenuation, float diffuse)
{
    // vec3 ambient_color = vec3(0.0);
    vec3 diffuse_color = vec3(0.0);
    
    // ambient_color = light_ambient * u_numerical_material.diffuse;
    diffuse_color = light_color * diffuse * 0.8 * u_numerical_material.diffuse;

    // ambient_color = light_ambient * f_color;
    // diffuse_color = light_color * diffuse * 0.8 * f_color;
    return light_ambient + diffuse_color;
}

vec3 get_reflected_light(vec3 light_direction, vec3 light_color, float attenuation, float diffuse)
{
    vec3 view_direction = normalize(u_camera_position - f_frag_position);
    vec3 half_vector = normalize(-light_direction + view_direction);
    float specular = max(0.0, dot(f_normal, half_vector));
    
    if (diffuse == 0.0) {
        specular = 0.0;
    } else {
        // specular = pow(specular, u_numerical_material.shininess) * u_numerical_material.specular_strength;
        specular = pow(specular, u_numerical_material.shininess);
    }

    return light_color * specular * attenuation * u_numerical_material.specular;
}

float get_spot_light_attenuation(vec3 light_direction, float light_distance, SpotLight light)
{
    float attenuation = 1.0 /  ( light.constant_attenuation + 
                        light.linear_attenuation * light_distance + 
                        light.quadratic_attenuation * light_distance * light_distance);

    float spot_cos = dot(light_direction, 
                    normalize(-light.cone_direction));

    if (spot_cos > light.spot_cos_cut_off) {
        attenuation = 0.0;
    } else {
        attenuation *= pow(spot_cos, light.spot_exponent);
    }
    
    return attenuation;
}

float get_point_light_attenuation(vec3 light_direction, float light_distance, PointLight light)
{
    return 1.0 /  ( light.constant_attenuation + 
                    light.linear_attenuation * light_distance + 
                    light.quadratic_attenuation * light_distance * light_distance );
}

vec4 get_final_color(vec3 light_direction, vec3 light_color, vec3 light_ambient, float attenuation, float diffuse) 
{
    vec3 scattered_light = get_scattered_light(light_ambient, light_color, attenuation, diffuse);
    vec3 reflected_light = get_reflected_light(light_direction, light_color, attenuation, diffuse);

    // float shadow = textureProj(depth_texture, fragment.shadow_coord);
    // vec3 rgb = min(shadow * f_color.rgb * scattered_light + shadow * reflected_light, vec3(1.0));
    vec3 rgb = f_color.rgb * scattered_light + reflected_light;

    return vec4(rgb, 1.0);
}

vec4 calc_directional_light(DirectionalLight light)
{
    float diffuse_scalar = max(0.0, dot(normalize(f_normal), normalize(-light.direction)));
    return get_final_color(light.direction, light.color, light.ambient, 1.0, diffuse_scalar);;
}

vec4 calc_point_light(PointLight light)
{
    vec3 light_direction = light.position - f_frag_position;
    float light_distance = length(light_direction);
    light_direction = light_direction / light_distance;
    
    float attenuation = get_point_light_attenuation(light_direction, light_distance, light);
    float diffuse = max(0.0, dot(f_normal, normalize(light_direction)));
    return get_final_color(light_direction, light.color, light.ambient, attenuation, diffuse);
}

vec4 calc_spot_light(SpotLight light)
{
    vec3 light_direction = light.position - f_frag_position;
    float light_distance = length(light_direction);
    light_direction = light_direction / light_distance;

    float attenuation = get_spot_light_attenuation(light_direction, light_distance, light);
    float diffuse = max(0.0, dot(f_normal, light.cone_direction));
    return get_final_color(light_direction, light.color, light.ambient, attenuation, diffuse);
}

void main()
{
    vec4 final_color = vec4(0.0, 0.0, 0.0, 1.0);

    for (int i = 0; i < directional_lights_size; i++) {
        final_color += calc_directional_light(u_directional_lights[i]);
    }

    if (point_lights_size != 0) {
        for (int i = 0; i < point_lights_size; i++) {
            final_color += calc_point_light(u_point_lights[i]);
        }
    }
    
    if (spot_lights_size != 0) {
        for (int i = 0; i < spot_lights_size; i++) {
            final_color += calc_spot_light(u_spot_lights[i]);
        }
    }
    
    fragcolor = final_color;
    // fragcolor = vec4(f_color, 1.0);
}
