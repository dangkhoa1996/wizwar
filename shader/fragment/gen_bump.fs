#version 330 core

// uniform sampler2D u_bias_noise_tex;
out vec4 color;

// in vec2 f_texcoord;
// in vec3 f_bitangent;
// in vec3 f_tangent;
in vec3 f_normal;

void main()
{
  vec3 rgb_normal = f_normal * 0.5 + 0.5;
  color = vec4(rgb_normal, 1.0);
}