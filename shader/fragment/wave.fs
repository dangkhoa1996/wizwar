#version 330 core

out vec4 frag_color;

in vec2 f_texcoord;
in vec3 f_frag_position;
in vec4 f_clipspace_pos;

in vec3 f_bitangent;
in vec3 f_tangent;
in vec3 f_normal;

uniform sampler2D u_bump_tex;
uniform sampler2D u_env_map_tex;
uniform sampler2D u_refract_tex;
uniform sampler2D u_reflect_tex;

// in VS_FS_INTERFACE {
//     // vec4 shadow_coord;
//     vec3 world_coord;
//     vec3 eye_coord;
//     vec3 normal;
// } fragment;

uniform vec3 u_camera_position;

struct DirectionalLight {
    vec3 ambient;
    vec3 color;
    vec3 direction;
    bool is_enabled;
};

struct NumericalMaterial {
    vec3 emission;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
    float specular_strength; // configurable
};

const int MAX_LIGHTS = 10;
uniform int directional_lights_size;
uniform DirectionalLight u_directional_lights[MAX_LIGHTS];

uniform DirectionalLight u_directional_light;
uniform NumericalMaterial u_numerical_material;

vec3 normal;

vec3 to_surface_space(vec3 input_vec)
{
    return normalize(vec3(dot(f_tangent, input_vec), dot(f_bitangent, input_vec), dot(f_normal, input_vec)));
}

vec3 get_scattered_light(vec3 light_ambient, vec3 light_color, float attenuation, float diffuse_scalar)
{
    vec3 diffuse_color =  diffuse_scalar * attenuation * light_color * u_numerical_material.diffuse;
    return light_ambient + diffuse_color;
}

vec3 get_reflected_light(vec3 light_direction, vec3 light_color, float attenuation, float diffuse_scalar)
{
    vec3 view_direction = to_surface_space(normalize(u_camera_position - f_frag_position));

    vec3 half_vector = normalize(light_direction + view_direction);
    float specular = max(0.0, dot(normal, half_vector));
    
    if (diffuse_scalar == 0.0) {
        specular = 0.0;
    } else {
        specular = pow(specular, u_numerical_material.shininess) * u_numerical_material.specular_strength;
        // specular = pow(specular, u_numerical_material.shininess);
    }

    return light_color * specular * attenuation;
}

vec4 get_final_color(vec3 light_direction, vec3 light_color, vec3 light_ambient, float attenuation, float diffuse_scalar) 
{
    vec3 scattered_light = get_scattered_light(light_ambient, light_color, attenuation, diffuse_scalar);
    vec3 reflected_light = get_reflected_light(light_direction, light_color, attenuation, diffuse_scalar);
    // float shadow = textureProj(depth_texture, fragment.shadow_coord);
    // vec3 rgb = min(shadow * f_color.rgb * scattered_light + shadow * reflected_light, vec3(1.0));
    vec3 rgb = scattered_light + reflected_light;
    //  vec3 rgb = min(scattered_light, vec3(1.0));

    return vec4(rgb, 1.0);
}

vec4 calc_directional_light(DirectionalLight light)
{
    const float ATTENUATION = 1.0;
    vec3 light_dir = to_surface_space(-light.direction);
    // vec3 light_dir = -light.direction;
    float diffuse_scalar = max(0.0, dot(normal, light_dir));
    // float diffuse_scalar = max(0.0, dot(f_normal, light_dir));

    return get_final_color(light_dir, light.color, light.ambient, ATTENUATION, diffuse_scalar);
}

void main()
{
    // normal = vec3(texture(u_bump_tex, f_texcoord.xy).rgb);
    // normal = to_surface_space(vec3(normal.x * 2.0 - 1.0, normal.y, normal.z * 2.0 - 1.0));
    // vec2 ndc = f_texcoord;
    // vec2 ndc = (f_clipspace_pos.xy / f_clipspace_pos.w) * 0.5 + 0.5;
    
    vec2 texsize = textureSize(u_refract_tex, 0).xy;
    vec2 new_texcoord = gl_FragCoord.xy / texsize;

    normal = to_surface_space(f_normal);
    // vec4 refract_color = texture(u_refract_tex, ndc + 0.2 * normal.xz);
    vec4 refract_color = texture(u_refract_tex, new_texcoord + normal.xz);
    // vec4 reflect_color = texture(u_reflect_tex, vec2(ndc.x, -ndc.y));
    vec4 reflect_color = texture(u_reflect_tex, vec2(new_texcoord.x, 1.0 - new_texcoord.y) + normal.xz);
    vec4 final_color = vec4(0.0);
    for (int i = 0; i < directional_lights_size; i++) {
        final_color += calc_directional_light(u_directional_lights[i]);
    }
    frag_color = vec4(final_color.rgb, 1.0) * mix(refract_color, reflect_color, 0.5);
    // frag_color = mix(refract_color, reflect_color, 0.5);
    // frag_color = refract_color;
}