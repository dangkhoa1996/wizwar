#version 330 core

in vec3 f_normal;
in vec2 f_texcoord;
in vec3 f_tangent;
in vec3 f_bitangent;
in vec3 f_fragpos;
// in vec4 f_shadow_coord;

out vec4 fragcolor;

uniform vec3 color_diffuse;
uniform vec3 color_specular;

// const int MAX_TEXTURE = 32;
uniform sampler2D u_texture;

uniform vec3 u_camera_position;

struct DirectionalLight {
    vec3 ambient;
    vec3 color;
    vec3 direction;
    bool is_enabled;
};

struct PointLight {
    vec3 color;
    vec3 position;
    vec3 ambient;
    float constant_attenuation;
    float linear_attenuation;
    float quadratic_attenuation;
    bool is_enabled;
};

struct SpotLight {
    vec3 ambient;
    vec3 color;
    vec3 position;
    vec3 cone_direction;
    float spot_cos_cut_off;
    float spot_exponent;
    float constant_attenuation;
    float linear_attenuation;
    float quadratic_attenuation;
    bool is_enabled;
};

const int MAX_LIGHTS = 10;

uniform int directional_lights_size;
uniform DirectionalLight u_directional_lights[MAX_LIGHTS];

uniform int point_lights_size;
uniform PointLight u_point_lights[MAX_LIGHTS];

uniform int spot_lights_size;
uniform SpotLight u_spot_lights[MAX_LIGHTS];

// uniform sampler2DShadow depth_texture;

vec3 get_scattered_light(vec3 light_ambient, vec3 light_color, float attenuation, float diffuse)
{
    vec3 ambient_color = light_ambient * color_diffuse;
    vec3 diffuse_color = light_color * diffuse * 0.8 * color_diffuse;
    return ambient_color + diffuse_color;
}

vec3 get_reflected_light(vec3 light_direction, vec3 light_color, float attenuation, float diffuse)
{
    vec3 view_direction = u_camera_position - f_fragpos;
    vec3 half_vector = normalize(light_direction + view_direction);
    float specular = max(0.0, dot(f_normal, half_vector));
    
    if (diffuse == 0.0) {
        specular = 0.0;
    } else {
        specular = pow(specular, 2.0) * 0.7;
    }

    return light_color * specular * attenuation * color_specular;
}

float get_spot_light_attenuation(vec3 light_direction, float light_distance, SpotLight light)
{
    float attenuation = 1.0 /  ( light.constant_attenuation + 
                        light.linear_attenuation * light_distance + 
                        light.quadratic_attenuation * light_distance * light_distance);

    float spot_cos = dot(light_direction, 
                    normalize(-light.cone_direction));

    if (spot_cos > light.spot_cos_cut_off) {
        attenuation = 0.0;
    } else {
        attenuation *= pow(spot_cos, light.spot_exponent);
    }
    
    return attenuation;
}

float get_point_light_attenuation(vec3 light_direction, float light_distance, PointLight light)
{
    return 1.0 /  ( light.constant_attenuation + 
                    light.linear_attenuation * light_distance + 
                    light.quadratic_attenuation * light_distance * light_distance );
}

vec4 get_final_color(vec3 light_direction, vec3 light_color, vec3 light_ambient, float attenuation, float diffuse) 
{
    vec3 scattered_light = get_scattered_light(light_ambient, light_color, attenuation, diffuse);
    vec3 reflected_light = get_reflected_light(light_direction, light_color, attenuation, diffuse);

    // float shadow = textureProj(depth_texture, f_shadow_coord);
    // vec3 rgb = min(shadow * scattered_light + shadow * reflected_light, vec3(1.0));
    vec3 rgb = min(scattered_light + reflected_light, vec3(1.0));
    return vec4(rgb, 1.0);
}

vec4 calc_directional_light(DirectionalLight light)
{
    float diffuse = max(0.0, dot(f_normal, normalize(light.direction)));
    return get_final_color(light.direction, light.color, light.ambient, 1.0, diffuse);;
}

vec4 calc_point_light(PointLight light)
{
    vec3 light_direction = light.position - f_fragpos;
    float light_distance = length(light_direction);
    light_direction = light_direction / light_distance;
    
    float attenuation = get_point_light_attenuation(light_direction, light_distance, light);
    float diffuse = max(0.0, dot(f_normal, normalize(light_direction)));
    return get_final_color(light_direction, light.color, light.ambient, attenuation, diffuse);
}

vec4 calc_spot_light(SpotLight light)
{
    vec3 light_direction = light.position - f_fragpos;
    float light_distance = length(light_direction);
    light_direction = light_direction / light_distance;

    float attenuation = get_spot_light_attenuation(light_direction, light_distance, light);
    float diffuse = max(0.0, dot(f_normal, light.cone_direction));
    return get_final_color(light_direction, light.color, light.ambient, attenuation, diffuse);
}

void main()
{
    vec4 final_color = texture(u_texture, f_texcoord);

    for (int i = 0; i < directional_lights_size; i++) {
        final_color += calc_directional_light(u_directional_lights[i]);
    }

    if (point_lights_size != 0) {
        for (int i = 0; i < point_lights_size; i++) {
            final_color += calc_point_light(u_point_lights[i]);
        }
    }
    
    if (spot_lights_size != 0) {
        for (int i = 0; i < spot_lights_size; i++) {
            final_color += calc_spot_light(u_spot_lights[i]);
        }
    }
    
    fragcolor = min(final_color, vec4(1.0));
}
