#version 330 core

uniform vec3 u_base_color;
uniform float u_specular_percent;
uniform float u_diffuse_percent;

uniform samplerCube u_specular_env_map;
uniform samplerCube u_diffuse_env_map;

in vec3 f_reflect_direction;
in vec3 f_normal;

out vec4 fragcolor;

void main()
{
    vec3 diffuse_color = vec3(texture( u_diffuse_env_map, f_normal ));
    vec3 specular_color = vec3(texture( u_specular_env_map, f_normal ));

    vec3 color = mix(u_base_color, diffuse_color * u_base_color, u_diffuse_percent);
    color = mix(color, specular_color + color, u_specular_percent);

    fragcolor = vec4(color, 1.0);
}