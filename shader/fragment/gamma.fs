#version 330 core
out vec4 fragColor;

in vec3 fragPos;
in vec3 normal;
in vec2 texCoord;

uniform sampler2D texSampler;
uniform vec3 viewPos;
uniform vec3 lightPos[4];
uniform vec3 lightColor[4];

void main()
{
    vec3 texColor = texture(texSampler, texCoord).rgb;
   
    float inverse_gamma = 1.0/2.2;
    float attenuation;
    float d;
    vec3 lighting;
    for(int i = 0; i < 4; i++)
    {
        // ambient
        d = length(lightPos[i] - fragPos);
        attenuation = 1 / d;
        // vec3 ambient =  0.02 * lightColor[i];

        // diffuse
        vec3 norm = normalize(normal);
        vec3 lightDir = normalize(lightPos[i] - fragPos);
        float diff = max(dot(norm, lightDir), 0.0);
        vec3 diffuse = diff * lightColor[i] * attenuation;

        // specular
        vec3 viewDir = normalize(viewPos - fragPos);
        vec3 reflectDir = reflect(lightDir, norm);
        float spec = pow(max(dot(reflectDir, viewDir), 0.0), 64.0);
        vec3 specular = spec * lightColor[i] * attenuation;
        lighting += diffuse + specular;
    }
    texColor *= lighting;
    texColor = pow(texColor, vec3(inverse_gamma));
    fragColor = vec4(texColor, 1.0); 
}