#version 330 core
#define M_PI 3.1415926535897932384626433832795
out vec4 fragColor;

in VertexAttribute {
    vec3 fragPos;
    vec3 normal;
    vec2 texCoords;
    vec3 tangent;
    vec3 bitangent;
} fs_in;

uniform vec3 albedo;
uniform float metallic; 
uniform float roughness;
uniform float ambientOclusion;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

uniform vec3 lightPos;
uniform vec3 viewPos;

float normalDistributeFunction(vec3 normal, vec3 halfwayDir, float roughness)
{
    float roughnessSquare = roughness * roughness;
    float normalDotHalfway = dot(normal, halfwayDir);
    float normalDotHalfwaySquare = normalDotHalfway * normalDotHalfway;
    float t = normalDotHalfwaySquare * (roughnessSquare - 1) + 1;
    float tSquare = t * t;
    float denominator = M_PI * tSquare;
    return roughnessSquare / denominator;
}

float geometryFunction(vec3 normal, vec3 view, float roughness)
{
    float k;
    // direct 
    k = (roughness + 1) * (roughness + 1) / 8; 
    // IBL

    float normalDotView = dot(normal, view);
    float denominator = normalDotView * (1 - k) + k;
    return normalDotView / denominator;
}

float geometrySmith(vec3 normal, vec3 viewDir, vec3 lightDir, float roughness)
{
    float geoView = geometryFunction(normal, viewDir, roughness);
    float geoLight = geometryFunction(normal, lightDir, roughness);
    return geoView * geoLight;
}

vec3 fresnel(vec3 halfwayDir, vec3 viewDir, vec3 baseReflectivity)
{
    float base = 1 - dot(halfwayDir, viewDir);
    vec3 res = baseReflectivity +  (1 - baseReflectivity) * pow(base, 5.0);
    return res;
}

void main()
{
    vec3 normal = normalize(fs_in.normal);
    vec3 viewDir = normalize(viewPos - fs_in.fragPos);

    vec3 L = vec3(0.0);
    vec3 lightColor = vec3(23.47, 21.31, 20.79);
    lightColor = normalize(lightColor);
    vec3 lightDir = normalize(lightPos - fs_in.fragPos); // solid angle
    vec3 halfwayDir = normalize(viewDir + lightDir);

    float lightDistance = length(fs_in.fragPos - lightPos);
    float attenuation = 1 / (lightDistance * lightDistance);

    float cosTheta = max(dot(normal, lightDir), 0);

    // Li of the light source
    vec3 radiance = lightColor * attenuation * cosTheta; 

    // for one light source
    // Lo = fr * radiance

    // calculatea fr = kd * flambert + ks * fcook-terrace
    // fLambert = c / pi
    // c is albedo
    vec3 diffuse = albedo / M_PI;
    
    vec3 baseReflectivity = vec3(0.04);
    baseReflectivity = mix(baseReflectivity, albedo, metallic);

    float D = normalDistributeFunction(normal, halfwayDir, roughness);
    float G = geometrySmith(normal, viewDir, lightDir, roughness);
    vec3 F = fresnel(halfwayDir, viewDir, baseReflectivity);
    vec3 numerator = D * F * G;
    float denominator = (4 * dot(viewDir, normal) * dot(lightDir, normal));
    vec3 specular =  numerator / max(denominator, 0.001);

    vec3 ks = F; // reflected
    vec3 kd = vec3(1.0) - ks; // refracted

    kd *= 1.0 - metallic; // metal dont refract light

    vec3 sumRadiance = (kd * albedo + ks * specular)  * radiance;

    vec3 ambient  = vec3(0.03) * albedo * ambientOclusion;
    vec3 color = ambient * sumRadiance;
    // gamma correction
    color = color / (color + vec3(1.0));
    color = pow (color, vec3(1.0 / 2.2));

    fragColor = vec4(color, 1.0);
}