#version 330 core

in VS_OUT {
    vec3 fragPos;
    vec2 texCoords;
    vec3 tangentLightPos;
    vec3 tangentViewPos;
    vec3 tangentFragPos;
} fs_in;

uniform sampler2D diffuseMap;
uniform sampler2D normalMap;
uniform sampler2D depthMap;

uniform float height_scale;

out vec4 fragColor;

vec2 steepParallaxMapping(vec2 texCoords, vec3 viewDir)
{
    float numberOfLayers = 10;
    float depthRate = 1 / numberOfLayers;

    float layerDepth = 0.0;

    vec2 p = viewDir.xy / viewDir.z * height_scale;
    vec2 deltaMovement = p / numberOfLayers;
    vec2 curTexCoords = texCoords;
    float curDepth = texture(depthMap, curTexCoords).r;
    for (int i = 0; i <= numberOfLayers; i++)
    {
        if (layerDepth >= curDepth)
        {
            return curTexCoords;
        }
        curTexCoords -= deltaMovement;
        layerDepth += depthRate;
        curDepth = texture(depthMap, curTexCoords).r;
    }
}

vec2 parallaxMapping(vec2 texCoords, vec3 viewDir)
{
    const float minLayers = 8;
    const float maxLayers = 32;

    float numLayers = mix(maxLayers, minLayers, abs(dot(vec3(0.0, 0.0, 1.0), viewDir)));

    float layerDepth = 1.0/ numLayers;

    float currentLayerDepth = 0.0;
    vec2 P = viewDir.xy / viewDir.z * height_scale;
    vec2 deltaTexCoords = P / numLayers;

    vec2 currentTexCoords = texCoords;
   

    float currentDepthMapValue = texture(depthMap, currentTexCoords).r;
    while(currentLayerDepth < currentDepthMapValue)
    {
        currentTexCoords -= deltaTexCoords;
        currentDepthMapValue = texture(depthMap, currentTexCoords).r;
        currentLayerDepth += layerDepth;
    }

    vec2 prevTexCoords = currentTexCoords + deltaTexCoords;
    float afterDepth = currentDepthMapValue - currentLayerDepth;
    float beforeDepth = texture(depthMap, prevTexCoords).r - currentLayerDepth + layerDepth;

    float weight = afterDepth / (afterDepth - beforeDepth);
    vec2 finalTexCoords = prevTexCoords * weight + currentTexCoords * (1.0 - weight);

    return finalTexCoords;
    
}

void main()
{
    vec3 viewDir = normalize(fs_in.tangentViewPos - fs_in.tangentFragPos);
    // vec2 texCoords = steepParallaxMapping(fs_in.texCoords, viewDir);
    vec2 texCoords = parallaxMapping(fs_in.texCoords, viewDir);

    if(texCoords.x > 1.0 || texCoords.y > 1.0 || texCoords.x < 0.0 || texCoords.y < 0.0)
        discard;

    vec3 normal = texture(normalMap, texCoords).rgb;
    normal = normalize(normal * 2.0 - 1.0);

    vec3 objectColor = texture(diffuseMap, texCoords).rgb;
    // vec3 lightColor = vec3(0.5);
    // ambient
    vec3 ambient = 0.1 * objectColor;

    // diffuse
    vec3 lightDir = normalize(fs_in.tangentLightPos - fs_in.tangentFragPos);
    float diff = max(dot(lightDir, normal), 0.0);
    vec3 diffuse = diff * objectColor;

    // specular
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float spec = pow(max(dot(normal, halfwayDir), 0.0), 32.0);
    vec3 specular = vec3(0.2) * spec;

    fragColor = vec4(ambient + diffuse + specular, 1.0);
}