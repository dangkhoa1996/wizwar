// follow example in opengl development cookbook
#version 330
layout (location = 0) in vec3 a_pos;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 model;

uniform float time;

const float amplitude = 0.5;
const float frequency = 4;
const float PI = 3.14159;

void main()
{
    float distance = length(a_pos);
    float y = amplitude * sin(-PI * distance * frequency + time * 2);
    gl_Position = projection * view * model * vec4(a_pos.x, y, a_pos.z, 1.0);
}
