#version 400 core

layout (vertices = 16) out;
uniform vec3 u_camera_position;
void main()
{
	vec4 center = vec4(0.0);

	for (int i = 0; i < gl_PatchVerticesIn; i++) {
		center += gl_in[i].gl_Position;
	}

	center /= gl_PatchVerticesIn;
	float d = distance(center, vec4(u_camera_position, 1.0));
	const float lodscale = 0.05;
	float tess_LOD = mix(64.0, 1.0, d * lodscale);
	tess_LOD = clamp(0.5 * tess_LOD, 1.0, 64.0);
	
	for (int i = 0; i < 4; i++) {
		gl_TessLevelOuter[i] = tess_LOD;
	}

	gl_TessLevelInner[0] = tess_LOD;
	gl_TessLevelInner[1] = tess_LOD;

	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
}