#version 420 core

layout (quads, equal_spacing, cw) in;
// layout (triangles, equal_spacing, cw) in;
// layout (isolines, equal_spacing, cw) in;

// layout (std140) uniform pv_mat {
//     mat4 projection;
//     mat4 view;
// };

#define M_PI 3.14159265358979323846
// uniform mat4 u_model;
// wave parameters


// out vec4 f_pass_color;
out vec2 texcoord;

float Hanning( vec2 p )
{
    p -= 0.5; // map unit square to [-.5, .5]
    float r = length( p );
    r = cos( M_PI * r / 2.0 );
    r *= r;
    return r;
}

// float waveHeight() {
//     float omega = 2 / L;
//     float phase = S * omega;
//     return Amp * sin(dot(D, vec2(gl_TessCoord.x, gl_TessCoord.y)) * omega + t * phase);
// }

void main()
{
    float u = gl_TessCoord.x;
    float v = gl_TessCoord.y;
    texcoord = gl_TessCoord.xy;

#define p(i)  gl_in[i].gl_Position

    // vec4 pos = v * (u * p(0) + (1 - u) * p(2)) + (1 - v) * (u * p(1) + (1 - u) * p(2)) + (u*v*p(4));
    vec4 pos = u * v * p(0) +
        (1 - u) * v * p(1) +
        (1 - u) * (1 - v) * p(2) +
        u * (1 - v) * p(3);

    // texcoord = pos.xz/ length(pos);

    gl_Position = pos;
}