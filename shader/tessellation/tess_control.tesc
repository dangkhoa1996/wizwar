#version 400 core

layout (vertices = 32) out;

void main()
{
    gl_TessLevelOuter[0] = 32.0; 
    gl_TessLevelOuter[1] = 32.0; 
    gl_TessLevelOuter[2] = 32.0; 
    gl_TessLevelOuter[3] = 32.0; 

    gl_TessLevelInner[0] = 16.0;
    gl_TessLevelInner[1] = 16.0;

    // if (gl_InvocationID >= gl_PatchVerticesIn) {
    //     if (gl_InvocationID == 2) {
    //         gl_out[gl_InvocationID].gl_Position = gl_in[1].gl_Position + vec4(0.0, -2.0, 3.0, 1.0);
    //     }
    //     if (gl_InvocationID == 3) {
    //         gl_out[gl_InvocationID].gl_Position = gl_in[0].gl_Position + vec4(0.0, -4.0, 1.0, 1.0);
    //     }
    //     if (gl_InvocationID == 4) {
    //         gl_out[gl_InvocationID].gl_Position = gl_in[0].gl_Position + vec4(1.0, 50.0, 2.0, 1.0);
    //     }
    // } else {
    // }
    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
}