#version 400 core

layout (vertices = 16) out;
struct EdgeCenters { vec4 edge_center[4]; };
uniform EdgeCenters patches[];
uniform vec3 u_camera_position;

void main()
{
	float tess_LOD = 0.0;
	for (int i = 0; i < 4; i++) {
		float d = distance(patches[gl_PrimitiveID].edge_center[i], vec4(u_camera_position, 1.0));
		const float lod_scale = 2.5;
		tess_LOD = mix(0.0, gl_MaxTessGenLevel, d * lod_scale);
		gl_TessLevelOuter[i] = tess_LOD;
	}

	tess_LOD = clamp(0.5 * tess_LOD, 0.0, gl_MaxTessGenLevel);
	gl_TessLevelInner[0] = tess_LOD;
	gl_TessLevelInner[1] = tess_LOD;

	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
	
}