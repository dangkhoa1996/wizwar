#version 330 core
layout (location = 0) in vec3 iPos;

out vec3 pos;
out vec3 vfragPos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
void main()
{
    pos = iPos.xyz;
    vfragPos = vec3(model * vec4(iPos, 1.0));
    gl_Position = projection * view * model * vec4(iPos, 1.0);
}