#version 330 core
layout (location = 0) in vec3 a_pos;
layout (location = 1) in vec3 a_normal;
layout (location = 2) in vec3 a_texcoord; // texcoord in assimp is a vec3
layout (location = 3) in vec3 a_tangent;
layout (location = 4) in vec3 a_bitangent;
layout (location = 5) in vec3 a_color;
layout (location = 6) in ivec4 a_bone_id;
layout (location = 7) in vec4 a_weight;

out vec3 f_normal;
out vec3 f_texcoord;
out vec3 f_tangent;
out vec3 f_bitangent;
out vec3 f_fragpos;
out vec4 f_shadow_coord;

layout (std140) uniform pv_mat {
    mat4 projection;
    mat4 view;
};

uniform mat4 u_model;
uniform mat4 u_root_model;

#define MAX_MATRICES_NUMBER 64
uniform mat4 bone_trans_mat[MAX_MATRICES_NUMBER];
uniform mat4 u_shadow_matrix;

void main()
{
    mat4 total_offset = mat4(1.0);
    float weight_0 = a_weight[0];
    float weight_1 = a_weight[1];
    float weight_2 = a_weight[2];
    float weight_3 = a_weight[3];
    float total_weight = weight_0 + weight_1 + weight_2 + weight_3;

    for (int i = 0; i < 4; i++) {
        if (a_weight[i] > 0) {
            float coefficient = a_weight[i] / total_weight;
            total_offset +=  coefficient * bone_trans_mat[a_bone_id[i]];
        }
    }

    mat4 final_model = u_model * total_offset;

    gl_Position = projection * view * u_root_model * final_model * vec4(a_pos, 1.0);
    f_texcoord = a_texcoord;
    f_fragpos = vec3(final_model * vec4(a_pos, 1.0));
    f_normal = normalize(mat3(transpose(inverse(final_model))) * a_normal);
    f_shadow_coord = u_shadow_matrix * final_model * vec4(a_pos, 1.0);
}