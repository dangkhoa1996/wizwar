#version 330 core
layout (location = 0) in vec3 a_pos;
layout (location = 1) in vec3 a_normal;
layout (location = 2) in vec2 a_texcoord;

out vec2 f_texcoord;
out vec3 f_normal;
out vec3 f_fragpos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4(a_pos, 1.0);
    f_texcoord = a_texcoord;
    f_fragpos = vec3(model * vec4(a_pos, 1.0));
    f_normal = mat3(transpose(inverse(model))) * a_normal;
}
