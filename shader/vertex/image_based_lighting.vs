#version 330 core

layout(location = 0) in vec3 a_pos;
layout(location = 1) in vec3 a_normal;
layout(location = 2) in vec2 a_texcoord;

layout (std140) uniform pv_mat {
    mat4 projection;
    mat4 view;
};

out vec3 f_normal;
out vec3 f_reflect_direction;

uniform mat4 u_model;

void main()
{
    mat3 normal_mat = mat3(transpose(inverse(u_model)));
    f_normal = normalize(normal_mat * a_normal);
    vec4 pos = view * u_model * vec4(a_pos, 1.0);
    vec3 eye_direction = pos.xyz;
    f_reflect_direction = reflect(eye_direction, f_normal);
    gl_Position = projection * view * u_model * vec4(a_pos, 1.0);
}
