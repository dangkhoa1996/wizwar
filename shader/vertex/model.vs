#version 330 core
layout (location = 0) in vec3 a_pos;
layout (location = 1) in vec3 a_normal;
layout (location = 3) in vec2 a_texcoord; // texcoord in assimp is a vec3
layout (location = 2) in vec3 a_tangent;
// layout (location = 4) in vec3 a_bitangent;
// layout (location = 5) in vec3 a_color;
// layout (location = 6) in ivec4 a_bone_id;
// layout (location = 7) in vec4 a_weight;

out vec3 f_normal;
out vec2 f_texcoord;
out vec3 f_tangent;
out vec3 f_bitangent;
out vec3 f_fragpos;

layout (std140) uniform pv_mat {
    mat4 projection;
    mat4 view;
};

uniform mat4 u_model;

void main()
{
    gl_Position = projection * view * u_model * vec4(a_pos, 1.0);
    f_texcoord = a_texcoord;
    f_fragpos = vec3(u_model * vec4(a_pos, 1.0));
    f_normal = mat3(transpose(inverse(u_model))) * a_normal;
}
