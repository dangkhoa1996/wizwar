#version 330 core
layout (location = 0) in vec3 position;
uniform sampler2D heightMap;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

uniform vec3 viewPos;
out float dis;
out vec3 outColor;
void main()
{
    vec4 pos = vec4(position, 1.0);
    vec4 fragPos = vec4(model * pos);
    float y = pos.y;
    if (y < -0.2) 
        outColor = vec3(66.0 / 255.0, 134.0 / 255.0 , 244.0 / 255.0);
    else if (y >= -0.2 && y <= 0.3)
        outColor = vec3(74.0 / 255.0, 178.0 / 255.0, 112.0 / 255.0);
    else outColor = vec3(1.0, 1.0, 1.0);

    gl_Position = projection * view * model * pos;
    dis = length(viewPos - fragPos.xyz);
}
