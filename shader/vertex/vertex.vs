#version 400
layout (location = 0) in vec2 a_pos;

uniform mat4 u_model;

layout(std140) uniform pv_mat {
    mat4 projection;
    mat4 view;
};

void main()
{
    gl_Position = projection * view * u_model * vec4(a_pos, 0.0, 1.0);
}