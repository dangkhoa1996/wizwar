#version 330 core
layout(location = 0) in vec3 a_pos;
layout (location = 6) in ivec4 a_bone_id;
layout (location = 7) in vec4 a_weight;
uniform mat4 u_model;
uniform mat4 u_root_model;
uniform mat4 u_vp_mat;

#define MAX_MATRICES_NUMBER 64
uniform mat4 bone_trans_mat[MAX_MATRICES_NUMBER];

void main()
{
    mat4 total_offset = mat4(1.0);
    mat4 fix_coord = mat4(  1.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 1.0, 0.0,
                            0.0, -1.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 1.0);
    float weight_0 = a_weight[0];
    float weight_1 = a_weight[1];
    float weight_2 = a_weight[2];
    float weight_3 = a_weight[3];

    float total_weight = weight_0 + weight_1 + weight_2 + weight_3;

    for (int i = 0; i < 4; i++) {
        if (a_weight[i] > 0) {
            float coefficient = a_weight[i] / total_weight;
            total_offset +=  coefficient * bone_trans_mat[a_bone_id[i]];
        }
    }

    mat4 final_model = fix_coord * u_model * total_offset;
    gl_Position = u_vp_mat * u_root_model * final_model * vec4(a_pos, 1.0);
}
