#version 330 core
layout (location = 0) in vec3 a_pos;
layout (location = 1) in vec3 a_normal;
layout (location = 2) in vec2 a_texcoord;

out vec3 f_normal;
out vec2 f_texcoord;
out vec3 f_color;
out vec3 f_frag_position;

// out VS_FS_INTERFACE {
//     // vec4 shadow_coord;
//     vec3 world_coord;
//     vec3 eye_coord;
//     vec3 normal;
// } vertex;


layout (std140) uniform pv_mat {
    mat4 projection;
    mat4 view;
};

uniform vec3 u_color;
uniform mat4 u_model;
uniform vec4 u_clip_plane;
// uniform mat4 u_shadow_mat;

void main()
{
    vec4 world_pos = u_model * vec4(a_pos, 1.0);
    gl_ClipDistance[0] = dot(world_pos, u_clip_plane);
    vec4 eye_pos = view * world_pos;
    vec4 clip_pos = projection * eye_pos;
    // vertex.world_coord = world_pos.xyz;
    // vertex.eye_coord = eye_pos.xyz;
    // // vertex.shadow_coord = u_shadow_mat * world_pos;
    // vertex.normal = mat3(view * u_model) * a_normal;

    mat3 normal_mat = mat3(transpose(inverse(u_model)));
    // f_normal = normalize(normal_mat * a_normal);
    f_normal = a_normal;
    f_texcoord = a_texcoord;
    f_color = u_color;
    f_frag_position = vec3(u_model * vec4(a_pos, 1.0));
    gl_Position = clip_pos;
}
