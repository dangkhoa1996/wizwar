#version 330 core
layout (location = 0) in vec3 a_pos;
layout (location = 1) in vec3 a_normal;
layout (location = 2) in vec2 a_texcoord;

out vec2 f_texcoord;
out vec3 f_normal;
out vec3 f_fragpos;

uniform mat4 u_model;
uniform vec4 u_clip_plane;

layout(std140) uniform pv_mat{
    mat4 projection;
    mat4 view;
};

void main()
{
    vec4 world_pos = u_model * vec4(a_pos, 1.0);
    gl_ClipDistance[0] = dot(world_pos, u_clip_plane);

    gl_Position = projection * view * world_pos;
    f_normal = mat3(transpose(inverse(u_model))) * a_normal;
    f_texcoord = a_texcoord;
    f_fragpos = vec3(u_model * vec4(a_pos, 1.0));
}