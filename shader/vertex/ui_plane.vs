#version 330 core

layout (location = 0) in vec3 a_pos;
layout (location = 1) in vec3 a_normal;
layout (location = 2) in vec2 a_texcoord;

layout (std140) uniform pv_mat {
    mat4 projection;
    mat4 view;
};

uniform mat4 u_model;
out vec2 f_texcoord;

void main() {
  f_texcoord = a_texcoord;
  gl_Position = u_model * vec4(a_pos, 1.0);
}