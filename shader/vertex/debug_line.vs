#version 330 core
layout(location = 0) in vec3 a_pos;


layout (std140) uniform pv_mat {
    mat4 projection;
    mat4 view;
};

uniform mat4 u_model;

void main()
{
    vec4 pos = projection * view * u_model * vec4(a_pos, 1.0);
    gl_Position = pos.xyww;
}