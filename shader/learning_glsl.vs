#version 330 core
#pragma STDGL invariant(all)

struct Particle {
    float lifetime;
    vec3 pos;
    vec3 velocity;
};

layout(shared, row_major) uniform {};

layout (std140) uniform b {
    float size;
    layout (offset=32) vec4 color;
    layout (align=1024) vec4 a[12];
    vec4 b[12];
} buf;

float coeff[3];
float[3] coeff;
int indices[];
uniform vec4 base_color;
buffer mat3 will_this_correct;
shared vec4 something_cool_here;
invariant int unchange;
precise float something_unchange;
invariant gl_Position;
invariant centroid out vec3 color;
#pragma optimize(off)

#extension all : <directive>

#define LPos(n) gl_LightSource[(n)].position

#if defined(CONDITION_VARS) && NUM_ELEMENTS < 10

#elif NUM_ELEMENTS > 10

#endif

void main()
{
    for (int i = 0; i < coeff.length(); i++) {
        coeff[i] *= 2.0;
    }

}

