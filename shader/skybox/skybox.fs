#version 330 core
out vec4 fragcolor;

in vec3 f_texcoord;
uniform samplerCube skybox;

void main()
{
    fragcolor = texture(skybox, f_texcoord);
}
