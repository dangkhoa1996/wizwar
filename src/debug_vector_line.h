#pragma once
#include <GL/glew.h>
#include <glm/glm.hpp>

#include "camera/abstract_camera.h"
#include "systems/render/shader_program.hpp"

namespace Engine
{
class DebugVectorLine
{
public:
    DebugVectorLine();
    DebugVectorLine(glm::vec3 p1, glm::vec3 p2);
    ~DebugVectorLine();

    void draw(ICamera& camera);

    void set_p1(glm::vec3& p1);
    void set_p2(glm::vec3& p2);
    void print_vertex_data();

    GLuint vao;
    GLuint vbo;
    float* m_vertex_data;

    ShaderProgram* m_shader_program;
    glm::vec3 m_p1;
    glm::vec3 m_p2;
};

}