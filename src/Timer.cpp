#include "timer.hpp"

namespace Engine
{
Timer* Timer::m_instance;
Timer::Timer() {
    start_time = glfwGetTime();
}

// in second
double Timer::get_current_time()
{
    return glfwGetTime();
}

double Timer::get_elapsed_time()
{
    return glfwGetTime() - start_time;
}

} // Engine
