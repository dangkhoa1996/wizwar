#include <GLFW/glfw3.h>

namespace Engine
{

class Timer
{
public:
    Timer();
    ~Timer() {
        delete m_instance;
    };

    static Timer* get_instance() {
        if (!m_instance) {
            m_instance = new Timer();
        }
        return m_instance;
    };

    double get_current_time();
    double get_elapsed_time();
private:
    double start_time;
    static Timer* m_instance;
};

} // Engine
