#include "target_camera.h"
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <iostream>

#include "systems/input/mouse_input.hpp"

namespace Engine
{

TargetCamera::TargetCamera()
    : ICamera(), rotate_rx(0.0), rotate_ry(0.0)
{
    m_position = glm::vec3(100.0, 80.0, 100.0);
    m_front = glm::vec3(0.0, m_position.y, 0.0);
    m_up    = glm::vec3(0.0, 1.0, 0.0);
    m_target = glm::vec3(0.0, 0.0, 0.0);

    m_right = glm::normalize(glm::cross(m_target - m_position, m_up));
    m_view = glm::lookAt(m_position, m_target, m_up);
    m_projection   = glm::perspective(glm::radians(45.0), m_aspect_ratio, 0.1, 2000.0);
}

TargetCamera::~TargetCamera()
{
}

void TargetCamera::update(double delta_time)
{
    if (MouseInput::get_instance()->left_button_state == GLFW_PRESS) {
        m_is_move = true;
    } else if (MouseInput::get_instance()->left_button_state == GLFW_RELEASE) {
        m_is_move = false;
    }
    
    update_rotation();
    update_scroll();
}

void TargetCamera::update_rotation()
{
    xpos = MouseInput::get_instance()->m_x_pos;
    ypos = MouseInput::get_instance()->m_y_pos;
    
    rx = xpos - old_x;
    ry = ypos - old_y;
    
    old_x = xpos;
    old_y = ypos;

    float angle = glm::acos(glm::dot(glm::normalize(glm::vec2(rx, ry)), glm::vec2(1.0, 0.0)));

    if (angle > glm::radians(45.0) && angle < glm::radians(135.0)) {
        m_right = glm::cross(m_position - m_target, m_up);
        (ry  > 0.0f) ? rotate_ry = 0.01f : rotate_ry = -0.01f;
        m_position = glm::rotate(m_position, (float)rotate_ry, m_right);
    } else if (angle < glm::radians(45.0) || angle > glm::radians(135.0)) {
        (rx > 0.0f) ? rotate_rx = -0.05f : rotate_rx = 0.05f;
        m_position = glm::rotate(m_position, (float)rotate_rx, glm::vec3(0.0, 1.0, 0.0));
    }
    // m_view = glm::lookAt(m_position, m_target, m_up) * glm::inverse(m_parent_model);
    m_view = glm::lookAt(m_position, m_target, m_up);
    // std::cout << "camera update new position: " << glm::to_string(m_position) << '\n' ;
}

void TargetCamera::update_scroll()
{
    if (!MouseInput::get_instance()->m_scroll_state) return;
    auto yoffset = MouseInput::get_instance()->m_scroll_offset;
    if (m_fov >= 1.0f && m_fov <= 45.0f)
        m_fov -= yoffset;
    if (m_fov <= 1.0f)
        m_fov = 1.0f;
    if (m_fov > 45.f)
        m_fov = 45.f;

    m_projection = glm::perspective(glm::radians(m_fov), m_aspect_ratio, 0.1, 1000.0);
    // m_projection = glm::ortho(100.0, 100.0, 100.0, 100.0);
}

void TargetCamera::update_target(glm::vec3& target)
{
    m_target = target;
}


}
