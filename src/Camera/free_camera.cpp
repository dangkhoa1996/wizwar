#include "free_camera.h"
#include <iostream>

#include "systems/input/keyboard_input.hpp"
#include "systems/input/mouse_input.hpp"


namespace Engine
{
FreeCamera::FreeCamera() : ICamera(), m_last_x(0.0), m_last_y(0.0), m_temp_front(glm::vec3(0.0f, 0.0f, -1.0f))
{
    m_position      = glm::vec3(2.0f, 5.0f, 2.0f);
    m_front         = glm::normalize(glm::vec3(-2.0f, -5.0f, -2.0f));
    m_right         = glm::cross(m_front, m_up);
    m_view          = glm::lookAt(m_position, m_position + m_front, m_up);
    m_projection    = glm::perspective(glm::radians(m_fov), m_aspect_ratio, m_z_near, m_z_far);
}

FreeCamera::~FreeCamera() {}

void FreeCamera::update(double delta_time)
{    
    // if (is_move()) {
    m_rot_mat = glm::yawPitchRoll(glm::radians(m_yaw), glm::radians(m_pitch),glm::radians(m_roll));
    m_front = glm::vec3(m_rot_mat * glm::vec4(0.0f, 0.0f, -1.0f, 0.0));
    m_right         = glm::normalize(glm::cross(m_front, m_up));
    m_view          = glm::lookAt(m_position, m_position + m_front, m_up);
    m_projection    = glm::perspective(glm::radians(m_fov), m_aspect_ratio, m_z_near, m_z_far);
    // }
}

// change this, add glfwWindow 
void FreeCamera::move()
{
    auto w_state = KeyboardInput::get_instance()->get_key_state(GLFW_KEY_W);
    auto s_state = KeyboardInput::get_instance()->get_key_state(GLFW_KEY_S);
    auto a_state = KeyboardInput::get_instance()->get_key_state(GLFW_KEY_A);
    auto d_state = KeyboardInput::get_instance()->get_key_state(GLFW_KEY_D);
   

    if (w_state >= GLFW_PRESS && a_state >= GLFW_PRESS) {
        m_position += m_camera_speed * glm::normalize(m_front - m_right);
    }
    else if (w_state >= GLFW_PRESS && d_state >= GLFW_PRESS) {
        m_position += m_camera_speed * glm::normalize(m_right + m_front);
    }
    else if (s_state >= GLFW_PRESS && a_state >= GLFW_PRESS) {
        m_position += m_camera_speed * glm::normalize(-m_right - m_front);
    }
    else if (s_state >= GLFW_PRESS && d_state >= GLFW_PRESS) {
        m_position += m_camera_speed * glm::normalize(m_right - m_front);
    }
    else if (w_state >= GLFW_PRESS) {
        m_position += m_camera_speed * m_front;
    } 
    else if (s_state >= GLFW_PRESS) {
        m_position += -m_camera_speed * m_front;
    }  
    else if (a_state >= GLFW_PRESS) {
        m_position += -m_camera_speed * m_right;
    } 
    else if (d_state >= GLFW_PRESS) {
        m_position += m_right * m_camera_speed;
    }
}

bool FreeCamera::is_move()
{
    auto w_state = KeyboardInput::get_instance()->get_key_state(GLFW_KEY_W);
    auto s_state = KeyboardInput::get_instance()->get_key_state(GLFW_KEY_S);
    auto a_state = KeyboardInput::get_instance()->get_key_state(GLFW_KEY_A);
    auto d_state = KeyboardInput::get_instance()->get_key_state(GLFW_KEY_D);
    auto xpos = MouseInput::get_instance()->m_x_pos;
    auto ypos = MouseInput::get_instance()->m_y_pos;
    return w_state || s_state || a_state || d_state || xpos || ypos;
}

void FreeCamera::update_rotation()
{
    auto xpos = MouseInput::get_instance()->m_x_pos;
    auto ypos = MouseInput::get_instance()->m_y_pos;

    m_yaw += (m_last_x - xpos) * m_sensitivity;
    m_pitch += (m_last_y - ypos) * m_sensitivity;

    m_last_x = xpos;
    m_last_y = ypos;

    if (m_pitch > 89.f)
        m_pitch = 89.f;
    if (m_pitch < -89.f)
        m_pitch = -89.f;
}

void FreeCamera::update_scroll()
{
    auto yoffset = MouseInput::get_instance()->m_scroll_offset;
    if (yoffset != 0.0f)
        m_fov -= yoffset;
    if (m_fov <= 1.0f)
        m_fov = 1.0f;
    if (m_fov > 45.f)
        m_fov = 45.f;
}

void FreeCamera::set_perspective()
{
    m_projection = glm::perspective(glm::radians(m_fov), m_aspect_ratio, m_z_near, m_z_far);
}

void FreeCamera::set_ortho()
{
    m_projection = glm::ortho(0.0, 1280.0, 0.0, 1280.0, m_z_near, m_z_far);
}

}