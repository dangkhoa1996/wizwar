#pragma once
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <iostream>

namespace Engine
{
class ICamera
{
public:
    ICamera()
        : m_yaw(0.0) , m_pitch(0.0) , m_roll(0.0)
        , m_fov(45.0)
        , m_position(glm::vec3(0.0))
        , m_up(glm::vec3(0.0, 1.0, 0.0))
        , m_right(glm::vec3(0.0))
        , m_view(glm::mat4(0.0))
        , m_front(glm::vec3(0.0, 0.0, -1.0))
        , m_is_move(false)
        , rx(0.0), ry(0.0)
        , old_x(0.0) , old_y(0.0)
        , m_parent_model(glm::mat4(1.0))
        , m_model(glm::mat4(1.0))
        , m_screen_height(1080.0)
        , m_screen_width(1920.0)
        , m_is_locked(true) {
        };
    virtual ~ICamera() {};

    const double m_aspect_ratio = 16.0 / 9.0;
    const double m_z_near       = 0.1;
    const double m_z_far        = 2000.0;

    glm::vec3 m_position;
    glm::vec3 m_front;
    glm::vec3 m_up;
    glm::vec3 m_right;

    double m_yaw, m_pitch, m_roll;
    double m_fov;

    glm::mat4 m_view;
    glm::mat4 m_projection;

    double m_screen_width;
    double m_screen_height;

    bool m_is_move;
    bool m_is_locked;

    int state;
    float old_x, old_y;
    float rx, ry;

    glm::mat4 m_parent_model;
    glm::mat4 m_model;

    virtual void update(double delta_time) {};

    virtual void move() {};
    virtual void update_rotation() {};
    virtual void update_scroll() {};
    virtual void set_perspective() {};
    virtual void set_ortho() {};
};
}
