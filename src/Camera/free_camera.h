#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#define GLM_ENABLE_EXPERIMENTAL

#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/string_cast.hpp>

#include "abstract_camera.h"
#include "systems/input/mouse_input.hpp"

namespace Engine
{
    class FreeCamera : public ICamera
    {
    public:
        FreeCamera();
        ~FreeCamera();

        bool m_first_mouse = true;

        double m_last_x;
        double m_last_y;

        double m_delta_time;

        glm::vec3 m_temp_front;
        glm::vec3 m_move_direction;
        glm::mat4 m_rot_mat;

        virtual void update(double delta_time) override;
        virtual void move();
        bool is_move();

        virtual void update_rotation() override;
        virtual void update_scroll() override;

        void set_perspective();
        void set_ortho();
    private:
        float m_camera_speed   = 0.5f;
        float m_sensitivity   = 0.05f;
    };
} // Engine
