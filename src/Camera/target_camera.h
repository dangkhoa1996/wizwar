#pragma once
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/euler_angles.hpp>

#include "abstract_camera.h"

namespace Engine
{

class TargetCamera : public ICamera
{
public:
    TargetCamera();
    ~TargetCamera();

    glm::vec3 m_target;
    glm::mat4 R;
   

    double rotate_rx;
    double rotate_ry;

    double xpos;
    double ypos;

    float m_distance;
    void update(double delta_time) override;
    void update_rotation() override;
    void update_scroll() override;
    void update_target(glm::vec3& target);
};

}
