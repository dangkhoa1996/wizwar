#include "ErrorLog.h"
#include <iostream>

namespace Engine
{
    ErrorLog* ErrorLog::p_Instance;
    ErrorLog::ErrorLog() {}

    ErrorLog::~ErrorLog() {}

    void ErrorLog::findError(const std::string& where, int line)
    {
        auto errorCode = glGetError();
        if (errorCode != GL_NO_ERROR)
        {
            std::cout << where << ": " << errorCode << "\tline: " << line << '\n';
        }
    }
} // Engine
