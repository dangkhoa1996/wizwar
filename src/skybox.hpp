#pragma once
#include <string>

#include "Camera/abstract_camera.h"
#include "systems/render/shader_program.hpp"
#include "geometry/basic_shape.hpp"

namespace Engine
{
    
class Skybox
{
public:
    Skybox();
    ~Skybox();

    ShaderProgram* m_shader_program;

    GLuint m_vbo;
    GLuint m_vao;
    GLuint m_cube_map_id;

    void use_cube_map();
    void draw(ICamera& camera);
};

}
