#include "util.h"

#include <glm/gtx/string_cast.hpp>
#include <iostream>
#include <vector>

namespace Engine
{
namespace Util
{

glm::mat4 ai_mat4x4_to_glm_mat4(const aiMatrix4x4& ai_mat)
{
    return glm::mat4(ai_mat.a1, ai_mat.b1, ai_mat.c1, ai_mat.d1,
                     ai_mat.a2, ai_mat.b2, ai_mat.c2, ai_mat.d2,
                     ai_mat.a3, ai_mat.b3, ai_mat.c3, ai_mat.d3,
                     ai_mat.a4, ai_mat.b4, ai_mat.c4, ai_mat.d4);

    //return glm::mat4(ai_mat.a1, ai_mat.a2, ai_mat.a3, ai_mat.a4,
    //                 ai_mat.b1, ai_mat.b2, ai_mat.b3, ai_mat.b4,
    //                 ai_mat.c1, ai_mat.c2, ai_mat.c3, ai_mat.c4,
    //                 ai_mat.d1, ai_mat.d2, ai_mat.d3, ai_mat.d4);
}


glm::vec3 ai_vec3_to_glm_vec3(const aiVector3D& ai_vec)
{
    return glm::vec3(ai_vec.x, ai_vec.y, ai_vec.z);
}


glm::quat ai_quat_to_glm_quat(const aiQuaternion& ai_quat)
{
    return glm::quat(ai_quat.w, ai_quat.x, ai_quat.y, ai_quat.z);
}

void print_nodes(aiNode* root) 
{
    std::vector<aiNode*> node_stack;
    node_stack.reserve(100);
    node_stack.emplace_back(root);

    while (!node_stack.empty()) {
        aiNode* cur_node = node_stack.back();
        node_stack.pop_back();

        std::cout << cur_node->mName.C_Str() << '\n'; 

        print_ai_mat(cur_node->mTransformation);

        if (cur_node->mNumChildren > 0) {
            std::cout << " children: \n";
            for (unsigned int i = 0; i < cur_node->mNumChildren; i++) {
                aiNode* child = cur_node->mChildren[i];
                std::cout << child->mName.C_Str() << '\n';
                node_stack.emplace_back(cur_node->mChildren[i]);
            }
            std::cout << '\n';
        }
    }
}

void print_ai_mat(const aiMatrix4x4& mat)
{
    std::cout << mat.a1 << ' ' << mat.a2 << ' ' << mat.a3 << ' ' << mat.a4 << '\n'
              << mat.b1 << ' ' << mat.b2 << ' ' << mat.b3 << ' ' << mat.b4 << '\n'
              << mat.c1 << ' ' << mat.c2 << ' ' << mat.c3 << ' ' << mat.c4 << '\n'
              << mat.d1 << ' ' << mat.d2 << ' ' << mat.d3 << ' ' << mat.d4 << '\n';
}

void print_ai_quat(const aiQuaternion& quat)
{
    std::cout << quat.w << ' ' << quat.x << ' ' << quat.y << ' ' << quat.z << '\n';
}

void print_ai_vec3(const aiVector3D& vec)
{
    std::cout << vec.x << ' ' << vec.y << ' ' << vec.z << '\n'; 
}


} // namespace Util

} // namespace Engine
