#include "transform.hpp"

namespace Engine
{

Transform::Transform() : 
    m_position(glm::vec3(0.0f)),
    m_rotation(glm::quat(1.0f, 0.0f, 0.0f ,0.0f)),
    m_scale(glm::vec3(1.0f))
{

}

Transform::~Transform()
{

}

Transform::Transform(glm::vec3 position, glm::vec3 scale, glm::quat rotation) : 
        m_position(position), 
        m_scale(scale), 
        m_rotation(rotation)
{
   
}

void Transform::set_position(glm::vec3& position) 
{
    m_position = position;
}
void Transform::set_scale(glm::vec3& scale) 
{
    m_scale = scale;
}

void Transform::set_rotation(glm::quat& quat) 
{
    m_rotation = quat;
}

void Transform::translate(glm::vec3& trans) 
{
    m_position += trans;
}

void Transform::scale(glm::vec3& scale) 
{
    set_scale(scale);
}

void Transform::rotate(glm::vec3& axis, float angle) 
{
    m_rotation = glm::rotate(m_rotation, angle, axis);
}

glm::mat4 Transform::get_transform() 
{
    glm::mat4 transform = glm::mat4(1.0);
    transform = glm::translate(transform, m_position);
    transform *= glm::mat4_cast(m_rotation);
    transform = glm::scale(transform, m_scale);
    return transform;
}

}
   
   