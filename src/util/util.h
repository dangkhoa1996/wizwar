#pragma once
#define GLM_ENABLE_EXPERIMENTAL

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <assimp/matrix4x4.h>
#include <assimp/quaternion.h>
#include <assimp/vector3.h>
#include <assimp/scene.h>


namespace Engine
{
struct BoneOffsetMatrix;
namespace Util
{
    glm::mat4 ai_mat4x4_to_glm_mat4(const aiMatrix4x4& ai_mat);
    glm::vec3 ai_vec3_to_glm_vec3(const aiVector3D& ai_vec);
    glm::quat ai_quat_to_glm_quat(const aiQuaternion& ai_quat);


    void print_nodes(aiNode* root);
    void print_ai_mat(const aiMatrix4x4& mat);
    void print_ai_quat(const aiQuaternion& quat);
    void print_ai_vec3(const aiVector3D& vec);
} // namespace Util

} // namespace Engine
