#pragma once
#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>


namespace Engine
{
class Transform
{
public:
    Transform();
    Transform(glm::vec3 pos, glm::vec3 scale, glm::quat rotation);
    ~Transform();

    glm::quat m_rotation;
    glm::vec3 m_position;
    glm::vec3 m_scale;

    Transform operator= (Transform other) {
        m_position = other.m_position;
        m_scale = other.m_scale;
        m_rotation = other.m_rotation;
        return *this;
    }

    void set_position(glm::vec3& position);
    void set_scale(glm::vec3& scale);
    void set_rotation(glm::quat& quat);
    void translate(glm::vec3& trans);
    void scale(glm::vec3& scale);
    void rotate(glm::vec3& axis, float angle);
    glm::mat4 get_transform();
};
}
