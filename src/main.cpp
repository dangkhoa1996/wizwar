#include "game.hpp"
#include "timer.hpp"

#define CRT_SECURE_NO_WARNINGS
using namespace Engine;

int main()
{
    Engine::Game* game = new Engine::Game();
    game->init("Prototype");

    // game loop
    double previous = Timer::get_instance()->get_current_time();
    double lag      = 0.0;
    double current  = 0.0;
    double elapsed  = 0.0;
    std::cout << "Getting inside the game loop\n";
    const double S_PER_UPDATE = 1.0 / 60.0;
    double fps                = 0.0;
    game->pre_render();
    while (game->is_running())
    {
        current  = Timer::get_instance()->get_current_time();
        elapsed  = current - previous;
        previous = current;
        lag += elapsed;
        // std::cout << elapsed << '\n';
        if (lag >= S_PER_UPDATE)
        {
            // update
            fps = 1.0 / lag;
            // std::cout << fps << '\n';
            game->update(S_PER_UPDATE);
            game->render(S_PER_UPDATE);
            lag -= S_PER_UPDATE;
        }
    }
    std::cout << "Exitting the game\n";
    game->exit();
    return 0;
}
