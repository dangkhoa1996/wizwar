/*
#include "UniformBlock.h"
#include <glm/glm.hpp>

namespace Example
{
    UniformBlock::UniformBlock(ICamera& camera)
    {
        ShaderProgram* redShader
            = new ShaderProgram(Shader{ ShaderType::VERTEX, "../shader/vertex.glsl" }, Shader{ ShaderType::FRAGMENT, "../shader/fragment/red.fs" });

        ShaderProgram* blueShader = new ShaderProgram(
            Shader{ ShaderType::VERTEX, "../shader/vertex.glsl" }, Shader{ ShaderType::FRAGMENT, "../shader/fragment/blue.fs" });

        ShaderProgram* greenShader = new ShaderProgram(
            Shader{ ShaderType::VERTEX, "../shader/vertex.glsl" }, Shader{ ShaderType::FRAGMENT, "../shader/fragment/green.fs" });

        ShaderProgram* yellowShader = new ShaderProgram(
            Shader{ ShaderType::VERTEX, "../shader/vertex.glsl" }, Shader{ ShaderType::FRAGMENT, "../shader/fragment/yellow.fs" });

        m_cube_red = new Cube(redShader);
        m_cube_red->m_cModelData->translate(glm::vec3(-0.75, 0.75, 0.0));

        m_cube_blue = new Cube(blueShader);
        m_cube_blue->m_cModelData->translate(glm::vec3(0.75, 0.75, 0.0));

        m_cube_green = new Cube(greenShader);
        m_cube_green->m_cModelData->translate(glm::vec3(-0.75, -0.75, 0.0));

        m_cube_yellow = new Cube(yellowShader);
        m_cube_yellow->m_cModelData->translate(glm::vec3(0.75, -0.75, 0.0));

        m_uniformBlockIndexRed    = glGetUniformBlockIndex(redShader->m_program, "VP");
        m_uniformBlockIndexBlue   = glGetUniformBlockIndex(blueShader->m_program, "VP");
        m_uniformBlockIndexGreen  = glGetUniformBlockIndex(greenShader->m_program, "VP");
        m_uniformBlockIndexYellow = glGetUniformBlockIndex(yellowShader->m_program, "VP");

        glUniformBlockBinding(redShader->m_program, m_uniformBlockIndexRed, 0);
        glUniformBlockBinding(blueShader->m_program, m_uniformBlockIndexBlue, 0);
        glUniformBlockBinding(greenShader->m_program, m_uniformBlockIndexGreen, 0);
        glUniformBlockBinding(yellowShader->m_program, m_uniformBlockIndexYellow, 0);

        glGenBuffers(1, &m_uboMatrices);
        glBindBuffer(GL_UNIFORM_BUFFER, m_uboMatrices);
        glBufferData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4), NULL, GL_STATIC_DRAW);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);

        glBindBufferRange(GL_UNIFORM_BUFFER, 0, m_uboMatrices, 0, 2 * sizeof(glm::mat4));

        glBindBuffer(GL_UNIFORM_BUFFER, m_uboMatrices);
        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), glm::value_ptr(camera.m_projection));
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
    }

    UniformBlock::~UniformBlock()
    {
        delete m_cube_red;
        delete m_cube_blue;
        delete m_cube_green;
        delete m_cube_yellow;
    }

    void UniformBlock::update() {}

    void UniformBlock::draw(ICamera& camera)
    {
        glBindBuffer(GL_UNIFORM_BUFFER, m_uboMatrices);
        glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(camera.m_view));
        glBindBuffer(GL_UNIFORM_BUFFER, 0);

        m_cube_red->draw(camera);
        m_cube_blue->draw(camera);
        m_cube_green->draw(camera);
        m_cube_yellow->draw(camera);
    }
} */
