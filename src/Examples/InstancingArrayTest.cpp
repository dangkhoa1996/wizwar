/*
#include "InstancingArrayTest.h"
namespace Example
{
    InstancingArrayTest::InstancingArrayTest()
    {
        m_planeShader = new ShaderProgram(
            Shader{ ShaderType::VERTEX, "../shader/vertex/instancingArray.vs" }, Shader{ ShaderType::FRAGMENT, "../shader/fragment/green.fs" });

        m_plane = new Plane(m_planeShader);
        setupInstanceVBO();
    }

    InstancingArrayTest::~InstancingArrayTest()
    {
        delete m_planeShader;
        delete m_plane;
        // glDeleteBuffers(1, &instanceVBO);
    }

    void InstancingArrayTest::update() {}

    void InstancingArrayTest::draw(ICamera& camera)
    {
        m_plane->drawInstances(camera);
    }

    void InstancingArrayTest::setupInstanceVBO()
    {
        int index    = 0;
        float offset = 0.1f;

        for (int y = -10; y < 10; y += 2)
        {
            for (int x = -10; x < 10; x += 2)
            {
                glm::vec2 translation;
                translation.x         = (float)x / 10.0f + offset;
                translation.y         = (float)y / 10.0f + offset;
                translations[index++] = translation;
            }
        }

        // need to bind the vertex array
        glBindVertexArray(CPlaneShape::getInstance().VAO);

        glGenBuffers(1, &instanceVBO);
        glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * 100, &translations[0], GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glEnableVertexAttribArray(2);
        glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glVertexAttribDivisor(2, 1);
    }
}*/
