#pragma once
#include <GLFW/glfw3.h>

#include "../Camera/abstract_camera.h"

using namespace Engine;
namespace Example
{

class Example
{
public:
    virtual ~Example() {}

    virtual void init(ICamera& camera) {}
    virtual void update(double elapsed_time) {}
    virtual void process_input(GLFWwindow*& window) {}
    virtual void draw(ICamera& camera) {}
};

}
