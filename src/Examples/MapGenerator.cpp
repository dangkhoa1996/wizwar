/*
#include "MapGenerator.h"
#include "../ErrorLog.h"
#include <iostream>
// load model
//      rework shader
// add light source
// skybox

namespace Example
{
    MapGenerator::MapGenerator() {}

    MapGenerator::~MapGenerator() {}

    void MapGenerator::init(TextureLibrary& textureLibrary, ICamera& camera)
    {

        m_cShader = new ShaderProgram(
            Shader{ ShaderType::VERTEX, "../shader/vertex/meshVertex.vs" },
            Shader{ ShaderType::FRAGMENT, "../shader/fragment/meshFragment.fs" });

        m_cShader->setMat4("projection", camera.m_projection);
        m_cShader->setMat4("view", camera.m_view);

        m_lightPos = glm::vec3(50.0f, 100.f, -40.f);

        m_cShader->setVec3("lightPos", m_lightPos);
        m_cShader->setVec3("viewPos", camera.m_position);

        m_model = new Model();
        m_model->load("../models/well/source/Kolodec.fbx", textureLibrary, camera, m_cShader);

        m_points.reserve(POINTS_NUM * POINTS_NUM);
        // height map
        heightShader = new ShaderProgram(
            Shader{ ShaderType::VERTEX, "../shader/vertex/heightMap.vs" },
            Shader{ ShaderType::FRAGMENT, "../shader/fragment/heightMap.fs" });

        m_noise.SetNoiseType(FastNoise::Perlin); // range from -1.0 to 1.0
        m_noise.SetFrequency(0.04);

        float step = 1.0 / POINTS_NUM;  // --> bug overhere texture coordinate range from 0.0 to 1.0
                                        // to scale up the terrain use the trasnform matrix
        float x = 0.0;
        float y = 0.0;
        float z = 0.0;
        for (int i = 0; i < POINTS_NUM; i++)
        {
            for (int j = 0; j < POINTS_NUM; j++)
            {
                z = i * step;
                x = j * step;
                y = m_noise.GetNoise(j, i);

                Point p = Point{ x, y, z };
                m_points.emplace_back(p);
            }
        }

        // indicies
        std::vector<unsigned int> heightMapIndices;
        int quadNum   = POINTS_NUM - 1;
        hmIndicesSize = 6 * quadNum * quadNum;
        heightMapIndices.reserve(hmIndicesSize);
        int i1, i2, i3, i4;
        for (int ci = 0; ci < quadNum; ci++)
        {
            for (int ri = 0; ri < quadNum; ri++)
            {
                i1 = ci * POINTS_NUM + ri;
                i2 = ci * POINTS_NUM + ri + 1;
                i3 = (ci + 1) * POINTS_NUM + ri;
                i4 = (ci + 1) * POINTS_NUM + ri + 1;

                heightMapIndices.push_back(i1);
                heightMapIndices.push_back(i2);
                heightMapIndices.push_back(i3);

                heightMapIndices.push_back(i3);
                heightMapIndices.push_back(i2);
                heightMapIndices.push_back(i4);
            }
        }

        std::cout << "hmIndicesSize : " << hmIndicesSize << '\n';

        glGenVertexArrays(1, &heightVAO);
        std::cout << "heightVAO: " << heightVAO << '\n';
        glGenBuffers(1, &heightVBO);
        glGenBuffers(1, &heightEBO);

        glBindVertexArray(heightVAO);
        glBindBuffer(GL_ARRAY_BUFFER, heightVBO);
        std::cout << "heightVBO size: " << 3 * sizeof(float) * m_points.size() << '\n';
        glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float) * m_points.size(), &m_points[0], GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, heightEBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, heightMapIndices.size() * sizeof(unsigned int), &heightMapIndices[0], GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, false, 3 * sizeof(float), (void*)0);

        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        heightMapId = textureLibrary.addTexture("../img/height_map.png", "heightMap", TextureType::DIFFUSE);
        std::cout << "heightMapId: " << heightMapId << '\n';

        std::cout << "heightVAO: " << heightVAO << '\n'
                  << "heightVBO: " << heightVBO << '\n';

        m_heightModelMatrix = glm::mat4(1.0f);
        m_heightModelMatrix = glm::translate(m_heightModelMatrix, glm::vec3(-10.0f, 0.0, -10.0f));
        m_heightModelMatrix = glm::scale(m_heightModelMatrix, glm::vec3(10.0f, 2.0f, 10.0f));

        heightShader->setMat4("projection", camera.m_projection);
        heightShader->setMat4("view", camera.m_view);
        heightShader->setMat4("model", m_heightModelMatrix);

        heightShader->setTexture("heightMap", 0);
        heightShader->setVec3("viewPos", camera.m_position);

        float plane[] = {
            1.0, 0.0, 1.0,
            1.0, 0.0, -1.0,
            -1.0, 0.0, -1.0,
            -1.0, 0.0, 1.0
        };

        int indices[] = {
            0, 1, 2,
            2, 3, 0
        };

        glGenVertexArrays(1, &gridVAO);
        glGenBuffers(1, &gridVBO);

        glGenBuffers(1, &gridEBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gridEBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), &indices[0], GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        glBindVertexArray(gridVAO);
        glBindBuffer(GL_ARRAY_BUFFER, gridVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(plane), &plane[0], GL_STATIC_DRAW);

        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, false, 3 * sizeof(GL_FLOAT), (void*)0);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

        // gridShader = new ShaderProgram(
        //     std::vector<Shader> {
        //         Shader { ShaderType::VERTEX, "../shader/vertex/gridPlane.vs" },
        //         Shader { ShaderType::GEOMETRY, "../shader/geometry/gridPlane.gs" },
        //         Shader { ShaderType::FRAGMENT, "../shader/fragment/gridPlane.fs" }
        //     }
        // );

        gridShader = new ShaderProgram(
            Shader{ ShaderType::VERTEX, "../shader/vertex/gridPlane.vs" },
            Shader{ ShaderType::FRAGMENT, "../shader/fragment/gridPlane.fs" });

        m_gridModelMatrix = glm::mat4(1.0);

        m_gridModelMatrix = glm::scale(m_gridModelMatrix, glm::vec3(10.0f, 10.0f, 10.0f));

        gridShader->setMat4("model", m_gridModelMatrix);
        gridShader->setMat4("projection", camera.m_projection);
    }

    void MapGenerator::update()
    {
        // m_heightModelMatrix = glm::rotate(m_heightModelMatrix, glm::radians(0.05f), glm::vec3(0.0f, 1.0f, 0.0f));
        // heightShader->setMat4("model", m_heightModelMatrix);
    }

    void MapGenerator::processInput(unsigned int key)
    {
        if (key == GLFW_KEY_LEFT_BRACKET)
            m_noiseFreq += 0.01;
        else if (key == GLFW_KEY_RIGHT_BRACKET)
            m_noiseFreq -= 0.01;
        m_noise.SetFrequency(m_noiseFreq);

        for (auto& p : m_points)
        {
            p.y = m_noise.GetNoise(p.x, p.z);
        }
        glBindBuffer(GL_ARRAY_BUFFER, heightVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(Point) * m_points.size(), &m_points[0], GL_STATIC_DRAW);
    }

    void MapGenerator::draw(ICamera& camera)
    {
        // model
        // m_cShader->setMat4("view", camera.m_view);
        // m_cShader->setMat4("projection", camera.m_projection);
        // m_cShader->setVec3("viewPos", camera.m_position);
        // m_model->draw(camera);

        // height map
        heightShader->use();
        heightShader->setMat4("view", camera.m_view);
        heightShader->setMat4("projection", camera.m_projection);
        heightShader->setVec3("viewPos", camera.m_position);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, heightMapId);
        heightShader->setTexture("heightMap", 1);

        glBindVertexArray(heightVAO);

        //glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(m_points)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, heightEBO);
        glDrawElements(GL_TRIANGLES, hmIndicesSize, GL_UNSIGNED_INT, NULL);
        glPointSize(2.0f);
        glDrawArrays(GL_POINTS, 0, m_points.size());
        glBindVertexArray(0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        // grid
        // gridShader->use();
        // gridShader->setMat4("view", camera.m_view);
        // gridShader->setMat4("projection", camera.m_projection);
        // glBindVertexArray(gridVAO);
        // glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gridEBO);
        // glDrawElements(GL_TRIANGLE_STRIP, 6, GL_UNSIGNED_INT, NULL);
        // glBindVertexArray(0);
    }
    void MapGenerator::generateMap(float*& heightMap, int size)
    {
        FastNoise noise;
        noise.SetNoiseType(FastNoise::Perlin);

        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                heightMap[i * size + j] = noise.GetNoise(i, j);
            }
        }
    }
}
*/
