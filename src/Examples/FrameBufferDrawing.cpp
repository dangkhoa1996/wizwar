/*
#include "FrameBufferDrawing.h"
#include <algorithm>
#include <iostream>

namespace Example
{
    FrameBufferDrawing::FrameBufferDrawing(unsigned int m_screenWidth, unsigned int m_screenHeight, TextureLibrary& textureLibrary, ICamera& camera)
    {
        // frame buffer
        glGenFramebuffers(1, &m_frameBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, m_frameBuffer);

        // generate texture
        glGenTextures(1, &m_texColorBuffer);
        glBindTexture(GL_TEXTURE_2D, m_texColorBuffer);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_screenWidth, m_screenHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_texColorBuffer, 0);
        glBindTexture(GL_TEXTURE_2D, 0);

        glGenRenderbuffers(1, &m_rbo);
        glBindRenderbuffer(GL_RENDERBUFFER, m_rbo);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, m_screenWidth, m_screenHeight);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);

        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, m_rbo);

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            std::cout << "framebuffer is not complete\n";
        }
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);

        glBindVertexArray(VAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));

        textureLibrary.addTexture("../img/wood.png", "floor", TextureType::DIFFUSE);

        m_screenShader = new ShaderProgram(Shader{ ShaderType::VERTEX, "../shader/vertex/framebuffer.vs" },
            Shader{ ShaderType::FRAGMENT, "../shader/post_processing/linearizedepth.fs" });

        m_screenShader->setTexture("screenTexture", 0);
        m_screenShader->setFloat("near_plane", m_nearPlane);
        m_screenShader->setFloat("far_plane", m_farPlane);

        m_planeShader
            = new ShaderProgram(Shader{ ShaderType::VERTEX, "../shader/vertex/plane.vs" }, Shader{ ShaderType::FRAGMENT, "../shader/fragment/plane.fs" });
        m_planeShader->setMat4("view", camera.m_view);
        m_planeShader->setMat4("projection", camera.m_projection);

        m_plane = new Plane(m_planeShader);
        m_plane->useTexture("floor", TextureType::DIFFUSE, "textureSampler", textureLibrary);
    }

    FrameBufferDrawing::~FrameBufferDrawing() {}

    void FrameBufferDrawing::update() {}

    void FrameBufferDrawing::draw(ICamera& camera)
    {
        // first pass
        glBindFramebuffer(GL_FRAMEBUFFER, m_frameBuffer);
        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glEnable(GL_DEPTH_TEST);

        // draw scene
        m_plane->draw(camera);
        // std::sort(m_grassArr.begin(), m_grassArr.end(), [=](Engine::Plane* a, Engine::Plane* b) {
        //     glm::vec3 camPos = camera.m_cameraPos;
        //     float disA       = glm::length(camPos - a->m_cModelData->m_position);
        //     float disB       = glm::length(camPos - b->m_cModelData->m_position);
        //     return disA > disB;
        // });
        // for (auto& grass : m_grassArr)
        // {
        //     grass->draw(camera);
        // }

        // second pass
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glDisable(GL_DEPTH_TEST);
        glClearColor(0.6f, 0.6f, 0.6f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        m_screenShader->use();
        glBindVertexArray(VAO);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_texColorBuffer);
        glDrawArrays(GL_TRIANGLES, 0, 6);
    }

    void FrameBufferDrawing::setupGrassArr(ShaderProgram* shader, glm::vec3* positions, Engine::TextureLibrary& textureLibrary)
    {
        for (int i = 0; i < 5; i++)
        {
            Engine::Plane* grass = new Engine::Plane(shader);
            grass->m_cModelData->setPosition(positions[i]);
            grass->m_cModelData->translate(positions[i]);
            grass->m_cModelData->scale(glm::vec3(0.2f));
            grass->m_cModelData->rotate(-90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
            grass->useTexture("grass", Engine::TextureType::DIFFUSE, "texture_diffuse0", textureLibrary);
            m_grassArr.emplace_back(grass);
        }
    }
} */
