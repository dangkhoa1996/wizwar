/*
#include "AntiAliasing.h"

namespace Example
{
    AntiAliasing::AntiAliasing(int width, int height)
        : m_width(width)
        , m_height(height)
    {
    }

    AntiAliasing::~AntiAliasing()
    {
        delete m_cubeShader;
        delete m_screenShader;
        delete m_cube;
    }

    void AntiAliasing::init(TextureLibrary& textureLibrary, ICamera& camera)
    {
        m_cubeShader
            = new ShaderProgram(Shader{ ShaderType::VERTEX, "../shader/vertex/cube.vs" }, Shader{ ShaderType::FRAGMENT, "../shader/fragment/green.fs" });
        m_screenShader = new ShaderProgram(
            Shader{ ShaderType::VERTEX, "../shader/vertex/framebuffer.vs" }, Shader{ ShaderType::FRAGMENT, "../shader/post_processing/normal.fs" });

        m_cubeShader->setMat4("view", camera.m_view);
        m_cubeShader->setMat4("projection", camera.m_projection);

        m_cube = new Cube(m_cubeShader);

        setupOpengl();
        setupFramebuffer(m_width, m_height);
    }

    void AntiAliasing::update() {}

    void AntiAliasing::drawNormalScene(ICamera& camera)
    {
        // draw scene as normal in multisampled buffers
        glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffer);
        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);

        m_cubeShader->setMat4("model", m_cube->m_cModelData->getModel());
        m_cube->draw(camera);
    }

    void AntiAliasing::blitMultisampledBuffers()
    {
        // blit multisampled buffers to normal colorbuffer of intermediate fbo, image is store in screen texture
        glBindFramebuffer(GL_READ_FRAMEBUFFER, m_framebuffer);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_intermediateFBO);
        glBlitFramebuffer(0, 0, m_width, m_height, 0, 0, m_width, m_height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
    }

    void AntiAliasing::drawQuad()
    {
        // render quad with scene visual as its texture image
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        glDisable(GL_DEPTH_TEST);

        // draw screen quad

        glBindVertexArray(m_quadVAO);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_screenTexture);
        glDrawArrays(GL_TRIANGLES, 0, 6);
    }

    void AntiAliasing::draw(ICamera& camera)
    {
        drawNormalScene(camera);
        blitMultisampledBuffers();
        drawQuad();
    }

    void AntiAliasing::setupOpengl()
    {
        glEnable(GL_MULTISAMPLE);
    }

    void AntiAliasing::configQuad()
    {
        // config vao and vbo of the quad
        glGenVertexArrays(1, &m_quadVAO);
        glGenBuffers(1, &m_quadVBO);
        glBindVertexArray(m_quadVAO);
        glBindBuffer(GL_ARRAY_BUFFER, m_quadVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);

        auto floatSize = sizeof(float);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, false, 4 * floatSize, (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, false, 4 * floatSize, (void*)(2 * floatSize));
    }

    void AntiAliasing::configMSAAFramebuffer()
    {
        // configure MSAA framebuffer
        glGenFramebuffers(1, &m_framebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffer);

        // create a multisampled color attachment texture
        glGenTextures(1, &m_textureColorBufferMultiSampled);
        glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, m_textureColorBufferMultiSampled);
        glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 4, GL_RGBA, m_width, m_height, GL_TRUE);
        glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, m_textureColorBufferMultiSampled, 0);

        // create multisampled renderbuffer object
        glGenRenderbuffers(1, &m_rbo);
        glBindRenderbuffer(GL_RENDERBUFFER, m_rbo);
        glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4, GL_DEPTH24_STENCIL8, m_width, m_height);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, m_rbo);

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            std::cout << "Error framebuffer: framebuffer is not complete!\n";
        }
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void AntiAliasing::configSecondPostProcessingFramebuffer()
    {
        // configure second post processing framebuffer
        glGenFramebuffers(1, &m_intermediateFBO);
        glBindFramebuffer(GL_FRAMEBUFFER, m_intermediateFBO);

        // create a color attachment texture
        glGenTextures(1, &m_screenTexture);
        glBindTexture(GL_TEXTURE_2D, m_screenTexture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_width, m_height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_screenTexture, 0);

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            std::cout << "Error framebuffer: Intermediate framebuffer is not complete\n";
        }
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void AntiAliasing::setupFramebuffer(int width, int height)
    {
        m_width  = width;
        m_height = height;

        configQuad();
        configMSAAFramebuffer();
        configSecondPostProcessingFramebuffer();

        // shader config

        m_screenShader->setTexture("screenTexture", 0);
    }
} */
