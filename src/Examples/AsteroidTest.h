/*
#pragma once
#include "../Model.h"
#include "Example.h"
#include <array>
#include <glm/glm.hpp>

using namespace Engine;

namespace Example
{
    class AsteroidTest : public Example
    {
    public:
        AsteroidTest();
        ~AsteroidTest();

        void init(TextureLibrary& textureLibrary, ICamera& camera) override;
        void update() override;
        void draw(ICamera& camera) override;

    private:
        std::vector<Model*> m_rocks;
        Model* m_planet;
        Model* m_test;
        std::array<glm::mat4, 1000> m_modelMatrices;
        unsigned int m_buffer;
        unsigned int m_rockCount;

        ShaderProgram* m_planetShader;
        ShaderProgram* m_rockShader;
    };
}
*/
