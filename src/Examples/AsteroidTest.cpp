/*
#include "AsteroidTest.h"

namespace Example
{
    AsteroidTest::AsteroidTest()
    {
        m_test   = new Model();
        m_planet = new Model();

        m_planetShader = new ShaderProgram(
            Shader{ ShaderType::VERTEX, "../shader/vertex/meshVertex.vs" }, Shader{ ShaderType::FRAGMENT, "../shader/fragment/meshFragment.fs" });

        m_rockShader = new ShaderProgram(
            Shader{ ShaderType::VERTEX, "../shader/vertex/multiModel.vs" }, Shader{ ShaderType::FRAGMENT, "../shader/fragment/meshFragment.fs" });

        m_rockCount = m_modelMatrices.size();
    }

    AsteroidTest::~AsteroidTest()
    {
        delete m_test;
        delete m_planet;
        delete m_planetShader;
        delete m_rockShader;
    }

    void AsteroidTest::init(TextureLibrary& textureLibrary, ICamera& camera)
    {
        m_test->load("../models/rock/rock.obj", textureLibrary, camera, m_rockShader);
        m_planet->load("../models/planet/planet.obj", textureLibrary, camera, m_planetShader);
        m_planet->m_cModelData->scale(glm::vec3(3.0f, 3.0f, 3.0f));

        // setup model matrix
        std::srand((int)glfwGetTime());
        float radius = 30.0;
        float offset = 2.5f;

        float angle;
        float displacement;
        float x, y, z;
        float scale;
        float rotAngle;
        glm::mat4 model;

        for (unsigned int i = 0; i < m_rockCount; i++) // m_rockCount == m_modelMatrices.size()
        {
            model        = glm::mat4(1.0f);
            angle        = (float)i / (float)100 * 360.0f;
            displacement = (std::rand() % (int)(2 * offset * 100)) / 100.0f - offset;
            x            = sin(angle) * radius + displacement;
            displacement = (std::rand() % (int)(2 * offset * 100)) / 100.f - offset;
            y            = displacement * 0.4f;
            displacement = (rand() % (int)(2 * offset * 100)) / 100.f - offset;
            z            = cos(angle) * radius + displacement;
            model        = glm::translate(model, glm::vec3(x, y, z));

            scale = (rand() % 20) / 100.f + 0.05;
            model = glm::scale(model, glm::vec3(scale));

            rotAngle = (std::rand() % 360);
            model    = glm::rotate(model, rotAngle, glm::vec3(0.4f, 0.6f, 0.8f));

            m_modelMatrices[i] = model;
        }

        glGenBuffers(1, &m_buffer);
        glBindBuffer(GL_ARRAY_BUFFER, m_buffer);
        glBufferData(GL_ARRAY_BUFFER, m_rockCount * sizeof(glm::mat4), &m_modelMatrices[0], GL_STATIC_DRAW);

        auto meshSize    = m_test->m_meshes.size();
        GLsizei vec4Size = sizeof(glm::vec4);
        for (unsigned int i = 0; i < meshSize; i++)
        {
            glBindVertexArray(m_test->m_meshes[i]->VAO);

            glEnableVertexAttribArray(3);
            glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)0);
            glEnableVertexAttribArray(4);
            glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(vec4Size));
            glEnableVertexAttribArray(5);
            glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(2 * vec4Size));
            glEnableVertexAttribArray(6);
            glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(3 * vec4Size));

            glVertexAttribDivisor(3, 1);
            glVertexAttribDivisor(4, 1);
            glVertexAttribDivisor(5, 1);
            glVertexAttribDivisor(6, 1);

            glBindVertexArray(0);
        }
    }

    void AsteroidTest::update() {}

    void AsteroidTest::draw(ICamera& camera)
    {
        m_planet->m_meshes[0]->m_cShader->setMat4("model", m_planet->m_cModelData->getModel());
        m_planet->draw(camera);

        m_test->drawInstance(camera, m_rockCount);
    }
}
*/
