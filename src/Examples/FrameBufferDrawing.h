/*
#pragma once

#include "../ShaderProgram.h"
#include "Example.h"

#include "../Plane.h"

using namespace Engine;
namespace Example
{
    class FrameBufferDrawing : public Example
    {
    public:
        FrameBufferDrawing(unsigned int m_screenWidth, unsigned int m_screenHeight, TextureLibrary& textureLibrary, ICamera& camera);
        ~FrameBufferDrawing();

        void update() override;
        void draw(ICamera& camera) override;

    private:
        ShaderProgram* m_screenShader;
        ShaderProgram* m_planeShader;

        float quadVertices[24] = { // positions   // texCoords
            -1.0f, 1.0f, 0.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, -1.0f, 1.0f, 0.0f,

            -1.0f, 1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f
        };

        float m_nearPlane = 1.0f;
        float m_farPlane  = 7.5f;

        unsigned int VAO;
        unsigned int VBO;

        unsigned int m_frameBuffer;
        unsigned int m_texColorBuffer;
        unsigned int m_rbo;

        Plane* m_plane;
        std::vector<Plane*> m_grassArr;

        void setupGrassArr(ShaderProgram* grassShader, glm::vec3* positions, TextureLibrary& textureLibraryv);
    };
} */
