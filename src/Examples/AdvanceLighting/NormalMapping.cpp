/*
#include "NormalMapping.h"

namespace Example
{
    NormalMapping::NormalMapping() {}

    NormalMapping::~NormalMapping() {}

    void NormalMapping::init(TextureLibrary& textureLibrary, ICamera& camera)
    {
        // load vertices

        // tangent and bitangent
        glm::vec3 pos1(-1.0, 1.0, 0.0);
        glm::vec3 pos2(-1.0, -1.0, 0.0);
        glm::vec3 pos3(1.0, -1.0, 0.0);
        glm::vec3 pos4(1.0, 1.0, 0.0);

        glm::vec2 uv1(0.0f, 1.0f);
        glm::vec2 uv2(0.0f, 0.0f);
        glm::vec2 uv3(1.0f, 0.0f);
        glm::vec2 uv4(1.0f, 1.0f);

        glm::vec3 nm(0.0f, 0.0f, 1.0f);

        glm::vec3 tangent1, bitangent1;
        glm::vec3 tangent2, bitangent2;

        glm::vec3 edge1    = pos2 - pos1;
        glm::vec3 edge2    = pos3 - pos1;
        glm::vec2 deltaUV1 = uv2 - uv1;
        glm::vec2 deltaUV2 = uv3 - uv1;

        float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

        tangent1.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
        tangent1.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
        tangent1.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
        tangent1   = glm::normalize(tangent1);

        bitangent1.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
        bitangent1.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
        bitangent1.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);
        bitangent1   = glm::normalize(bitangent1);

        edge1    = pos3 - pos1;
        edge2    = pos4 - pos1;
        deltaUV1 = uv3 - uv1;
        deltaUV2 = uv4 - uv1;

        f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

        tangent2.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
        tangent2.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
        tangent2.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
        tangent2   = glm::normalize(tangent2);

        bitangent2.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
        bitangent2.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
        bitangent2.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);
        bitangent2   = glm::normalize(bitangent2);
        // edit  clang format again
        float quadVertices[] = {
            pos1.x,
            pos1.y,
            pos1.z,
            nm.x,
            nm.y,
            nm.z,
            uv1.x,
            uv1.y,
            tangent1.x,
            tangent1.y,
            tangent1.z,
            bitangent1.x,
            bitangent1.y,
            bitangent1.z,
            pos2.x,
            pos2.y,
            pos2.z,
            nm.x,
            nm.y,
            nm.z,
            uv2.x,
            uv2.y,
            tangent1.x,
            tangent1.y,
            tangent1.z,
            bitangent1.x,
            bitangent1.y,
            bitangent1.z,
            pos3.x,
            pos3.y,
            pos3.z,
            nm.x,
            nm.y,
            nm.z,
            uv3.x,
            uv3.y,
            tangent1.x,
            tangent1.y,
            tangent1.z,
            bitangent1.x,
            bitangent1.y,
            bitangent1.z,

            pos1.x,
            pos1.y,
            pos1.z,
            nm.x,
            nm.y,
            nm.z,
            uv1.x,
            uv1.y,
            tangent2.x,
            tangent2.y,
            tangent2.z,
            bitangent2.x,
            bitangent2.y,
            bitangent2.z,
            pos3.x,
            pos3.y,
            pos3.z,
            nm.x,
            nm.y,
            nm.z,
            uv3.x,
            uv3.y,
            tangent2.x,
            tangent2.y,
            tangent2.z,
            bitangent2.x,
            bitangent2.y,
            bitangent2.z,
            pos4.x,
            pos4.y,
            pos4.z,
            nm.x,
            nm.y,
            nm.z,
            uv4.x,
            uv4.y,
            tangent2.x,
            tangent2.y,
            tangent2.z,
            bitangent2.x,
            bitangent2.y,
            bitangent2.z,
        };

        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);

        glBindVertexArray(VAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);

        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, false, 14 * sizeof(GL_FLOAT), (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, false, 14 * sizeof(GL_FLOAT), (void*)(3 * sizeof(GL_FLOAT)));
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 2, GL_FLOAT, false, 14 * sizeof(GL_FLOAT), (void*)(6 * sizeof(GL_FLOAT)));
        glEnableVertexAttribArray(3);
        glVertexAttribPointer(3, 3, GL_FLOAT, false, 14 * sizeof(GL_FLOAT), (void*)(8 * sizeof(GL_FLOAT)));
        glEnableVertexAttribArray(4);
        glVertexAttribPointer(4, 3, GL_FLOAT, false, 14 * sizeof(GL_FLOAT), (void*)(11 * sizeof(GL_FLOAT)));

        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // init shader
        m_shader = new ShaderProgram(
            Shader{ ShaderType::VERTEX, "../shader/vertex/normalMapping.vs" }, Shader{ ShaderType::FRAGMENT, "../shader/fragment/normalMapping.fs" });

        // load texture
        normalMap      = textureLibrary.addTexture("../img/normalMapExample/brickwall_normal.jpg", "normalMap", TextureType::NORMALS);
        diffuseTexture = textureLibrary.addTexture("../img/normalMapExample/brickwall.jpg", "diffuse", TextureType::DIFFUSE);

        m_shader->use();
        m_shader->setTexture("normalMap", 0);
        m_shader->setTexture("diffuseTexture", 1);
        model = glm::mat4(1.0);
        // model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));

        lightPos = glm::vec3(1.0, 0.0, 3.0);
        m_shader->setVec3("lightPos", lightPos);

        m_modelShader = new ShaderProgram(
            Shader{ ShaderType::VERTEX, "../shader/vertex/meshVertex.vs" }, Shader{ ShaderType::FRAGMENT, "../shader/fragment/meshFragment.fs" });

        m_model = new Model();
        m_model->load("../models/tarvern/scene.gltf", textureLibrary, camera, m_modelShader);
    }

    void NormalMapping::update()
    {
        // model = glm::translate(model, glm::vec3(cos(glfwGetTime()), 0, 0));
        lightPos = glm::vec3(0.4 * cos(glfwGetTime()), 0, 3.0);
    }

    void NormalMapping::draw(ICamera& camera)
    {
        m_shader->setMat4("model", model);
        m_shader->setMat4("projection", camera.m_projection);
        m_shader->setMat4("view", camera.m_view);
        m_shader->setVec3("viewPos", camera.m_position);
        m_shader->setVec3("lightPos", lightPos);
        m_shader->use();
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, normalMap);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, diffuseTexture);
        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, 6);
    }
}
*/
