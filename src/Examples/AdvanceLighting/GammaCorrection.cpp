/*
#include "GammaCorrection.h"

namespace Example
{
    GammaCorrection::GammaCorrection() {}

    GammaCorrection::~GammaCorrection() {}

    void GammaCorrection::init(TextureLibrary& textureLibrary, ICamera& camera)
    {
        m_planeShader
            = new ShaderProgram(Shader{ ShaderType::VERTEX, "../shader/vertex/plane.vs" }, Shader{ ShaderType::FRAGMENT, "../shader/fragment/gamma.fs" });

        m_planeShader->setMat4("view", camera.m_view);
        m_planeShader->setMat4("projection", camera.m_projection);

        m_planeShader->setVec3("viewPos", camera.m_position);
        for (int i = 0; i < 4; i++)
        {
            auto idxStr = std::to_string(i);
            m_planeShader->setVec3("lightPos[" + idxStr + "]", m_lightPosition[i]);
            m_planeShader->setVec3("lightColor[" + idxStr + "]", m_lightColors[i]);
        }

        m_plane = new Plane(m_planeShader);

        textureLibrary.addTexture("../img/wood.png", "floor", TextureType::DIFFUSE, true);
        m_plane->useTexture("floor", TextureType::DIFFUSE, "texSampler", textureLibrary);

        glEnable(GL_FRAMEBUFFER_SRGB);
    }

    void GammaCorrection::update() {}

    void GammaCorrection::draw(ICamera& camera)
    {
        m_planeShader->setMat4("model", m_plane->m_cModelData->getModel());
        m_plane->draw(camera);
    }
} */
