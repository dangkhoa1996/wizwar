/*
#include "AdvanceLighting.h"

namespace Example
{
    AdvanceLighting::AdvanceLighting() {}

    AdvanceLighting::~AdvanceLighting()
    {
        delete m_planeShader;
        delete m_plane;
    }

    void AdvanceLighting::init(TextureLibrary& textureLibrary, ICamera& camera)
    {
        m_planeShader
            = new ShaderProgram(Shader{ ShaderType::VERTEX, "../shader/vertex/plane.vs" }, Shader{ ShaderType::FRAGMENT, "../shader/fragment/plane.fs" });
        m_planeShader->setMat4("view", camera.m_view);
        m_planeShader->setMat4("projection", camera.m_projection);

        m_planeShader->setVec3("viewPos", camera.m_position);
        m_planeShader->setVec3("lightPos", glm::vec3(0.0, 2.0, 0.0));
        m_planeShader->setInt("blinn", 1);

        m_plane = new Plane(m_planeShader);

        textureLibrary.addTexture("../img/wood.png", "floor", TextureType::DIFFUSE);
        m_plane->useTexture("floor", TextureType::DIFFUSE, "textureSampler", textureLibrary);
        // m_plane->m_cModelData->scale(glm::vec3(3.0f, 3.0f, 3.0f));
    }

    void AdvanceLighting::update() {}

    void AdvanceLighting::draw(ICamera& camera)
    {

        m_planeShader->setMat4("model", m_plane->m_cModelData->getModel());
        m_plane->draw(camera);
    }
} */
