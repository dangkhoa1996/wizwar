/*
#include "PointShadow.h"
#include "../../ErrorLog.h"

namespace Example
{
    PointShadow::PointShadow() {}

    PointShadow::~PointShadow() {}

    void PointShadow::init(TextureLibrary& textureLibrary, ICamera& camera)
    {

        m_cubeShader = new ShaderProgram(
            Shader{ ShaderType::VERTEX, "../shader/vertex/pointShadow.vs" }, Shader{ ShaderType::FRAGMENT, "../shader/fragment/pointShadow.fs" });

        m_depthShader = new ShaderProgram(std::vector<Shader>{ Shader{ ShaderType::VERTEX, "../shader/vertex/pointShadowDepthMap.vs" },
            Shader{ ShaderType::GEOMETRY, "../shader/geometry/pointShadowDepthMap.gs" },
            Shader{ ShaderType::FRAGMENT, "../shader/fragment/pointShadowDepthMap.fs" } });

        m_lightPos = glm::vec3(0.0f);

        glGenTextures(1, &depthCubeMap);

        glBindTexture(GL_TEXTURE_CUBE_MAP, depthCubeMap);
        for (unsigned int i = 0; i < 6; i++)
        {
            glTexImage2D(
                GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        }

        glGenFramebuffers(1, &depthMapFBO);
        glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
        glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthCubeMap, 0);
        glDrawBuffer(GL_NONE);
        glReadBuffer(GL_NONE);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        // light space transform
        float aspect = (float)SHADOW_WIDTH / (float)SHADOW_HEIGHT;
        near_plane   = 1.0f;
        far_plane    = 25.f;

        glm::mat4 shadowProj = glm::perspective(glm::radians(90.f), aspect, near_plane, far_plane);
        shadowTransforms.push_back(shadowProj * glm::lookAt(m_lightPos, m_lightPos + glm::vec3(1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
        shadowTransforms.push_back(shadowProj * glm::lookAt(m_lightPos, m_lightPos + glm::vec3(-1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
        shadowTransforms.push_back(shadowProj * glm::lookAt(m_lightPos, m_lightPos + glm::vec3(0.0, 1.0, 0.0), glm::vec3(0.0, 0.0, 1.0)));
        shadowTransforms.push_back(shadowProj * glm::lookAt(m_lightPos, m_lightPos + glm::vec3(0.0, -1.0, 0.0), glm::vec3(0.0, 0.0, -1.0)));
        shadowTransforms.push_back(shadowProj * glm::lookAt(m_lightPos, m_lightPos + glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, -1.0, 0.0)));
        shadowTransforms.push_back(shadowProj * glm::lookAt(m_lightPos, m_lightPos + glm::vec3(0.0, 0.0, -1.0), glm::vec3(0.0, -1.0, 0.0)));
        // setup cubevao and planevao
        // cube
        glGenVertexArrays(1, &cubeVAO);
        glGenBuffers(1, &cubeVBO);

        glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(m_cubeVertices), m_cubeVertices, GL_STATIC_DRAW);

        glBindVertexArray(cubeVAO);

        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, false, 8 * sizeof(GL_FLOAT), (void*)0);

        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, false, 8 * sizeof(GL_FLOAT), (void*)(3 * sizeof(GL_FLOAT)));

        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 2, GL_FLOAT, false, 8 * sizeof(GL_FLOAT), (void*)(6 * sizeof(GL_FLOAT)));

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

        texFloor = textureLibrary.addTexture("../img/wood.png", "floor", TextureType::DIFFUSE);

        m_cubeShader->setTexture("diffuseTexture", 0);
        m_cubeShader->setTexture("depthMap", 1);

        m_depthShader->use();
        for (unsigned int i = 0; i < 6; i++)
        {
            m_depthShader->setMat4("shadowMatrices[" + std::to_string(i) + "]", shadowTransforms[i]);
        }

        m_depthShader->setFloat("far_plane", far_plane);
        m_depthShader->setVec3("lightPos", m_lightPos);
        model = glm::mat4(1.0);
    }

    void PointShadow::update() {}

    void PointShadow::draw(ICamera& camera)
    {
        // gen depthmap

        glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
        glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
        glClear(GL_DEPTH_BUFFER_BIT);

        m_depthShader->use();
        m_depthShader->setVec3("lightPos", m_lightPos);
        for (unsigned int i = 0; i < 6; i++)
        {
            m_depthShader->setMat4("shadowMatrices[" + std::to_string(i) + "]", shadowTransforms[i]);
        }

        m_depthShader->setFloat("far_plane", far_plane);
        m_depthShader->setVec3("lightPos", m_lightPos);
        // draw

        model = glm::mat4(1.0f);
        model = glm::scale(model, glm::vec3(5.0f));
        model = glm::translate(model, glm::vec3(0.0f));
        m_depthShader->setMat4("model", model);
        m_depthShader->setInt("reverse_normals", 1);
        glDisable(GL_CULL_FACE);
        // render cubes
        glBindVertexArray(cubeVAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);
        m_depthShader->setInt("reverse_normals", 0);
        glEnable(GL_CULL_FACE);

        // cubes
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(4.0f, -3.5f, 0.0));
        model = glm::scale(model, glm::vec3(0.5f));
        m_depthShader->setMat4("model", model);
        // render cubes
        glBindVertexArray(cubeVAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        // draw normal scene
        glViewport(0, 0, camera.m_screenWidth, camera.m_screenHeight);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        m_cubeShader->use();
        m_cubeShader->setMat4("projection", camera.m_projection);
        m_cubeShader->setMat4("view", camera.m_view);
        // set lighting uniform
        m_cubeShader->setVec3("lightPos", m_lightPos);
        m_cubeShader->setVec3("viewPos", camera.m_position);
        m_cubeShader->setInt("isShadow", 1);

        m_cubeShader->setFloat("far_plane", far_plane);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texFloor);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_CUBE_MAP, depthCubeMap);
        // draw
        model = glm::mat4(1.0f);
        model = glm::scale(model, glm::vec3(5.0f));
        model = glm::translate(model, glm::vec3(0.0f));
        m_cubeShader->setMat4("model", model);
        glDisable(GL_CULL_FACE);
        m_cubeShader->setInt("reverse_normals", 1);
        // render cubes
        glBindVertexArray(cubeVAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);
        m_cubeShader->setInt("reverse_normals", 0);
        glEnable(GL_CULL_FACE);

        // cubes
        model = glm::mat4(1.0f);
        model = glm::scale(model, glm::vec3(0.5f));
        model = glm::translate(model, glm::vec3(4.0f, -3.5f, 0.0));
        m_cubeShader->setMat4("model", model);
        // render cubes
        glBindVertexArray(cubeVAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);
    }

    void PointShadow::renderScene(ShaderProgram& shader)
    {
        // render scene
        // room cube
    }
} */
