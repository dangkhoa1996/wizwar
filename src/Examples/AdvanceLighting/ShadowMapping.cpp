/*
#include "ShadowMapping.h"
#include "../../ErrorLog.h"
#include <GLFW/glfw3.h>

namespace Example
{
    ShadowMapping::ShadowMapping(unsigned int screenWidth, unsigned int screenHeight)
        : m_screenWidth(screenWidth)
        , m_screenHeight(screenHeight)
    {
        m_lightPos         = glm::vec3(-2.0f, 4.0f, -1.0f);
        m_lightProjection  = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, m_nearPlane, m_farPlane);
        m_lightView        = glm::lookAt(m_lightPos, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        m_lightSpaceMatrix = m_lightProjection * m_lightView;
    }

    ShadowMapping::~ShadowMapping()
    {
        // delete m_plane;
        // delete m_cube;
    }

    void ShadowMapping::init(TextureLibrary& textureLibrary, ICamera& camera)
    {
        m_depthShader = new ShaderProgram(
            Shader{ ShaderType::VERTEX, "../shader/vertex/depthmap.vs" }, Shader{ ShaderType::FRAGMENT, "../shader/fragment/depthmap.fs" });
        m_depthShader->setMat4("lightSpaceMatrix", m_lightSpaceMatrix);

        m_shadowShader
            = new ShaderProgram(Shader{ ShaderType::VERTEX, "../shader/vertex/shadow.vs" }, Shader{ ShaderType::FRAGMENT, "../shader/fragment/shadow.fs" });
        m_shadowShader->setMat4("lightSpaceMatrix", m_lightSpaceMatrix);
        m_screenShader = new ShaderProgram(
            Shader{ ShaderType::VERTEX, "../shader/vertex/framebuffer.vs" }, Shader{ ShaderType::FRAGMENT, "../shader/post_processing/normal.fs" });

        texFloor     = textureLibrary.addTexture("../img/wood.png", "floor", TextureType::DIFFUSE);
        texContainer = textureLibrary.addTexture("../img/container2.png", "container", TextureType::DIFFUSE);
        ;

        glGenVertexArrays(1, &planeVAO);
        glGenBuffers(1, &planeVBO);
        glBindVertexArray(planeVAO);
        glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(m_planeVertices), m_planeVertices, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
        glBindVertexArray(0);

        glGenVertexArrays(1, &cubeVAO);
        glGenBuffers(1, &cubeVBO);
        // fill buffer
        glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(m_cubeVertices), m_cubeVertices, GL_STATIC_DRAW);
        // link vertex attributes
        glBindVertexArray(cubeVAO);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

        // m_plane = new Plane(m_depthShader);
        // m_plane->useTexture("floor", TextureType::DIFFUSE, "textureSampler", textureLibrary);

        // m_cube = new Cube(m_depthShader);
        // m_cube->m_cModelData->translate(glm::vec3(-3.0, 2.0, 0.0));
        // m_cube->useTexture("container", TextureType::DIFFUSE, "textureSampler", textureLibrary);
        // m_cube2 = new Cube(m_depthShader);
        // m_cube2->m_cModelData->translate(glm::vec3(-5.0, 1.0, -1.0));
        // m_cube2->useTexture("container", TextureType::DIFFUSE, "textureSampler", textureLibrary);

        glGenVertexArrays(1, &m_quadVAO);

        glGenBuffers(1, &m_quadVBO);
        glBindBuffer(GL_ARRAY_BUFFER, m_quadVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);

        glBindVertexArray(m_quadVAO);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, false, 4 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, false, 4 * sizeof(float), (void*)(2 * sizeof(float)));
        glBindVertexArray(0);

        glGenFramebuffers(1, &m_depthMapFBO);

        glGenTextures(1, &m_depthMap);
        glBindTexture(GL_TEXTURE_2D, m_depthMap);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

        glBindFramebuffer(GL_FRAMEBUFFER, m_depthMapFBO);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_depthMap, 0);
        // here are my mistake using GL_DEPTH_COMPONENT instead of GL_DEPTH_ATTACHMENT be careful !!!
        glDrawBuffer(GL_NONE);
        glReadBuffer(GL_NONE);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        m_shadowShader->setVec3("lightPos", m_lightPos);
        m_shadowShader->setTexture("textureSampler", 1);
        m_shadowShader->setTexture("shadowMap", 0);
        m_screenShader->setTexture("screenTexture", 0);
        m_screenShader->setFloat("near_plane", m_nearPlane);
        m_screenShader->setFloat("far_plane", m_farPlane);
    }

    void ShadowMapping::update()
    {
        angle += 1;
        if (angle == 360)
        {
            angle = 0;
        }
    }

    void ShadowMapping::draw(ICamera& camera)
    {
        // step 1
        glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
        glBindFramebuffer(GL_FRAMEBUFFER, m_depthMapFBO);
        glClear(GL_DEPTH_BUFFER_BIT);
        // // draw here
        glCullFace(GL_FRONT);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texFloor);
        glm::mat4 model = glm::mat4(1.0f);
        m_depthShader->setTexture("floor", 1);
        m_depthShader->setMat4("model", model);
        m_depthShader->use();
        glBindVertexArray(planeVAO);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        // cubes
        model = glm::mat4(1.0f);
        model = glm::scale(model, glm::vec3(0.5f));
        model = glm::rotate(model, glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::translate(model, glm::vec3(0.0f, 1.5f, 0.0));

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texContainer);
        m_depthShader->setTexture("container", 1);
        m_depthShader->setMat4("model", model);
        glBindVertexArray(cubeVAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        // // step 2
        glViewport(0, 0, m_screenWidth, m_screenHeight);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glCullFace(GL_BACK);

        m_shadowShader->setMat4("projection", camera.m_projection);
        m_shadowShader->setMat4("view", camera.m_view);
        m_shadowShader->setVec3("viewPos", camera.m_position);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_depthMap);

        // planes
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texFloor);
        model = glm::mat4(1.0f);
        m_shadowShader->setMat4("model", model);
        m_shadowShader->use();
        glBindVertexArray(planeVAO);
        glDrawArrays(GL_TRIANGLES, 0, 6);

        // cube
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_depthMap);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texContainer);
        model = glm::mat4(1.0f);
        model = glm::scale(model, glm::vec3(0.5f));
        model = glm::rotate(model, glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::translate(model, glm::vec3(0.0f, 1.5f, 0.0));

        m_shadowShader->setMat4("model", model);
        m_shadowShader->use();
        glBindVertexArray(cubeVAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);

        // m_screenShader->use();
        // m_screenShader->setFloat("near_plane", m_nearPlane);
        // m_screenShader->setFloat("far_plane", m_farPlane);
        // glBindVertexArray(m_quadVAO);
        // glActiveTexture(GL_TEXTURE0);
        // glBindTexture(GL_TEXTURE_2D, m_depthMap);
        // glDrawArrays(GL_TRIANGLES, 0, 6);
        // glBindVertexArray(0);

        // // config shader and matrice
        // glBindTexture(GL_TEXTURE_2D, m_depthMap);
        // m_plane->m_cShader = m_planeShader;
        // m_cube->m_cShader = m_cubeShader;
        // m_plane->draw(camera);
        // m_cube->drawInstance(camera, 4);
    }
} */
