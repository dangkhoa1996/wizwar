/*
#pragma once
#include "../ShaderProgram.h"
#include "../Model.h"
#include "Example.h"
#include "FastNoise.h"
#include <glm/glm.hpp>

using namespace Engine;

namespace Example
{
    struct Point
    {
        float x;
        float y;
        float z;
    };
    class MapGenerator : public Example
    {
    public:
        MapGenerator();
        ~MapGenerator();

        void init(TextureLibrary& textureLibrary, ICamera& camera) override;
        void processInput(unsigned int key) override;
        void update() override;
        void draw(ICamera& camera);

    private:
        Model* m_model;
        ShaderProgram* m_cShader;
        glm::vec3 m_lightPos;
        std::vector<Point> m_points;

        const int POINTS_NUM = 100;

        GLuint heightVAO;
        GLuint heightVBO;
        GLuint heightEBO;
        int hmIndicesSize;

        GLuint gridVAO;
        GLuint gridVBO;
        GLuint gridEBO;

        ShaderProgram* gridShader;

        TextureId_t heightMapId;
        ShaderProgram* heightShader;

        glm::mat4 m_heightModelMatrix;
        glm::mat4 m_gridModelMatrix;
        float m_noiseFreq = 0.01f;
        FastNoise m_noise;

        void generateMap(float*& heightMap, int size);
    };
}
*/
