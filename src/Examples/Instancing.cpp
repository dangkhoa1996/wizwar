/*
#include "Instancing.h"

namespace Example
{
    Instancing::Instancing()
    {
        m_planeShader = new ShaderProgram(
            Shader{ ShaderType::VERTEX, "../shader/vertex/instancing.vs" }, Shader{ ShaderType::FRAGMENT, "../shader/fragment/instancing.fs" });
        // m_planeShader->init(camera);
        setupTranslation();

        m_plane = new Plane(m_planeShader);
    }

    Instancing::~Instancing()
    {
        delete m_planeShader;
        delete m_plane;
    }

    void Instancing::update() {}

    void Instancing::draw(Engine::ICamera& camera)
    {
        m_plane->drawInstances(camera);
    }

    void Instancing::setupTranslation()
    {

        int index    = 0;
        float offset = 0.1f;

        for (int y = -10; y < 10; y += 2)
        {
            for (int x = -10; x < 10; x += 2)
            {
                glm::vec2 translation;
                translation.x         = (float)x / 10.0f + offset;
                translation.y         = (float)y / 10.0f + offset;
                translations[index++] = translation;
            }
        }

        for (unsigned int i = 0; i < 100; i++)
        {
            auto index = std::to_string(i);
            m_planeShader->setVec2("offsets[" + index + "]", translations[i]);
        }
    }
}*/
