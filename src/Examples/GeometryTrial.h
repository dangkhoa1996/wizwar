/*
#pragma once
#include "../ShaderProgram.h"
#include "Example.h"

namespace Example
{
    class GeometryTrial : public Example
    {
    public:
        GeometryTrial();
        ~GeometryTrial();

        unsigned int m_VAO;
        unsigned int m_VBO;

        float m_points[20] = { -0.5f, 0.5f, 1.0f, 0.0f, 0.0f, // top-left
            0.5f, 0.5f, 0.0f, 1.0f, 0.0f,                     // top-right
            0.5f, -0.5f, 0.0f, 0.0f, 1.0f,                    // bottom-right
            -0.5f, -0.5f, 1.0f, 1.0f, 0.0f };

        ShaderProgram* m_cShader;

        virtual void update();
        virtual void draw(ICamera& camera);
    };
} // Engine */
