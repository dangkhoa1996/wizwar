/*
#pragma once
#include "../Cube.h"
#include "Example.h"

using namespace Engine;

namespace Example
{
    class AntiAliasing : public Example
    {
    public:
        AntiAliasing(int width, int height);
        ~AntiAliasing();

        void init(TextureLibrary& textureLibrary, ICamera& camera);
        void update();
        void draw(ICamera& camera);

    private:
        ShaderProgram* m_cubeShader;
        ShaderProgram* m_screenShader;
        Cube* m_cube;

        float quadVertices[24] = { // positions   // texCoords
            -1.0f, 1.0f, 0.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, -1.0f, 1.0f, 0.0f,

            -1.0f, 1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f
        };

        int m_height;
        int m_width;

        unsigned int m_quadVAO, m_quadVBO;

        unsigned int m_framebuffer, m_intermediateFBO;
        unsigned int m_screenTexture;
        unsigned int m_rbo;

        unsigned int m_textureColorBufferMultiSampled;

        void setupFramebuffer(int width, int height);
        void setupOpengl();
        void configQuad();
        void configMSAAFramebuffer();
        void configSecondPostProcessingFramebuffer();

        void drawNormalScene(ICamera& camera);
        void blitMultisampledBuffers();
        void drawQuad();
    };
}*/
