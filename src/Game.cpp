#include <utility>
#include <cstdio>
#include <iostream>
#include "Camera/free_camera.h"
#include "Camera/target_camera.h"
#include "ErrorLog.h"

#include "game.hpp"

#define LOG(X) std::cout << X << '\n'

namespace Engine
{
Game::Game() {}

Game::~Game()
{
    glfwDestroyWindow(m_window);
}

void Game::init(std::string&& name)
{
    setup_window_context(name);

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    (void)io;
    ImGui::StyleColorsDark();
    ImGui_ImplGlfw_InitForOpenGL(m_window, true);
    ImGui_ImplOpenGL3_Init("#version 330");
    setup_examples();
    LOG("Finish initializing Game\n");
}

void Game::update(double delta_time)
{
    //m_free_camera->update(delta_time);
    process_input(delta_time);
    m_scene->update(delta_time);
}

void Game::pre_render()
{
    // glfwPollEvents();
    // m_scene->pre_render();
    // glfwSwapBuffers(m_window);
}

void Game::render(double delta_time)
{
    glfwPollEvents();
    //glfwWaitEvents();
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    m_scene->render(delta_time);
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    glfwSwapBuffers(m_window);
}

void Game::setup_window_context(std::string& name)
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
    m_monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode* mode = glfwGetVideoMode(m_monitor);
    
    glfwWindowHint(GLFW_RED_BITS, mode->redBits);
    glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
    glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
    glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
    m_screen_width = mode->width;
    m_screen_height = mode->height;
    m_window = glfwCreateWindow(mode->width, mode->height, name.c_str(), NULL, NULL);

    if (!m_window) {
        LOG("Failed to create glfw window");
        glfwTerminate();
        return;
    }

    glfwMakeContextCurrent(m_window);
    glfwSetFramebufferSizeCallback(m_window, [](GLFWwindow* window, int width, int height) { glViewport(0, 0, width, height); });
    glfwSetWindowTitle(m_window, "Wiz War");

    std::cout << "Creating window finished\n";
    GLint glew_init_result = glewInit();
    if (glew_init_result != GLEW_OK) {
        std::cerr << "Error glewInit()\n" << glewGetErrorString(glew_init_result);
    }

    glfwSetInputMode(m_window, GLFW_STICKY_KEYS, GLFW_TRUE);
    glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetKeyCallback(m_window, keyboard_callback);
    glfwSetMouseButtonCallback(m_window, mouse_button_callback);
    glfwSetCursorPosCallback(m_window, cursor_pos_callback);
    glfwSetScrollCallback(m_window, scroll_callback);
}

void Game::process_input(double delta_time)
{
    if (glfwGetKey(m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(m_window, true);
    }
    
    if (glfwGetKey(m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(m_window, true);
    }

    m_scene->process_input();
}

void Game::exit()
{
    glfwTerminate();
}

bool Game::is_running()
{
    return !glfwWindowShouldClose(m_window);
}

void Game::keyboard_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    KeyboardInput::get_instance()->add_event(key, action);
    if (key == GLFW_KEY_EQUAL && action == GLFW_PRESS) {
        int mode = glfwGetInputMode(window, GLFW_CURSOR);
        if (mode == GLFW_CURSOR_DISABLED) {
            KeyboardInput::m_is_cam_locked = false;
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        } else if (mode == GLFW_CURSOR_NORMAL) {
            KeyboardInput::m_is_cam_locked = true;
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        }
    }
}

void Game::mouse_button_callback(GLFWwindow* window, 
        int button, int action, int mods) 
{
    MouseInput::get_instance()->update_button_state(button, action);
}

void Game::cursor_pos_callback(GLFWwindow* window, double xpos, double ypos)
{
    MouseInput::get_instance()->update_cursor(xpos, ypos);
}

void Game::scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    MouseInput::get_instance()->update_scroll(yoffset);
}

void Game::setup_examples()
{
    using namespace Scene;
    m_scene = new WaveScene();
    m_scene->init();
    // std::cout << "m_screen_width: " << m_screen_width << " m_screen_height: " << m_screen_height << '\n';
    m_scene->set_camera_width_and_height((double)m_screen_width, (double)m_screen_height);
}

}
