#include "skybox.hpp"
#include "ErrorLog.h"

namespace Engine
{
Skybox::Skybox() 
{
    m_shader_program = new ShaderProgram(
        Shader{ ShaderType::VERTEX, "../shader/skybox/skybox.vs" },
        Shader{ ShaderType::FRAGMENT, "../shader/skybox/skybox.fs" }
    );

    m_shader_program->attach();

    m_shader_program->bind_attrib_location(0, "a_pos");
    m_shader_program->link();
    m_shader_program->use();

    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);

    m_vbo = BasicShape::create_skybox_vbo();
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    GLuint apos_location = m_shader_program->get_attrib_location("a_pos");

    glVertexAttribPointer(apos_location, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, (void*)(sizeof(float) * 0));
    glEnableVertexAttribArray(apos_location); 

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

Skybox::~Skybox()
{
    delete m_shader_program;
}

void Skybox::use_cube_map()
{
    m_shader_program->use();
    m_shader_program->set_texture("skybox", 0);
}

void Skybox::draw(ICamera& camera)
{
    m_shader_program->use();
    m_shader_program->set_mat4("projection", camera.m_projection);
    m_shader_program->set_mat4("view", glm::mat4(glm::mat3(camera.m_view)));

    glActiveTexture(GL_TEXTURE0);
    glDepthMask(GL_FALSE);
    glBindVertexArray(m_vao);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_cube_map_id);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);
    glDepthMask(GL_TRUE);
}

} // Engine
