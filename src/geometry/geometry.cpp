#include "geometry.hpp"

namespace Engine
{

Geometry::Geometry()
{
}

Geometry::~Geometry() {}

void Geometry::draw(ShaderProgram& shader_program)
{
    // cube
    shader_program.use();
    // shader_program.set_mat4("u_model", m_transform.get_transform());
    shader_program.set_mat4("u_model", m_transform.get_transform());
    shader_program.set_vec3("u_color", m_material.m_diffuse);

    glBindVertexArray(m_vao);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

    switch(m_drawmode)
    {
        case ELEMENT:
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
            glDrawElements(GL_TRIANGLES, m_triangles_num, GL_UNSIGNED_INT, 0);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
            break;
        case ARRAY:
            glDrawArrays(GL_TRIANGLES, 0, m_vertices_num);
            break;
        case TRIANGLE_FAN:
            glDrawArrays(GL_TRIANGLE_FAN, 0, m_vertices_num);
            break;
        case PATCHES:
            glPatchParameteri(GL_PATCH_VERTICES, m_patch_vertices);
            glDrawArrays(GL_PATCHES, 0, m_vertices_num);
        default:
            break;
    }
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    shader_program.un_use();
}

}