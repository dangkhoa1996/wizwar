#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <string>
#include "systems/render/shader_program.hpp"
#include "basic_shape.hpp"
#include "systems/render/material.hpp"

#include "util/transform.hpp"
#include <iostream>


namespace Engine
{
enum EnumDrawMode { ARRAY, ELEMENT, TRIANGLE_FAN, PATCHES };
enum GeometryType { CUBE, CONE, CYLINDER, PLANE, ROUND_PLANE, SPHERE };

class Geometry
{
public:
    Geometry();
    ~Geometry();

    virtual void draw(ShaderProgram& shader_program);

    GLuint m_vao;
    GLuint m_vbo;
    GLuint m_ebo;

    size_t m_vertices_num;
    size_t m_triangles_num;
    size_t m_faces;
    size_t m_patch_vertices;

    EnumDrawMode m_drawmode;

    Transform m_transform;
    Material m_material;

    void set_transform(Transform trans) {
        m_transform = trans;
    };
    void set_material(Material mat) {
        m_material = mat;
    };

};

}
