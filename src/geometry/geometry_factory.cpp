#include "geometry_factory.hpp"
#include "basic_shape.hpp"
#include <iostream>

namespace Engine
{
GeometryFactory* GeometryFactory::m_pInstance;

GeometryFactory::GeometryFactory()
{}

GeometryFactory::~GeometryFactory()
{

}

Geometry GeometryFactory::create_cube()
{
    Geometry geom;
    geom.m_drawmode = EnumDrawMode::ARRAY;
    geom.m_vertices_num = 36;
    
    glGenVertexArrays(1, &geom.m_vao);
    glBindVertexArray(geom.m_vao);

    geom.m_vbo = BasicShape::create_cube_vbo();
    BasicShape::setting_attrib_vbo(geom.m_vbo);
    return geom;
}

Geometry GeometryFactory::create_cone()
{
    Geometry geom;
    geom.m_drawmode = EnumDrawMode::ELEMENT;

    glGenVertexArrays(1, &geom.m_vao);
    glBindVertexArray(geom.m_vao);


    geom.m_faces = 30;
    geom.m_vbo = BasicShape::create_cone_vbo(geom.m_faces);
    glBindBuffer(GL_ARRAY_BUFFER, geom.m_vbo);

    geom.m_ebo = BasicShape::create_cone_ebo(geom.m_faces);
    geom.m_triangles_num = geom.m_faces * 6 - 6;

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

    size_t normal_offset = (geom.m_faces + 1) * 3 * sizeof(float);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)(normal_offset));
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    return geom;
}

Geometry GeometryFactory::create_cylinder()
{
    Geometry geom;
    geom.m_drawmode = EnumDrawMode::ELEMENT;

    glGenVertexArrays(1, &geom.m_vao);
    glBindVertexArray(geom.m_vao);

    geom.m_faces = 20;
    geom.m_vbo = BasicShape::create_cylinder_vbo(geom.m_faces);
    geom.m_ebo = BasicShape::create_cylinder_ebo(geom.m_faces);
    geom.m_triangles_num = geom.m_faces * 12 - 12; 
    glBindBuffer(GL_ARRAY_BUFFER, geom.m_vbo);
    
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, (void*)0);

    size_t normal_offset = geom.m_faces * 6 * sizeof(float);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, (void*)(normal_offset));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    return geom;
}

Geometry GeometryFactory::create_plane()
{
    Geometry geom;
    geom.m_drawmode = EnumDrawMode::ARRAY;

    glGenVertexArrays(1, &geom.m_vao);
    glBindVertexArray(geom.m_vao);

    geom.m_vertices_num = 6;
    geom.m_vbo = BasicShape::create_plane_vbo();
    BasicShape::setting_attrib_vbo(geom.m_vbo);
    
    glBindVertexArray(0);
    return geom;
}

Geometry GeometryFactory::create_round_plane()
{
    Geometry geom;
    geom.m_drawmode = EnumDrawMode::TRIANGLE_FAN;
    
    glGenVertexArrays(1, &geom.m_vao);
    glBindVertexArray(geom.m_vao);

    geom.m_vertices_num = 40;
    geom.m_vbo = BasicShape::create_round_plane_vbo(geom.m_vertices_num);
    glBindBuffer(GL_ARRAY_BUFFER, geom.m_vbo);
    
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, (void*)0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    return geom;
}

Geometry GeometryFactory::create_grid()
{
    Geometry geom;
    geom.m_drawmode = EnumDrawMode::PATCHES;

    glGenVertexArrays(1, &geom.m_vao);
    glBindVertexArray(geom.m_vao);

    // GLfloat vertices [][3] = {
    //     {-5.0f, 0.0f, -5.0f}, {-5.0f, 0.0f, 5.0f}, {5.0f, 0.0f, 5.0f}, {5.0f, 0.0f, -5.0f}
    // };

    auto vertices = gen_grid_data(30.0f, 30.0f, 3.0f);

    geom.m_vertices_num = vertices.size();
    geom.m_patch_vertices = 4;
    
    glGenBuffers(1, &geom.m_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, geom.m_vbo);
    std::cout << "vertices size: " << vertices.size() << '\n'
            << "glm::vec3 size: " << sizeof(vertices[0]) << '\n'
            << "total size: " << vertices.size() * sizeof(glm::vec3) << '\n';

    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), vertices.data(), GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, (void*)0);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // glPatchParameteri(GL_PATCH_VERTICES, 4);
    // glDrawArrays(GL_PATCHES, 0, 8);

    return geom;
}

std::vector<glm::vec3> GeometryFactory::gen_grid_data(float width, float height, float quad_size)
{
    std::vector<glm::vec3> data;
    size_t data_size = (size_t) (width / quad_size);
    if (data_size == 1) {
        data.emplace_back(glm::vec3(-width * 0.5f, 0.0f, -height * 0.5f));
        data.emplace_back(glm::vec3(-width * 0.5f, 0.0f, height * 0.5f));
        data.emplace_back(glm::vec3(width * 0.5f, 0.0f, height * 0.5f));
        data.emplace_back(glm::vec3(width * 0.5f, 0.0f, -height * 0.5f));
        return data;
    } else {

        data.reserve(data_size * data_size);
        float x_base = 0.0f - width / 2.0f;
        float z_base = 0.0f - height / 2.0f;


        float x = 0.0f;
        float z = 0.0f;
        int next_row = 0;
        int next_col = 0;
        for (int r = 0; r < data_size - 1; r++) {
            for (int c = 0; c < data_size - 1; c++) {
                // next row
                next_row = r + 1;
                next_col = c + 1; // 1 2 3 4
                // 1 3 4 2
                // p1
                x = x_base + float(c) * quad_size;
                z = z_base + float(r) * quad_size;
                data.emplace_back(glm::vec3(x, 0.0f, z));
                // p2
                x = x_base + c * quad_size;
                z = z_base + next_row * quad_size;
                data.emplace_back(glm::vec3(x, 0.0f, z));
                // p3
                x = x_base + next_col * quad_size;
                z = z_base + next_row * quad_size;
                data.emplace_back(glm::vec3(x, 0.0f, z));
                // p4
                x = x_base + next_col * quad_size;
                z = z_base + r * quad_size;
                data.emplace_back(glm::vec3(x, 0.0f, z));
            }
        }
        return data;
    }

}

}