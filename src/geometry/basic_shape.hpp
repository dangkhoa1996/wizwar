#pragma once
#include <GL/glew.h>
#include <cstdio>
#include <glm/glm.hpp>
#include <vector>

namespace Engine
{
namespace BasicShape 
{
    static GLuint s_cube_vbo;
    static GLuint s_plane_vbo;
    // static GLuint s_ui_plane_vbo;
    static GLuint s_skybox_vbo;
    static GLuint s_cone_vbo;
    static GLuint s_cylinder_vbo;
    static GLuint s_sphere_vbo;
    static GLuint s_round_plane_vbo;
    // add vao but maybe later

    static GLuint s_cone_ebo;
    static GLuint s_cylinder_ebo;
    static GLuint s_sphere_ebo;

    GLuint create_cube_vbo();
    GLuint create_plane_vbo();
    GLuint create_sphere_vbo();
    GLuint create_skybox_vbo();
    
    // cone
    GLuint create_cone_vbo(uint32_t faces = 40);

    GLuint create_cylinder_vbo(uint32_t faces = 40);
    GLuint create_sphere_vbo(uint32_t v_slices, uint32_t h_slices);
    GLuint create_round_plane_vbo(uint32_t faces = 40);

    GLuint create_cone_ebo(uint32_t faces);
    GLuint create_cylinder_ebo(uint32_t faces);
    GLuint create_sphere_ebo();

    void setting_attrib_vbo(GLuint vbo);
}

}
