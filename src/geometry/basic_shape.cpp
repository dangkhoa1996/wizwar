#include "basic_shape.hpp"
#include <glm/gtc/constants.hpp>
#include <iostream>

namespace Engine
{
namespace BasicShape
{
    // this is only true for geometry
    const unsigned int POS_ATTRIB_LOCATION = 0;
    const unsigned int NORM_ATTRIB_LOCATION = 1;
    const unsigned int TEXCOORD_ATTRIB_LOCATION = 2;
    GLuint create_cube_vbo()
    {
        if (s_cube_vbo != 0) return s_cube_vbo;
        // clang-format off
        const float vertices[288] = {
            // pos               // normal          // texCoord
            -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 
            1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 
            1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 
            1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f, 
            -1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 
            -1.0f, -1.0f,-1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,

            -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 
            1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
            -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,

            -1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
            -1.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
            -1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
            -1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
            -1.0f, -1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
            -1.0f, 1.0f,1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,

            1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
            1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
            1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
            1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
            1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
            1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,

            -1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f,
            1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f,
            1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
            1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,
            -1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f,

            -1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
            1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
            -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
            -1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f
        };
        // clang-format on
        glGenBuffers(1, &s_cube_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, s_cube_vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        return s_cube_vbo;
    }

    // GLuint create_ui_plane_vbo() 
    // {
    //     if (s_ui_plane_vbo != 0) return s_ui_plane_vbo;
    //     const float vertices[] = {
            
    //     }
    // }

    GLuint create_plane_vbo()
    {
        if (s_plane_vbo != 0) return s_plane_vbo;
        // clang-format off
        const float vertices[48] = {
            0.0f, 1.0f,  1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 
            0.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 
            0.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
       
            0.0f, 1.0f,   1.0f,0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 
            0.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 
            0.0f, 1.0f,  -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f };
        // clang-format on
        glGenBuffers(1, &s_plane_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, s_plane_vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        return s_plane_vbo;
    }

    GLuint create_skybox_vbo()
    {
        if (s_skybox_vbo != 0) return s_skybox_vbo;
        // clang-format off
        const float vertices[108] = {
            // positions          
            -1.0f,  1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            1.0f,  1.0f, -1.0f,
            -1.0f,  1.0f, -1.0f,

            -1.0f, -1.0f,  1.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f,  1.0f, -1.0f,
            -1.0f,  1.0f, -1.0f,
            -1.0f,  1.0f,  1.0f,
            -1.0f, -1.0f,  1.0f,

            1.0f, -1.0f, -1.0f,
            1.0f, -1.0f,  1.0f,
            1.0f,  1.0f,  1.0f,
            1.0f,  1.0f,  1.0f,
            1.0f,  1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,

            -1.0f, -1.0f,  1.0f,
            -1.0f,  1.0f,  1.0f,
            1.0f,  1.0f,  1.0f,
            1.0f,  1.0f,  1.0f,
            1.0f, -1.0f,  1.0f,
            -1.0f, -1.0f,  1.0f,

            -1.0f,  1.0f, -1.0f,
            1.0f,  1.0f, -1.0f,
            1.0f,  1.0f,  1.0f,
            1.0f,  1.0f,  1.0f,
            -1.0f,  1.0f,  1.0f,
            -1.0f,  1.0f, -1.0f,

            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f,  1.0f,
            1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f,  1.0f,
            1.0f, -1.0f,  1.0f
        };
        // clang-format on
        glGenBuffers(1, &s_skybox_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, s_skybox_vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        return s_skybox_vbo;
    }

    GLuint create_cone_vbo(uint32_t faces)
    {
        if (s_cone_vbo) return s_cone_vbo;
        // need one vertex for top vertex
        std::vector<glm::vec3> vertices_arr;
        vertices_arr.reserve(faces + 1);
        // default height is 1.0
        vertices_arr.emplace_back(glm::vec3(0.0f, 1.0f, 0.0f));
        // default raidus is 1.0
        float x, z;
        float rad_per_face = glm::two_pi<float>() / faces;
        float rad;
        for (size_t i = 1; i < faces + 1; i++) {
            rad = (i - 1) * rad_per_face;
            x = glm::cos(rad);
            z = glm::sin(rad);

            vertices_arr.emplace_back(glm::vec3(x, 0.0f, z));
        }

        // normal
        std::vector<glm::vec3> normals;
        normals.reserve(faces + 1);
        normals.emplace_back(glm::vec3(0.0f, 1.0f, 0.0f));
        glm::vec3 v;
        for (size_t i = 1; i <= faces; i++) {
            v = vertices_arr[i];
            normals.emplace_back(v);
        }

        glGenBuffers(1, &s_cone_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, s_cone_vbo);

        size_t vertices_buffer_size = vertices_arr.size() * sizeof(glm::vec3);

        glBufferData(GL_ARRAY_BUFFER, 2 * vertices_buffer_size, NULL, GL_STATIC_DRAW);
        glBufferSubData(GL_ARRAY_BUFFER, 0, vertices_buffer_size, vertices_arr.data());
        glBufferSubData(GL_ARRAY_BUFFER, vertices_buffer_size, vertices_buffer_size, normals.data());
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        return s_cone_vbo;
    }

    GLuint create_cone_ebo(GLuint faces)
    {
        if (s_cone_ebo) return s_cone_ebo;

        auto arr_sz = faces * 6 - 6;
        uint32_t* indices = new uint32_t[arr_sz];

        size_t i = 0;
        for (; i < faces - 1; i++) {
            indices[i * 3] = 0;
            indices[i * 3 + 1] = i + 1;
            indices[i * 3 + 2] = i + 2;
        }

        indices[i * 3] = 0;
        indices[i * 3 + 1] = faces;
        indices[i * 3 + 2] = 1;
        i++;

        size_t k = i;
        for (size_t j = 1; j < faces - 1; j++) {
            indices[3 * k] = 1;
            indices[3 * k + 1] = j + 1;
            indices[3 * k + 2] = j + 2;
            k++;
        }

        glGenBuffers(1, &s_cone_ebo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, s_cone_ebo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, arr_sz * sizeof(uint32_t), indices, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        delete[] indices;

        return s_cone_ebo;
    }

    GLuint create_cylinder_vbo(uint32_t faces)
    {
        if (s_cylinder_vbo) return s_cylinder_vbo;

        uint32_t arr_sz = faces * 2;
        std::vector<glm::vec3> vertices(arr_sz);
        std::vector<glm::vec3> normals(arr_sz);
        // vertices.reserve(arr_sz);

        float x, z;
        size_t i = 0;

        for (; i < faces; i++) {
            x = glm::cos(i * (glm::two_pi<float>() / faces));
            z = glm::sin(i * (glm::two_pi<float>() / faces));

            vertices[i] = glm::vec3(x, 0.0f, z);
            normals[i] = glm::vec3(x, 0.0f, z);

            vertices[i + faces] = glm::vec3(x, 1.0f, z);
            normals[i + faces] = glm::vec3(x, 1.0f, z);
        }

        glGenBuffers(1, &s_cylinder_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, s_cylinder_vbo);
        size_t vertices_buffer_size = vertices.size() * sizeof(glm::vec3);

        glBufferData(GL_ARRAY_BUFFER, 2 * vertices_buffer_size, NULL, GL_STATIC_DRAW);
        glBufferSubData(GL_ARRAY_BUFFER, 0, vertices_buffer_size, vertices.data());
        glBufferSubData(GL_ARRAY_BUFFER, vertices_buffer_size, vertices_buffer_size, normals.data());
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        return s_cylinder_vbo;
    }

    GLuint create_cylinder_ebo(uint32_t faces)
    {
        if (s_cylinder_ebo) return s_cylinder_ebo;

        size_t arr_sz = faces * 12 - 12;

        uint32_t* indices = new uint32_t[arr_sz];

        size_t i = 0;
        for (;i < faces - 1; i++) {
            indices[i * 6] = i;
            indices[i * 6 + 1] = i + faces;
            indices[i * 6 + 2] = i + faces + 1;

            indices[i * 6 + 3] = i;
            indices[i * 6 + 4] = i + 1;
            indices[i * 6 + 5] = i + faces + 1;
        }

        indices[i * 6] = faces - 1;
        indices[i * 6 + 1] = faces;
        indices[i * 6 + 2] = faces + faces - 1;

        indices[i * 6 + 3] = faces - 1;
        indices[i * 6 + 4] = faces;
        indices[i * 6 + 5] = 0;


        size_t k = i * 6 + 6;
        for (size_t j = 0; j < faces - 2; j++) {
            indices[k + j * 3] = 0;
            indices[k + j * 3 + 1] = j + 1;
            indices[k + j * 3 + 2] = j + 2;
        }

        k += 3 * (faces - 2);
        for (size_t j = 0; j < faces - 2; j++) {
            indices[k + j * 3] = faces;
            indices[k + j * 3 + 1] = faces + j + 1;
            indices[k + j * 3 + 2] = faces + j + 2;
        }

        glGenBuffers(1, &s_cylinder_ebo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, s_cylinder_ebo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, arr_sz * sizeof(uint32_t), indices, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        return s_cylinder_ebo;
    }

    GLuint create_sphere_vbo(uint32_t v_slices, uint32_t h_slices)
    {
        // TODO: add sphere vbo
        // if (s_sphere_vbo) return s_sphere_vbo;

        // float* vertices = new float[v_slices * h_slices];

        return 0;
    }

    GLuint create_round_plane_vbo(uint32_t faces)
    {
        if (s_round_plane_vbo) return s_round_plane_vbo;

        float* vertices = new float[3 * faces];
        float x, z;
        float per_faces = glm::two_pi<float>() / faces;
        for (size_t i = 0; i < faces; i++) {
            x = glm::cos(i * per_faces);
            z = glm::sin(i * per_faces);

            vertices[i * 3] = x;
            vertices[i * 3 + 1] = 0.0f;
            vertices[i * 3 + 2] = z;
        }

        glGenBuffers(1, &s_round_plane_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, s_round_plane_vbo);
        glBufferData(GL_ARRAY_BUFFER, 3 * faces * sizeof(float), vertices, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        return s_round_plane_vbo;
    }

    void setting_attrib_vbo(GLuint vbo)
    {
        glBindBuffer(GL_ARRAY_BUFFER, vbo);

        glEnableVertexAttribArray(POS_ATTRIB_LOCATION);
        glVertexAttribPointer(POS_ATTRIB_LOCATION, 3, GL_FLOAT, false, sizeof(float) * 8, (void*)(0));

        glEnableVertexAttribArray(NORM_ATTRIB_LOCATION);
        glVertexAttribPointer(NORM_ATTRIB_LOCATION, 3, GL_FLOAT, false, sizeof(float) * 8, (void*)(sizeof(float) * 3));
        
        glEnableVertexAttribArray(TEXCOORD_ATTRIB_LOCATION);
        glVertexAttribPointer(TEXCOORD_ATTRIB_LOCATION, 2, GL_FLOAT, false, sizeof(float) * 8, (void*)(sizeof(float) * 6));

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }


} // namespace BasicShape

} // namespace Engine

