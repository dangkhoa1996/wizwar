#pragma once

#include "geometry.hpp"
#include <vector>

namespace Engine
{

class GeometryFactory
{
public:
    GeometryFactory();
    ~GeometryFactory();

    static GeometryFactory& get_instance() {
        if (!m_pInstance) {
            m_pInstance = new GeometryFactory();
        }
        return *m_pInstance;
    }

    Geometry create_cube();
    Geometry create_cone();
    Geometry create_cylinder();
    Geometry create_plane();
    Geometry create_round_plane();
    Geometry create_grid();

    std::vector<glm::vec3> gen_grid_data(float width, float height, float quad_size);
private:
    static GeometryFactory* m_pInstance;

};

}