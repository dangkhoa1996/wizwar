#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <string>
#include <vector>

#include "Camera/free_camera.h"
#include "Camera/abstract_camera.h"
#include "Camera/target_camera.h"

#include "scene/graph_scene.hpp"
#include "scene/test_scene.hpp"
#include "scene/wave_scene.hpp"
#include "scene/model_scene.hpp"

#include "systems/input/keyboard_input.hpp"
#include "systems/input/mouse_input.hpp"

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"


namespace Engine
{

class Game
{
public:
    Game();
    ~Game();

    void init(std::string&& name);
    void update(double delta_time);
    void pre_render();
    void render(double delta_time);
    void exit();

    bool is_running();

    GLFWwindow* m_window;
    GLFWmonitor* m_monitor;
private:
    // examples from learnopengl
    Scene::IScene* m_scene;
    unsigned int m_cube_map_texture_id;

    int m_screen_width  = 1920;
    int m_screen_height = 1080;

    bool show_demo_window = true;
    bool show_another_window = false;
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.0f);

    int n = 20;
    int i = 0;

    static void keyboard_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
    static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
    static void cursor_pos_callback(GLFWwindow* window, double xpos, double ypos);
    static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);

    void process_input(double deltaTime);
    void setup_window_context(std::string& name);
    void setup_examples();

};

} // Engine
