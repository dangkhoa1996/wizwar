#pragma once
#include <memory>
#include <gl/glew.h>
#include <GLFW/glfw3.h>

namespace Engine
{

class MouseInput
{
public:
    MouseInput();
    ~MouseInput();

    double m_x_pos;
    double m_y_pos;
    double m_scroll_offset;

    int left_button_state;
    int right_button_state;
    int middle_button_state;

    bool m_scroll_state;

    static MouseInput* get_instance() {
        if (!m_instance) {
            m_instance = new MouseInput();
        }
        return m_instance;
    }

    void update_button_state(int button, int state);
    void update_cursor(double x_pos, double y_pos);
    void update_scroll(double offset);
private:
    static MouseInput* m_instance;
};

}