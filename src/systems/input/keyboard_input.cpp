#include "keyboard_input.hpp"

namespace Engine
{
KeyboardInput* KeyboardInput::m_instance;
bool KeyboardInput::m_is_cam_locked = false;

KeyboardInput::KeyboardInput()
{

}

KeyboardInput::~KeyboardInput()
{

}

void KeyboardInput::add_event(int key, int action)
{
    m_keys_state[key] = action;
}

int KeyboardInput::get_key_state(int key)
{
    if (m_keys_state.find(key) != m_keys_state.end()) {
        auto state = m_keys_state[key];
        // m_keys_state[key] = ACTION_UNKOWN;
        return state;
    }
    return ACTION_UNKOWN;
}

// event model
// 

}