#pragma once
#include <GLFW/glfw3.h>
#include <map>
#define ACTION_UNKOWN GLFW_KEY_UNKNOWN

namespace Engine
{

struct ButtonData {
    int m_key = GLFW_KEY_UNKNOWN;
    int m_action = ACTION_UNKOWN;
    ButtonData(int key, int action) {
        m_key = key;
        m_action = action;
    }
};

class KeyboardInput
{
public:
    KeyboardInput();
    ~KeyboardInput();

    static KeyboardInput* get_instance() {
        if (!m_instance) {
            m_instance = new KeyboardInput();
        }
        return m_instance;
    }

    std::map<int, int> m_keys_state;

    void add_event(int key, int action);
    int get_key_state(int key);
    static bool m_is_cam_locked;

private:
    static KeyboardInput* m_instance;
};

}