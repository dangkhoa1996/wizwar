#include "mouse_input.hpp"

namespace Engine
{
MouseInput* MouseInput::m_instance;

MouseInput::MouseInput() : m_scroll_state(false),
    m_x_pos(0.0),
    m_y_pos(0.0),
    m_scroll_offset(0.0)
{

}

MouseInput::~MouseInput()
{
    delete m_instance;
}

void MouseInput::update_cursor(double xpos, double ypos)
{
    m_x_pos = xpos;
    m_y_pos = ypos; 
}

void MouseInput::update_scroll(double offset)
{
    m_scroll_state = !m_scroll_state;
    m_scroll_offset = offset;
}

void MouseInput::update_button_state(int button, int state)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        left_button_state = state;
    } else if (button == GLFW_MOUSE_BUTTON_MIDDLE) {
        middle_button_state = state;
    } else if (button == GLFW_MOUSE_BUTTON_RIGHT) {
        right_button_state = state;
    }
}

}