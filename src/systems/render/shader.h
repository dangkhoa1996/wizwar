#pragma once
#include <GL/glew.h>
#include <string>

namespace Engine
{
enum ShaderType
{
    VERTEX   = GL_VERTEX_SHADER,
    GEOMETRY = GL_GEOMETRY_SHADER,
    TESS_CONTROL = GL_TESS_CONTROL_SHADER,
    TESS_EVAL = GL_TESS_EVALUATION_SHADER,
    FRAGMENT = GL_FRAGMENT_SHADER
};

class Shader
{
public:
    Shader(ShaderType type, const char* file_path);
    ~Shader();

    GLuint m_shader;
    ShaderType m_type;
    std::string m_file_path;

    void delete_shader();
    void append_flag(const char* flag);

private:
    void compile(const char* file_path);
    std::string read_from_file(const char* file_path);
};
}
