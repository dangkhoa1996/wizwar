#include "light.hpp"
#include <string>
#include <sstream>


namespace Engine 
{

DirectionalLight::DirectionalLight()
{
    
}

DirectionalLight::DirectionalLight(glm::vec3 color, glm::vec3 ambient, glm::vec3 direction, bool is_enabled) :
    m_ambient(ambient),
    m_color(color),
    m_direction(direction),
    m_is_enabled(is_enabled)
{

}

DirectionalLight::~DirectionalLight()
{

}

void DirectionalLight::pass_to_shader(ShaderProgram& shader_program, size_t index)
{
    std::stringstream ss;
    std::string prefix;
    shader_program.use();
    ss << "u_directional_lights[" << index << "].";
    prefix = ss.str();
    shader_program.set_vec3(prefix + "ambient", m_ambient);
    shader_program.set_vec3(prefix + "color", m_color);
    shader_program.set_vec3(prefix + "direction", m_direction);
    shader_program.set_bool(prefix + "is_enabled", m_is_enabled);
    ss.str("");

    shader_program.un_use();
}

}