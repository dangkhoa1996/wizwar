#pragma once
#include <vector>
#include <string>
#include <map>
#include <gl/glew.h>

#include "geometry/geometry.hpp"
#include "geometry/geometry_factory.hpp"
#include "pv_uniform_buffer.hpp"
#include "shader_program.hpp"
#include "light.hpp"
#include "shadow.hpp"

#include "camera/free_camera.h"
#include "camera/target_camera.h"

namespace Engine
{

class Renderer
{
public:
    Renderer();
    ~Renderer();

    static Renderer& get_instance() {
        if (!m_pInstance) {
            m_pInstance = new Renderer();
        }
        return *m_pInstance;
    }

    static void destroy() {
        delete m_pInstance;
    }

    std::map<std::string, ShaderProgram*> m_shader_program_map;
    PV_UniformBuffer* m_pv_uniform_buffer;
    Shadow m_shadow;
    ICamera* m_current_camera;

    void draw(std::vector<Geometry>& geometries, double delta_time);
    
    void pre_render();
    void post_render();
    void render();

    void set_camera(ICamera* camera);
    void add_directional_lights(std::vector<DirectionalLight>& directional_light_arr);
    void add_material(std::vector<Geometry>& geometries);
private:
    static Renderer* m_pInstance;

    void init_shaders();
    void render_shadow();
};

}