#include "shadow.hpp"

namespace Engine
{

Shadow::Shadow()
{
    init_depth_texture();
    init_depth_frame_buffer();
    init_shader_program();
    init_shadow_matrix();
}

Shadow::~Shadow()
{
    glDeleteTextures(1, &depth_texture);
    glDeleteFramebuffers(1, &depth_fbo);
}

void Shadow::init_depth_texture()
{
    glGenTextures(1, &depth_texture);
    glBindTexture(GL_TEXTURE_2D, depth_texture);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, 
                 DEPTH_TEXTURE_WIDTH, DEPTH_TEXTURE_HEIGHT, 
                 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBindTexture(GL_TEXTURE_2D, 0);
}


void Shadow::init_depth_frame_buffer()
{
    glGenFramebuffers(1, &depth_fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, depth_fbo);

    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depth_texture, 0);
    glDrawBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Shadow::init_shader_program()
{
    m_shader_program = new ShaderProgram(
        Shader { ShaderType::VERTEX, "../shader/vertex/shadow_map.vs"},
        Shader { ShaderType::FRAGMENT, "../shader/fragment/shadow_map.fs"}
    );

    m_shader_program->attach();
    m_shader_program->bind_attrib_location(0, "a_pos");
    m_shader_program->link();
    m_shader_program->use();
}

void Shadow::init_shadow_matrix()
{
    // need to parameterize these
    light_view_matrix = glm::lookAt(glm::vec3(50.0, 100.0, 0.0), glm::vec3(0.0, 0.0, 0.0), glm::vec3(0.0, 1.0, 0.0));
    light_projection_matrix = glm::frustum(-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 500.f);
    vp = light_projection_matrix * light_view_matrix;
    m_shader_program->set_mat4("u_vp_mat", vp);

    scale_bias_matrix = glm::mat4(glm::vec4(0.5f, 0.0f, 0.0f, 0.0f),
                                  glm::vec4(0.0f, 0.5f, 0.0f, 0.0f),
                                  glm::vec4(0.0f, 0.0f, 0.5f, 0.0f),
                                  glm::vec4(0.5f, 0.5f, 0.5f, 1.0f));

    shadow_matrix = scale_bias_matrix * light_projection_matrix * light_view_matrix;
}

}