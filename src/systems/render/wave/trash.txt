    void WaveSystem::init_bump_tex()
    {
        glGenTextures(1, &m_bump_tex);
        glBindTexture(GL_TEXTURE_2D, m_bump_tex);

        glTexImage2D(
            GL_TEXTURE_2D,    // target
            0,                // level
            GL_RGBA,          // internal format
            BUMP_TEX_SIZE,    // width
            BUMP_TEX_SIZE,    // height
            0,                // border
            GL_RGBA,          // format
            GL_UNSIGNED_BYTE, // type
            NULL              // data
        );
        glGenerateMipmap(GL_TEXTURE_2D);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LOD, 0);
        // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        ERROR_DETECT
        glBindTexture(GL_TEXTURE_2D, 0);
    }


 void WaveSystem::create_bias_noise_map()
    {
        glGenTextures(1, &m_bias_noise_map);
        glBindTexture(GL_TEXTURE_2D, m_bias_noise_map);
        if (glIsTexture(m_bias_noise_map))
        {
            unsigned long** data;
            float x         = 0;
            float y         = 0;
            unsigned char r = 0;
            unsigned char g = 0;
            for (size_t i = 0; i < BUMP_TEX_SIZE; i++)
            {
                data[i] = new unsigned long[BUMP_TEX_SIZE];
                for (size_t j = 0; j < BUMP_TEX_SIZE; j++)
                {
                    x = rand_zero_to_one();
                    y = rand_zero_to_one();
                    r = (unsigned char)(x * 255.999f);
                    g = (unsigned char)(y * 255.999f);

                    data[i][j] = (r << 24)
                        | (g << 16)
                        | (0xff << 8)
                        | 0xff;
                }
            }

            glTexSubImage2D(
                GL_TEXTURE_2D,
                0,
                0,
                0,
                GLsizei(BUMP_TEX_SIZE),
                GLsizei(BUMP_TEX_SIZE),
                GL_RGBA8,
                GL_UNSIGNED_BYTE,
                data);
        }
    }


    void WaveSystem::init_framebuffer()
    {
        glGenFramebuffers(1, &m_framebuffer);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_framebuffer);
        glFramebufferTexture2D(
            GL_FRAMEBUFFER,       //target
            GL_COLOR_ATTACHMENT0, // attachment
            GL_TEXTURE_2D,        // texturetarget
            m_bump_tex,           // texture
            0                     // level
        );
    }