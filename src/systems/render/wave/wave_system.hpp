#pragma once
#include <random>
#include <sstream>

#include <gl/glew.h>
#include <glm/glm.hpp>

#include "geometry/geometry.hpp"
#include "geometry/geometry_factory.hpp"

#include "camera/free_camera.h"
#include "camera/target_camera.h"

namespace Engine
{
    struct Wave {
        glm::vec2 m_dir;
        float m_len;
        float m_amp;
        float m_phase;
        float m_freq;
        float m_speed;
    };

    class WaveSystem
    {
    public:
        WaveSystem();
        ~WaveSystem();

        void init_shaders();
        void init_waves();
        void init_tex_waves();

        void update_waves(double delta_time);
        void render_waves(ICamera* current_camera, GLuint& refract_tex, GLuint& reflect_tex);
        void render_bump_map();
        void render_normal();
        void draw();

        void update_wave_params(int index, float dir, float amp, float len, float speed);
        void update_tex_wave_params(int index, float dir, float amp, float len, float speed);

        ShaderProgram* m_shader_program;
        ShaderProgram* m_bump_shader_program;
        ShaderProgram* m_normal_shader_program;
        GLuint m_framebuffer;
        GLuint m_bump_tex;

    private:
        Geometry m_grid;

        std::vector<Wave> m_waves;
        std::vector<Wave> m_tex_waves;

        int m_steepness;
        const int MAX_GEOMETRIES_NUM = 100;
        const int MAX_LIGHTS_NUM     = 10;
        const int MAX_WAVES          = 3;
        const int MAX_TEX_WAVES      = 16;
        const int BUMP_TEX_SIZE      = 256;
        const float MAX_WAVE_LEN     = 10.0f;
        const float MIN_WAVE_LEN     = 5.0f;
        const float MAX_AMP          = 5.0f;
        const float MIN_AMP          = 1.0f; 
        const float GRAVITY_CONST    = 30.0f;
        const float PI               = glm::pi<float>();
        const float STEEPNESS = 0.4f; 
        std::stringstream ss;


        float rand_minus_one_to_one();
        float rand_zero_to_one();
    };

}