#include "wave_system.hpp"
#define ERROR_DETECT                                                     \
    auto error = glGetError();                                           \
    if (error != GL_NO_ERROR)                                            \
    {                                                                    \
        std::cout << "error " << error << " line: " << __LINE__ << '\n'; \
    }

#include <sstream>

namespace Engine
{
    WaveSystem::WaveSystem()
    {
        init_shaders();
        init_waves();
        init_tex_waves();
        m_grid = GeometryFactory::get_instance().create_grid();
        m_grid.m_transform.set_position(glm::vec3(5.0f, 0.0f, 5.0f));
        m_grid.m_material.m_diffuse = glm::vec3(0.0f, 0.4f, 0.4f);
        m_grid.m_material.m_ambient = glm::vec3(0.0f, 0.0f, 0.0f);
        m_grid.m_material.m_specular = glm::vec3(0.1f, 0.1f, 0.1f);
        m_grid.m_material.m_specular_strength = 0.8f;
        m_grid.m_material.m_shininess = 2.0f;
    }

    WaveSystem::~WaveSystem()
    {
        delete m_shader_program;
    }

    void WaveSystem::init_waves()
    {
        m_waves.reserve(MAX_WAVES);
        float dirX;
        float dirY;
        float len;
        float amp;
        float phase;
        float freq;
        float speed;
        float rot_base = PI / 180.0f;
        for (int i = 0; i < MAX_WAVES; i++) 
        {
            float rads = rand_zero_to_one() * 360.0f * rot_base;
            dirX = cos(rads);
            dirY = sin(rads);

            Wave wave{};
            wave.m_dir = glm::vec2(dirX, dirY);
            wave.m_len = MIN_WAVE_LEN + rand_zero_to_one() * (MAX_WAVE_LEN - MIN_WAVE_LEN);
            wave.m_speed = wave.m_len + rand_zero_to_one() * wave.m_len;
            wave.m_phase = wave.m_speed / (2 * wave.m_len);
            wave.m_freq = sqrt(GRAVITY_CONST * 2 * PI / wave.m_len);
            wave.m_amp = MIN_AMP + rand_zero_to_one() * (MAX_AMP - MIN_AMP);
            m_waves.emplace_back(wave);
        }
    }

    void WaveSystem::init_tex_waves() 
    {
        m_tex_waves.reserve(MAX_TEX_WAVES);
        float dirX;
        float dirY;
        float len;
        float amp;
        float phase;
        float freq;
        float speed;
        float rot_base = PI / 180.0f;
        for (int i = 0; i < MAX_TEX_WAVES; i++) 
        {
            float rads = rand_zero_to_one() * 360.0f * rot_base;
            dirX = cos(rads);
            dirY = sin(rads);

            Wave wave{};
            wave.m_dir = glm::vec2(dirX, dirY);
            wave.m_len = MIN_WAVE_LEN + rand_zero_to_one() * (MAX_WAVE_LEN - MIN_WAVE_LEN);
            wave.m_speed = rand_zero_to_one();
            wave.m_phase = wave.m_speed / (2 * wave.m_len);
            wave.m_freq = sqrt(GRAVITY_CONST * 2 * PI / wave.m_len);
            wave.m_amp = MIN_AMP + rand_zero_to_one() * (MAX_AMP - MIN_AMP);
            m_tex_waves.emplace_back(wave);
        }


        glGenTextures(1, &m_bump_tex);
        glBindTexture(GL_TEXTURE_2D, m_bump_tex);

        glTexImage2D(
            GL_TEXTURE_2D,    // target
            0,                // level
            GL_RGBA,          // internal format
            BUMP_TEX_SIZE,    // width
            BUMP_TEX_SIZE,    // height
            0,                // border
            GL_RGBA,          // format
            GL_UNSIGNED_BYTE, // type
            NULL              // data
        );
        glGenerateMipmap(GL_TEXTURE_2D);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LOD, 0);
        // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        ERROR_DETECT
        glBindTexture(GL_TEXTURE_2D, 0);

        // setup framebuffer
        glGenFramebuffers(1, &m_framebuffer);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_framebuffer);
        glFramebufferTexture2D(
            GL_FRAMEBUFFER,       //target
            GL_COLOR_ATTACHMENT0, // attachment
            GL_TEXTURE_2D,        // texturetarget
            m_bump_tex,           // texture
            0                     // level
        );
    }

    void WaveSystem::update_wave_params(int index, float dir, float amp, float len, float speed)
    {
        auto& wave = m_waves[index];
        wave.m_dir.x = cos(dir);
        wave.m_dir.y = sin(dir);
        wave.m_len = len;
        wave.m_speed = speed;
        wave.m_freq = sqrt(GRAVITY_CONST * 2 * PI * speed / len);
        // wave.m_phase = wave.m_speed / (2 * wave.m_len);
        wave.m_amp = amp;
        // std::cout << "dir_" << index << " " << dir << '\n';
    }

    void WaveSystem::update_tex_wave_params(int index, float dir, float amp, float len, float speed)
    {
        auto& wave = m_tex_waves[index];
        wave.m_dir.x = cos(dir);
        wave.m_dir.y = sin(dir);
        wave.m_len = len;
        wave.m_freq = sqrt(GRAVITY_CONST * 2 * PI / len);
        // wave.m_phase = 0.0f;
        wave.m_amp = amp;
        wave.m_speed = speed;
    }

    void WaveSystem::init_shaders()
    {
        m_shader_program = new ShaderProgram(
            std::vector<Shader> {
                Shader { VERTEX, "../shader/vertex/wave.vs" },
                Shader { TESS_CONTROL, "../shader/tessellation/tess_control.tesc" },
                Shader { TESS_EVAL, "../shader/tessellation/tess_eval.tese" },
                Shader { GEOMETRY, "../shader/geometry/wave.geom" },
                Shader { FRAGMENT, "../shader/fragment/wave.fs" } });

        m_shader_program->attach();
        m_shader_program->bind_attrib_location(0, "a_pos");
        m_shader_program->link();

        m_bump_shader_program = new ShaderProgram(
            std::vector<Shader> {
                Shader { VERTEX, "../shader/vertex/gen_bump.vs" },
                Shader { TESS_CONTROL, "../shader/tessellation/tess_control.tesc" },
                Shader { TESS_EVAL, "../shader/tessellation/tess_eval.tese" },
                Shader { GEOMETRY, "../shader/geometry/gen_bump.geom" },
                Shader { FRAGMENT, "../shader/fragment/gen_bump.fs" } }
        );

        m_bump_shader_program->attach();
        m_bump_shader_program->bind_attrib_location(0, "a_pos");
        m_bump_shader_program->link();

        m_normal_shader_program = new ShaderProgram(
            std::vector<Shader> {
                Shader { VERTEX, "../shader/vertex/gen_bump.vs" },
                Shader { TESS_CONTROL, "../shader/tessellation/tess_control.tesc" },
                Shader { TESS_EVAL, "../shader/tessellation/tess_eval.tese" },
                Shader { GEOMETRY, "../shader/geometry/normal.geom" },
                Shader { FRAGMENT, "../shader/fragment/blue.fs" }
            }
        );

        m_normal_shader_program->attach();
        m_normal_shader_program->bind_attrib_location(0, "a_pos");
        m_normal_shader_program->link();
    }

    void WaveSystem::render_waves(ICamera* current_camera, GLuint& refract_tex, GLuint& reflect_tex)
    {
        m_shader_program->use();
        m_shader_program->set_vec3("u_camera_position", current_camera->m_position);
        m_shader_program->set_float("u_steepness", STEEPNESS);
        
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, refract_tex);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, reflect_tex);
        m_shader_program->set_texture("u_refract_tex", 0);
        m_shader_program->set_texture("u_reflect_tex", 1);

        for (int i = 0; i < MAX_WAVES; i++) {
            ss << "u_waves[" << i << "].dir";
            m_shader_program->set_vec2(ss.str(), m_waves[i].m_dir);
            ss.str("");

            ss << "u_waves[" << i << "].freq";
            m_shader_program->set_float(ss.str(), m_waves[i].m_freq);
            ss.str("");

            ss << "u_waves[" << i << "].phase";
            m_shader_program->set_float(ss.str(), m_waves[i].m_phase);
            ss.str("");

            ss << "u_waves[" << i << "].amp";
            m_shader_program->set_float(ss.str(), m_waves[i].m_amp);
            ss.str("");
        }
        
        // glActiveTexture(GL_TEXTURE0);
        // std::cout << "m_bump_tex: " << m_bump_tex << '\n'; 
        // glBindTexture(GL_TEXTURE_2D, m_bump_tex);
        // m_shader_program->set_texture("u_bump_tex", 0);
        m_grid.m_material.pass_to_shader(*m_shader_program);
        m_grid.draw(*m_shader_program);
        m_shader_program->un_use();
    }

    void WaveSystem::render_bump_map() 
    {
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_framebuffer);
        glViewport(0, 0, BUMP_TEX_SIZE, BUMP_TEX_SIZE);
        glClearColor(0.0, 0.0, 0.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT);

        m_bump_shader_program->use();
        m_bump_shader_program->set_float("u_steepness", STEEPNESS);
        for (int i = 0; i < MAX_TEX_WAVES; i++) {
            ss << "u_waves[" << i << "].dir";
            m_bump_shader_program->set_vec2(ss.str(), m_tex_waves[i].m_dir);
            ss.str("");

            ss << "u_waves[" << i << "].freq";
            m_bump_shader_program->set_float(ss.str(), m_tex_waves[i].m_freq);
            ss.str("");

            ss << "u_waves[" << i << "].phase";
            m_bump_shader_program->set_float(ss.str(), m_tex_waves[i].m_phase);
            ss.str("");

            ss << "u_waves[" << i << "].amp";
            m_bump_shader_program->set_float(ss.str(), m_tex_waves[i].m_amp);
            ss.str("");
        }

        m_grid.draw(*m_bump_shader_program);
        m_bump_shader_program->un_use();   
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        ERROR_DETECT
    }

    void WaveSystem::render_normal()
    {
        m_normal_shader_program->use();
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_bump_tex);
        m_normal_shader_program->set_texture("u_bump_tex", 0);
        m_grid.draw(*m_normal_shader_program);
        m_normal_shader_program->un_use();
    }

    void WaveSystem::update_waves(double delta_time)
    {
        for (auto& wave : m_waves)
        {
            wave.m_phase += 0.2f * wave.m_speed * 2 / (wave.m_len * (float)delta_time) ;
            wave.m_phase = fmod(wave.m_phase, 2 * PI);
        }

        for (auto& tex_wave : m_tex_waves) 
        {
            tex_wave.m_phase += 0.002f * tex_wave.m_speed * 2 / (tex_wave.m_len * (float)delta_time);
            tex_wave.m_phase = fmod(tex_wave.m_phase, 2 * PI);
        }
    }

    float WaveSystem::rand_minus_one_to_one()
    {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> dis(-1.0, 1.0);
        return dis(gen);
    }

    float WaveSystem::rand_zero_to_one()
    {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> dis(0.0, 1.0);
        return dis(gen);
    }
}