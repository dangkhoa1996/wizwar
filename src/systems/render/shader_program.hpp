#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <utility>
#include <vector>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>

#include "shader.h"

namespace Engine
{
    class ShaderProgram 
    {
    public:
        ShaderProgram(Shader& vertexShader, Shader& fragmentShader);
        ShaderProgram(std::vector<Shader>& shaders);
        ~ShaderProgram();

        GLuint m_program;
        std::vector<Shader> m_shaders;

        void add_shader(Shader&& shader);
        void attach();
        void link();
        void use();
        void un_use();
        void bind_attrib_location(GLuint index, const char* name);
        GLuint get_attrib_location(const char* name);

        void set_bool(const std::string& name, bool value);
        void set_int(const std::string& name, int value);
        void set_float(const std::string& name, float value);
        void set_vec2(const std::string& name, const glm::vec2& value);
        void set_vec3(const std::string& name, const glm::vec3& value);
        void set_vec4(const std::string& name, const glm::vec4& value);
        void set_mat4(const std::string& name, const glm::mat4& value);

        void set_texture(std::string uniformName, int val);
    };
}
