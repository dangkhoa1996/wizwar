#include "pv_uniform_buffer.hpp"
#include <iostream>

namespace Engine
{

PV_UniformBuffer::PV_UniformBuffer()
{

}

PV_UniformBuffer::~PV_UniformBuffer()
{
    glDeleteBuffers(1, &m_ubo);
}

GLuint PV_UniformBuffer::create_uniform_buffer()
{
    // uniform buffer storing pv mat
    // should there be any other situations require uniform buffer ?
    glGenBuffers(1, &m_ubo);
    glBindBuffer(GL_UNIFORM_BUFFER, m_ubo);
    glBufferData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4), NULL, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    return m_ubo;
}

void PV_UniformBuffer::bind_buffer_to_shader_program(int program_id, const std::string& uniform_name)
{
    glBindBuffer(GL_UNIFORM_BUFFER, m_ubo);
    auto uniform_index = glGetUniformBlockIndex(program_id, uniform_name.c_str());
    glUniformBlockBinding(program_id, uniform_index, m_uniform_binding_pos);
    glBindBufferBase(GL_UNIFORM_BUFFER, m_uniform_binding_pos, m_ubo);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void PV_UniformBuffer::set_buffer_data(glm::mat4& proj_mat, glm::mat4& view_mat)
{
    glBindBuffer(GL_UNIFORM_BUFFER, m_ubo);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), glm::value_ptr(proj_mat));
    glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(view_mat));
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void PV_UniformBuffer::set_view_mat(glm::mat4& view)
{
    glBindBuffer(GL_UNIFORM_BUFFER, m_ubo);
    glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(view));
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void PV_UniformBuffer::set_projection_mat(glm::mat4& projection)
{
    glBindBuffer(GL_UNIFORM_BUFFER, m_ubo);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), glm::value_ptr(projection));
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}



} // namespace Engine
