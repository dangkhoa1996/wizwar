#pragma once
#include <glm/glm.hpp>
#include "shader_program.hpp"

namespace Engine 
{

struct Material {
    glm::vec3 m_emission;
    glm::vec3 m_ambient;
    glm::vec3 m_diffuse;
    float m_specular_strength;
    glm::vec3 m_specular;
    float m_shininess;

    Material() : 
        m_emission(glm::vec3(1.0f)),
        m_ambient(glm::vec3(1.0f)),
        m_diffuse(glm::vec3(1.0f)),
        m_specular(glm::vec3(1.0f)),
        m_specular_strength(1.0f),
        m_shininess(1.0f) {};

    Material(glm::vec3 diffuse) : m_diffuse(diffuse) { }

    Material(glm::vec3 ambient, glm::vec3 diffuse, 
    glm::vec3 emission, glm::vec3 specular, float shininess, float specular_strength) : m_ambient(ambient), 
    m_diffuse(diffuse), 
    m_emission(emission), 
    m_specular(specular),
    m_shininess(shininess) {}

    void pass_to_shader(ShaderProgram& shader_program) 
    {
        shader_program.use();
        shader_program.set_vec3("u_numerical_material.ambient", m_ambient);
        shader_program.set_vec3("u_numerical_material.diffuse", m_diffuse);
        shader_program.set_vec3("u_numerical_material.specular", m_specular);
        shader_program.set_vec3("u_numerical_material.emission", m_emission);
        shader_program.set_float("u_numerical_material.shininess", m_shininess);
        shader_program.set_float("u_numerical_material.specular_strength", m_specular_strength);
        shader_program.un_use();
    }

};

} // namespace Engine