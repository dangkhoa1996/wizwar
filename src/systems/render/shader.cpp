#include "shader.h"
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>


namespace Engine
{
Shader::Shader(ShaderType type, const char* file_path)
    : m_type(type), m_file_path(file_path)
{
    m_shader = glCreateShader(m_type);
    compile(file_path);
}

Shader::~Shader() {}

std::string Shader::read_from_file(const char* file_path)
{
    std::string buffer;
    std::ifstream src_file;
    src_file.open(file_path);
    if (src_file.is_open())
    {
        buffer.assign((std::istreambuf_iterator<char>(src_file)), std::istreambuf_iterator<char>());
    }
    else
    {
        std::cout << "Cannot find shader file path: " << file_path << '\n';
    }
    return buffer;
}

void Shader::append_flag(const char* flag)
{
    auto src = read_from_file(m_file_path.c_str());
    std::stringstream ss;
    ss << flag << src;
}

void Shader::compile(const char* file_path)
{
    auto src          = read_from_file(file_path);
    const char* c_src = src.c_str();
    int src_len       = src.length();
    
    glShaderSource(m_shader, 1, &c_src, &src_len);
    glCompileShader(m_shader);
    
    char info_log[512];
    int success;
    glGetShaderiv(m_shader, GL_COMPILE_STATUS, &success);
    
    if (!success)
    {
        glGetShaderInfoLog(m_shader, sizeof(info_log), NULL, info_log);
        std::cout << "Error shader compiling " << info_log << "\n";
    }
}

void Shader::delete_shader()
{
    glDeleteShader(m_shader);
}

} // namespace Engine
