#pragma once
#include <glm/glm.hpp>
#include <iostream>

#include "shader_program.hpp"

namespace Engine
{


class ILight 
{
public:
    ILight() {};
    ~ILight() {};

    virtual void pass_to_shader(ShaderProgram& shader_program, size_t index) = 0;
};


class DirectionalLight : public ILight {
public:
    DirectionalLight();
    DirectionalLight(glm::vec3 color, glm::vec3 ambient, glm::vec3 direction, bool is_enabled);
    ~DirectionalLight();

    glm::vec3 m_ambient;
    glm::vec3 m_color;
    glm::vec3 m_direction;
    bool m_is_enabled;

    virtual void pass_to_shader(ShaderProgram& shader_program, size_t index) override;
};

class PointLight : public ILight 
{
public:
    PointLight() {};
    ~PointLight() {};


    glm::vec3 color;
    glm::vec3 position;
    float constant_attenuation;
    float linear_attenuation;
    float quadratic_attenuation;
    bool is_enabled;

    virtual void pass_to_shader(ShaderProgram& shader_program, size_t index) override {};
};

class SpotLight : public ILight 
{
public:
    SpotLight() {};
    ~SpotLight() {};


    glm::vec3 color;
    glm::vec3 position;
    float spot_cos_cut_off;
    float spot_exponent;
    glm::vec3 cone_direction;
    bool is_enabled;

    virtual void pass_to_shader(ShaderProgram& shader_program, size_t index) override {};
};

} // namespace Engine