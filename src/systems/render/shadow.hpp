#pragma once
#include <gl/glew.h>
#include "shader_program.hpp"


namespace Engine
{

class Shadow
{
public:
    Shadow();
    ~Shadow();

    GLuint depth_texture;
    GLuint depth_fbo;

    // TODO: move this shader to somewhere else
    ShaderProgram* m_shader_program;

    const int DEPTH_TEXTURE_WIDTH = 1600;
    const int DEPTH_TEXTURE_HEIGHT = 900;

    glm::mat4 scene_model_matrix;
    glm::mat4 light_view_matrix;
    glm::mat4 light_projection_matrix;

    glm::mat4 scale_bias_matrix;
    glm::mat4 shadow_matrix;
    glm::mat4 vp;

    void update();
    
private:
    void init_depth_texture();
    void init_depth_frame_buffer();
    void init_shader_program();
    void init_shadow_matrix();
    
};
} // namespace Engine
