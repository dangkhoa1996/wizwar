#include "shader_program.hpp"
#include <iostream>

namespace Engine
{
    ShaderProgram::ShaderProgram(Shader& vertexShader, Shader& fragmentShader)
    {
        m_program = glCreateProgram();
        m_shaders.reserve(3);

        m_shaders.emplace_back(vertexShader);
        m_shaders.emplace_back(fragmentShader);

    }

    ShaderProgram::ShaderProgram(std::vector<Shader>& shaders)
        : m_shaders(shaders)
    {
        m_program = glCreateProgram();
    }

    ShaderProgram::~ShaderProgram() {}

    void ShaderProgram::use()
    {
        glUseProgram(m_program);
    }

    void ShaderProgram::un_use()
    {
        glUseProgram(0);
    }

    void ShaderProgram::link()
    {
        glLinkProgram(m_program);

        int success;
        glGetProgramiv(m_program, GL_LINK_STATUS, &success);
        if (!success)
        {
            char info_log[512];
            glGetProgramInfoLog(m_program, sizeof(info_log), NULL, info_log);
            std::cout << "Error linking shader program: \n\t Program Id: " << m_program << '\n'
                      << info_log << "\n";
        }
    }

    void ShaderProgram::add_shader(Shader&& shader)
    {
        m_shaders.emplace_back(shader);
    }

    void ShaderProgram::attach()
    {
        for (const auto& shader : m_shaders)
        {
            glAttachShader(m_program, shader.m_shader);
        }
    }

    void ShaderProgram::bind_attrib_location(GLuint index, const char* name)
    {
        if (m_program != 0) 
        {
            glBindAttribLocation(m_program, index, name);
        }
    }

    GLuint ShaderProgram::get_attrib_location(const char* name)
    {
        if (m_program != 0)
        {
            return glGetAttribLocation(m_program, name);
        }
        return -1;
    }

    void ShaderProgram::set_bool(const std::string& name, bool value)
    {
        glUniform1i(glGetUniformLocation(m_program, name.c_str()), value);
    }
    // send uniform value to shader program
    void ShaderProgram::set_int(const std::string& name, int value)
    {
        glUniform1i(glGetUniformLocation(m_program, name.c_str()), value);
    }

    void ShaderProgram::set_float(const std::string& name, float value)
    {
        glUniform1f(glGetUniformLocation(m_program, name.c_str()), value);
    }

    void ShaderProgram::set_vec2(const std::string& name, const glm::vec2& value)
    {
        glUniform2fv(glGetUniformLocation(m_program, name.c_str()), 1, glm::value_ptr(value));
    }

    void ShaderProgram::set_vec3(const std::string& name, const glm::vec3& value)
    {
        glUniform3fv(glGetUniformLocation(m_program, name.c_str()), 1, glm::value_ptr(value));
    }

    void ShaderProgram::set_vec4(const std::string& name, const glm::vec4& value)
    {
        glUniform4fv(glGetUniformLocation(m_program, name.c_str()), 1, glm::value_ptr(value));
    }

    void ShaderProgram::set_mat4(const std::string& name, const glm::mat4& value)
    {
        glUniformMatrix4fv(glGetUniformLocation(m_program, name.c_str()), 1, GL_FALSE, glm::value_ptr(value));
    }

    void ShaderProgram::set_texture(std::string uniformName, int val)
    {
        set_int(uniformName, val);
    }
}
