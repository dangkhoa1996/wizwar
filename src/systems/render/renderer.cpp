#include "renderer.hpp"
#include <iostream>
#include <sstream>
#include <glm/gtx/string_cast.hpp>

namespace Engine
{

Renderer* Renderer::m_pInstance;

Renderer::Renderer() : m_shadow(), m_pv_uniform_buffer(new PV_UniformBuffer())
{
    init_shaders();
}

Renderer::~Renderer()
{
  
}

void Renderer::init_shaders()
{
    ShaderProgram* geom_shader_program = new ShaderProgram(
        Shader {VERTEX, "../shader/vertex/basic_shape.vs"},
        Shader {FRAGMENT, "../shader/fragment/basic_shape.fs" }
    );

    geom_shader_program->attach();
    geom_shader_program->bind_attrib_location(0, "a_pos");
    geom_shader_program->bind_attrib_location(1, "a_normal");
    geom_shader_program->bind_attrib_location(2, "a_texcoord");
    geom_shader_program->link();

    m_shader_program_map["basic_shape"] = geom_shader_program;
    
    m_pv_uniform_buffer->create_uniform_buffer();
    m_pv_uniform_buffer->bind_buffer_to_shader_program(geom_shader_program->m_program, "pv_mat");
}

void Renderer::draw(std::vector<Geometry>& geometries, double delta_time) 
{
    // update_waves
    glClearColor(0.4f, 0.4f, 0.4f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, m_current_camera->m_screen_width, m_current_camera->m_screen_height);
    
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    m_pv_uniform_buffer->set_buffer_data(m_current_camera->m_projection, m_current_camera->m_view);

    auto geom_shader = m_shader_program_map["basic_shape"];
    geom_shader->use();
    geom_shader->set_vec3("u_camera_position", m_current_camera->m_position);
    geometries[1].draw(*geom_shader);
    geom_shader->un_use();
}

void Renderer::add_directional_lights(std::vector<DirectionalLight>& light_arr)
{
    for (size_t i = 0; i < light_arr.size(); i++) {
        for (auto& pair: m_shader_program_map) {
            light_arr[i].pass_to_shader(*pair.second, i);
        }
    }

    for (auto& pair: m_shader_program_map) {
         pair.second->use();
         pair.second->set_int("directional_lights_size", light_arr.size());
         pair.second->un_use();
     }
}
    
void Renderer::set_camera(ICamera* camera)
{
    m_current_camera = camera;
}

void Renderer::add_material(std::vector<Geometry>& geometries)
{
    auto tess_shader = m_shader_program_map["tess_basic_shape"];
    geometries[0].m_material.pass_to_shader(*tess_shader);
    auto geom_shader = m_shader_program_map["basic_shape"];
    geometries[1].m_material.pass_to_shader(*geom_shader);
}

}