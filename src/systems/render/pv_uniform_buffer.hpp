#pragma once 

#include "GL/glew.h"

#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace Engine
{
class PV_UniformBuffer
{
public:
    PV_UniformBuffer();
    ~PV_UniformBuffer();

    GLuint m_ubo;
    GLuint m_uniform_binding_pos = 0;

    GLuint create_uniform_buffer();
    void bind_buffer_to_shader_program(int program_id, const std::string& uniform_name);
    void set_buffer_data(glm::mat4& proj_mat, glm::mat4& view_mat);
    void set_view_mat(glm::mat4& view);
    void set_projection_mat(glm::mat4& projection);

};

} // namespace Engine
