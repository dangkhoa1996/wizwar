#pragma once
#include <physx/PxPhysicsAPI.h>
#include <assimp/scene.h>
#include <vector>

using namespace physx;

namespace Engine
{
using namespace physx;


static PxDefaultErrorCallback g_default_error_callback;
static PxDefaultAllocator g_default_allocator_callback;
extern PxFoundation* g_foundation;
extern PxPhysics* g_physics;
extern PxPvd* g_physics_debug;
extern PxCooking* g_cooking;

extern PxDefaultCpuDispatcher* g_dispatcher;
extern PxScene* g_scene;

extern std::vector<PxVec3> g_contact_positions;
extern std::vector<PxVec3> g_contact_impulses;

// extern PxCudaContextManager* g_cuda_context_manager;

extern PxMaterial* g_material;

void init_pxphysic();
void shutdown();

void step_physics();
PxConvexMesh* create_convex_mesh(const aiScene* scene, bool direct_insertion);

// contact test
void disable_shape_in_contact_tests(PxShape* shape);
void enable_shape_in_contact_tests(PxShape* shape);

// scene query
void disable_shape_in_scene_query_tests(PxShape* shape);
void enable_shape_in_scene_query_tests(PxShape* shape);

PxFilterFlags contact_report_filter_shader(PxFilterObjectAttributes attributes0, PxFilterData filter_data0,
                                           PxFilterObjectAttributes attributes1, PxFilterData filter_data1,
                                           PxPairFlags& pair_flags, const void* constant_block, PxU32 constant_block_size);

} // namespace Engine

