#include "physic.hpp"
#include <iostream>

#include <gl/glew.h>

namespace Engine
{
PxFoundation* g_foundation = nullptr;
PxPhysics* g_physics = nullptr;
PxPvd* g_physics_debug = nullptr;
PxCooking* g_cooking = nullptr;

PxDefaultCpuDispatcher* g_dispatcher = nullptr;
PxScene* g_scene = nullptr;

// PxCudaContextManager* g_cuda_context_manager = nullptr;

PxMaterial* g_material;

const char* PVD_HOST = "127.0.0.1";

class ContactReportCallback : public PxSimulationEventCallback
{

void onConstraintBreak(PxConstraintInfo* constraints, PxU32 count) 
{
    PX_UNUSED(constraints); PX_UNUSED(count);
}

void onWake(PxActor** actors, PxU32 count) 
{
    PX_UNUSED(actors); PX_UNUSED(count);
}

void onSleep(PxActor** actors, PxU32 count)
{
    PX_UNUSED(actors); PX_UNUSED(count);
}

void onTrigger(PxTriggerPair* pairs, PxU32 count)
{
    PX_UNUSED(pairs); PX_UNUSED(count);
}

void onAdvance(const PxRigidBody* const*, const PxTransform*, const PxU32) 
{
    
}

void onContact(const PxContactPairHeader& pairHeader, const PxContactPair* pairs, PxU32 num_pairs)
{
    PX_UNUSED((pairHeader));
    std::vector<PxContactPairPoint> contact_points;

    for (PxU32 i = 0; i < num_pairs; i++)
    {
        PxU32 contact_count = pairs[i].contactCount;
        if (contact_count) {
            contact_points.resize(contact_count);
            pairs[i].extractContacts(&contact_points[0], contact_count);
        }

        for (PxU32 j = 0; j < contact_count; j++) {
            g_contact_positions.push_back(contact_points[i].position);
            g_contact_impulses.push_back(contact_points[i].impulse);
        }
    }    
}

};

ContactReportCallback g_contact_report_callback;

void init_pxphysic()
{
    g_foundation = PxCreateFoundation(PX_PHYSICS_VERSION, g_default_allocator_callback,
    g_default_error_callback);

    if (!g_foundation) std::cout << "PxCreateFoundation failed!\n"; 

    bool record_memory_allocations = true;

    g_physics_debug = PxCreatePvd(*g_foundation);
    PxPvdTransport* transport = PxDefaultPvdSocketTransportCreate(PVD_HOST, 5425, 10);
    g_physics_debug->connect(*transport, PxPvdInstrumentationFlag::eALL);

    g_physics = PxCreatePhysics(PX_PHYSICS_VERSION, *g_foundation, PxTolerancesScale(), record_memory_allocations, g_physics_debug);
    if (!g_physics) std::cout << "PxCreatePhysics failed!\n";

    g_cooking = PxCreateCooking(PX_PHYSICS_VERSION, *g_foundation, PxCookingParams(PxTolerancesScale()));
    if (!g_cooking) std::cout << "PxCreateCooking failed\n";


    // PxCudaContextManagerDesc cuda_context_manager_desc;
    // cuda_context_manager_desc.interopMode = PxCudaInteropMode::OGL_INTEROP;

    // g_cuda_context_manager = PxCreateCudaContextManager(*g_foundation, cuda_context_manager_desc, PxGetProfilerCallback());

    // if (g_cuda_context_manager) {
    //     if (!g_cuda_context_manager->contextIsValid()) {
    //         g_cuda_context_manager->release();
    //         g_cuda_context_manager = nullptr;
    //     }
    // }

    PxSceneDesc scene_desc(g_physics->getTolerancesScale());
    scene_desc.gravity = PxVec3(0.0f, -19.0f, 0.0f);
    g_dispatcher = PxDefaultCpuDispatcherCreate(4);
    scene_desc.cpuDispatcher = g_dispatcher;
    // scene_desc.filterShader = PxDefaultSimulationFilterShader;
    scene_desc.staticKineFilteringMode = PxPairFilteringMode::eKEEP;
    scene_desc.filterShader = contact_report_filter_shader;
    scene_desc.simulationEventCallback = &g_contact_report_callback;

    // scene_desc.flags |= PxSceneFlag::eENABLE_GPU_DYNAMICS;
    // scene_desc.flags |= PxSceneFlag::eENABLE_PCM;
    // scene_desc.flags |= PxSceneFlag::eENABLE_STABILIZATION;
    // scene_desc.broadPhaseType = PxBroadPhaseType::eGPU;
    // scene_desc.gpuMaxNumPartitions = 8;

    g_scene = g_physics->createScene(scene_desc);

    PxPvdSceneClient* pvd_client = g_scene->getScenePvdClient();
    if (pvd_client) {
        pvd_client->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_CONSTRAINTS, true);
        pvd_client->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_CONTACTS, true);
        pvd_client->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_SCENEQUERIES, true);
    }

    g_material = g_physics->createMaterial(0.5f, 0.5f, 0.6f);

}

void step_physics()
{
    g_contact_positions.clear();
    g_contact_impulses.clear();

    g_scene->simulate(1.0f/30.f);
    g_scene->fetchResults(true);

    // std::cout << g_contact_positions.size() << " contact reports\n";
}

void shutdown()
{
    g_physics->release();
    g_foundation->release();
    g_physics_debug->release();
    g_cooking->release();
}

PxConvexMesh* create_convex_mesh(const aiScene* scene, bool direct_insertion)
{
    const size_t num_vertices = scene->mMeshes[0]->mNumVertices;
    PxVec3* vertices = new PxVec3[num_vertices];

    auto assimp_vertices = scene->mMeshes[0]->mVertices;
    for (size_t i = 0; i < num_vertices; i++) {
        auto v = assimp_vertices[i];
        vertices[i] = PxVec3(v.x, v.y, v.z);
    }

    PxCookingParams params = g_cooking->getParams();
    params.convexMeshCookingType = PxConvexMeshCookingType::eQUICKHULL;

    PxU32 gauss_map_limit = 256;
    params.gaussMapLimit = gauss_map_limit;
    g_cooking->setParams(params);

    PxConvexMeshDesc desc;
    desc.points.data = vertices;
    desc.points.count = num_vertices;
    desc.points.stride = sizeof(PxVec3);
    desc.flags = PxConvexFlag::eCOMPUTE_CONVEX;

    PxU32 mesh_size = 0;
    PxConvexMesh* convex = nullptr;

    if (direct_insertion) {
        convex = g_cooking->createConvexMesh(desc, g_physics->getPhysicsInsertionCallback());
    } else {
        PxDefaultMemoryOutputStream out_stream;
        bool res = g_cooking->cookConvexMesh(desc, out_stream);
        mesh_size = out_stream.getSize();

        PxDefaultMemoryInputData in_stream(out_stream.getData(), out_stream.getSize());
        convex = g_physics->createConvexMesh(in_stream);
    }

    std::cout   << "convex mesh:\n"
                << "original num of vertices: " << num_vertices << '\n'
                << "num of vertices: " << convex->getNbVertices() << '\n'
                << "num of polygons: " << convex->getNbPolygons() << '\n';


    // convex->release();

    delete[] vertices;
    return convex;
}

void disable_shape_in_contact_tests(PxShape* shape)
{
    shape->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);
}

void enable_shape_in_contact_tests(PxShape* shape)
{
    shape->setFlag(PxShapeFlag::eSIMULATION_SHAPE, true);
}

void disable_shape_in_scene_query_tests(PxShape* shape)
{
    shape->setFlag(PxShapeFlag::eSCENE_QUERY_SHAPE, false);
}

void enable_shape_in_scene_query_tests(PxShape* shape)
{
    shape->setFlag(PxShapeFlag::eSCENE_QUERY_SHAPE, true);
}

PxFilterFlags contact_report_filter_shader(PxFilterObjectAttributes attributes0, PxFilterData filter_data0,
                                           PxFilterObjectAttributes attributes1, PxFilterData filter_data1,
                                           PxPairFlags& pair_flags, const void* constant_block, PxU32 constant_block_size)
{
    PX_UNUSED(attributes0);
    PX_UNUSED(attributes1);
    PX_UNUSED(filter_data0);
    PX_UNUSED(filter_data1);
    PX_UNUSED(constant_block_size);
    PX_UNUSED(constant_block);

    pair_flags = PxPairFlag::eSOLVE_CONTACT | PxPairFlag::eDETECT_DISCRETE_CONTACT
            | PxPairFlag::eNOTIFY_TOUCH_FOUND
            | PxPairFlag::eNOTIFY_TOUCH_PERSISTS
            | PxPairFlag::eNOTIFY_CONTACT_POINTS;
    return PxFilterFlag::eDEFAULT;
}

std::vector<PxVec3> g_contact_positions;
std::vector<PxVec3> g_contact_impulses;



}