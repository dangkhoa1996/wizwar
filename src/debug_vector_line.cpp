#include "debug_vector_line.h"

namespace Engine
{
DebugVectorLine::DebugVectorLine() : 
    m_p1(glm::vec3(0.0, 0.0, 0.0)),
    m_p2(glm::vec3(0.0, 0.0, 0.0))
{
    m_vertex_data = new float[6];
    m_vertex_data[0] = m_p1.x;
    m_vertex_data[1] = m_p1.y;
    m_vertex_data[2] = m_p1.z;

    m_vertex_data[3] = m_p2.x;
    m_vertex_data[4] = m_p2.y;
    m_vertex_data[5] = m_p2.z;

    m_shader_program = new ShaderProgram(
        Shader { ShaderType::VERTEX, "../shader/vertex/debug_line.vs" },
        Shader { ShaderType::FRAGMENT, "../shader/fragment/debug_line.fs" }
    );

    m_shader_program->attach();
    m_shader_program->bind_attrib_location(0, "a_pos");
    m_shader_program->link();
    m_shader_program->use();
 
    m_shader_program->set_mat4("u_model", glm::mat4(1.0));
    m_shader_program->set_vec3("u_color", glm::vec3(1.0, 1.0, 0.0));

    auto pv_index = glGetUniformBlockIndex(m_shader_program->m_program, "pv_mat");
    glUniformBlockBinding(m_shader_program->m_program, pv_index, 0);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, 6, 0, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * 6, m_vertex_data);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, (void*)0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}
DebugVectorLine::~DebugVectorLine() {}

void DebugVectorLine::set_p1(glm::vec3& p1)
{
    m_vertex_data[0] = p1.x;
    m_vertex_data[1] = p1.y;
    m_vertex_data[2] = p1.z;
}

void DebugVectorLine::set_p2(glm::vec3& p2)
{
    m_vertex_data[3] = p2.x;
    m_vertex_data[4] = p2.y;
    m_vertex_data[5] = p2.z;
}

void DebugVectorLine::draw(ICamera& camera)
{
    m_shader_program->use();

    // glPolygonMode(GL_FRONT_AND_BACK, GL_LINES);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), 0, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * 6, m_vertex_data);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glDrawArrays(GL_LINES, 0, 2);
    glBindVertexArray(0);
    // glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void DebugVectorLine::print_vertex_data()
{
    std::cout << "vertex data: ";
    for (int i = 0; i < 6; i++) {
        std::cout << m_vertex_data[i] << ' ';
    }
    std::cout << '\n';
}

}