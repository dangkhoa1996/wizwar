#include "model_scene.hpp"
#include <fstream>
#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>
#define ERROR_DETECT                                                     \
    auto error = glGetError();                                           \
    if (error != GL_NO_ERROR)                                            \
    {                                                                    \
        std::cout << "error " << error << " line: " << __LINE__ << '\n'; \
    }


#define TINYGLTF_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#define TINYGLTF_NOEXCEPTION
#define JSON_NOEXCEPTION
#include "tiny_gltf.h"

#define BUFFER_OFFSET(i) ((char*)NULL + (i))
namespace Engine
{
namespace Scene
{
ModelScene::ModelScene(): m_pv_uniform_buffer(new PV_UniformBuffer()), m_current_camera(new FreeCamera())
{

}

ModelScene::~ModelScene()
{

}

void ModelScene::init()
{
		load_model("../models/rock_stylized/scene.gltf");
		debug_model(m_model);

		bind_model(m_model);
		m_model_mat = glm::mat4(1.0);
		
		m_shader_program = new ShaderProgram(
			Shader { VERTEX, "../shader/vertex/model.vs" },
			Shader { FRAGMENT, "../shader/fragment/model.fs" }
		);

		m_shader_program->attach();
		m_shader_program->bind_attrib_location(0, "a_pos");
		m_shader_program->bind_attrib_location(1, "a_normal");
		m_shader_program->bind_attrib_location(3, "a_texcoord");
		m_shader_program->link();

		m_pv_uniform_buffer->create_uniform_buffer();
		m_pv_uniform_buffer->bind_buffer_to_shader_program(m_shader_program->m_program, "pv_mat");

		m_dir_light = DirectionalLight{
        glm::vec3(1.0f, 1.0f, 1.0f), // color
        glm::vec3(0.0f, 0.0f, 0.0f), // ambient
        glm::vec3(-1.0f, -5.0f, -1.0f), // direction
        true    // enabled
		};

		m_dir_light.pass_to_shader(*m_shader_program, 0);
		m_shader_program->use();
		m_shader_program->set_int("directional_lights_size", 1);
		m_shader_program->un_use();

}

void ModelScene::process_input()
{
	m_current_camera->move();
	m_current_camera->update_rotation();
	m_current_camera->update_scroll();
}

void ModelScene::render(double delta_time)
{
	glClearColor(0.4f, 0.4f, 0.4f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	m_pv_uniform_buffer->set_buffer_data(m_current_camera->m_projection, m_current_camera->m_view);


	m_shader_program->use();
	m_shader_program->set_vec3("u_camera_position", m_current_camera->m_position);
	m_shader_program->set_mat4("u_model", m_model_mat);
	draw_model(m_model);
	m_shader_program->un_use();
	
}

void ModelScene::update(double delta_time)
{
	m_current_camera->update(delta_time);
}

bool ModelScene::load_model(const char* filename)
{
	tinygltf::TinyGLTF loader;
	std::string err;
	std::string warn;

	bool res = loader.LoadASCIIFromFile(&m_model, &err, &warn, filename);

	if (!err.empty()) {
		std::cout << "EMPTY: " << err << '\n';
	}

	if (!res) {
		std::cout << "Failed to load gltf: " << filename << '\n';
	} else {
		std::cout << "Loaded gltf: " << filename << '\n';
	}

	return res;
}

void ModelScene::bind_mesh(tinygltf::Model& model, int mesh_index)
{
	const auto& mesh = model.meshes[mesh_index];
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	m_vaos[mesh_index] = vao;
	for (size_t i = 0; i < mesh.primitives.size(); i++) {
		tinygltf::Primitive primitive = mesh.primitives[i];
		tinygltf::Accessor index_accessor = model.accessors[primitive.indices];
		const auto& ebo_buffer_view = model.bufferViews[index_accessor.bufferView];
		auto& ebo_buffer = model.buffers[ebo_buffer_view.buffer]; 

		GLuint ebo;
		glGenBuffers(1, &ebo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, ebo_buffer_view.byteLength, &ebo_buffer.data.at(0) + ebo_buffer_view.byteOffset, GL_STATIC_DRAW);
		m_ebos[primitive.indices] = ebo;

		for (auto &attrib : primitive.attributes) {
			tinygltf::Accessor accessor = model.accessors[attrib.second];
			int byteStride = accessor.ByteStride(model.bufferViews[accessor.bufferView]);
			const auto& buffer_view = model.bufferViews[accessor.bufferView];
			auto& buffer = model.buffers[buffer_view.buffer];

			GLuint vbo;
			glGenBuffers(1, &vbo);
			glBindBuffer(GL_ARRAY_BUFFER, vbo);
			glBufferData(buffer_view.target, buffer_view.byteLength, &buffer.data.at(0) + buffer_view.byteOffset, GL_STATIC_DRAW);
			
			m_vbos[attrib.second] = vbo;

			int size = 1;
			if (accessor.type != TINYGLTF_TYPE_SCALAR) {
				size = accessor.type;
			}

			int vaa = -1;
			if (attrib.first.compare("POSITION") == 0) vaa = attrib.second;
			if (attrib.first.compare("NORMAL") == 0) vaa = attrib.second;
			if (attrib.first.compare("TEXCOORD_0") == 0) vaa = attrib.second;
			if (vaa > -1) {
				glEnableVertexAttribArray(vaa);
				glVertexAttribPointer(vaa, size, accessor.componentType,
					accessor.normalized ? GL_TRUE : GL_FALSE,
					byteStride, BUFFER_OFFSET(accessor.byteOffset));
			} else {
				std::cout << "vaa missing: " << attrib.first << '\n';
			}
		}
	}
	glBindVertexArray(0);
}

void ModelScene::bind_model_nodes(tinygltf::Model& model, tinygltf::Node& node)
{
	if ((node.mesh >= 0) && (node.mesh < model.meshes.size())) {
		bind_mesh(model, node.mesh);
	}

	for (size_t i = 0; i < node.children.size(); i++) {
		assert((node.children[i] >= 0) && (node.children[i] < model.nodes.size()));
		bind_model_nodes(model, model.nodes[node.children[i]]);
	}
}

void ModelScene::bind_model(tinygltf::Model& model)
{
	if (model.textures.size() > 0) {
		for (int i = 0; i < model.textures.size(); i++) {
			tinygltf::Texture &tex = model.textures[i];
			if (tex.source > -1) {
				GLuint texid;
				glGenTextures(1, &texid);
				tinygltf::Image &image = model.images[tex.source];
				tinygltf::Sampler &sampler = model.samplers[tex.sampler];
				glBindTexture(GL_TEXTURE_2D, texid);

				GLenum format = GL_RGBA;
				if (image.component == 1) {
					format = GL_RED;
				} else if (image.component == 2) {
					format = GL_RG;
				} else if (image.component == 3) {
					format = GL_RGB;
				}

				GLenum type = GL_UNSIGNED_BYTE;
				if (image.bits == 8) {
					// what to do here ?
				} else if (image.bits == 16) {
					type = GL_UNSIGNED_SHORT;
				}

				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0, format, type, &image.image.at(0));
				glGenerateMipmap(GL_TEXTURE_2D);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, sampler.minFilter);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, sampler.magFilter);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
				glBindTexture(GL_TEXTURE_2D, 0);
				m_tex_id[i] = texid;
			}
		}
	}

	const tinygltf::Scene &scene = model.scenes[model.defaultScene];
	for (size_t i = 0; i < scene.nodes.size(); i++) {
		assert((scene.nodes[i] >= 0) && (scene.nodes[i] < model.nodes.size()));
		bind_model_nodes(model, model.nodes[scene.nodes[i]]);
	}
}

void ModelScene::draw_mesh(tinygltf::Model& model, int mesh_index, glm::mat4 transform)
{
	const auto& mesh = model.meshes[mesh_index];
	glBindVertexArray(m_vaos[mesh_index]);
	for (size_t i = 0; i < mesh.primitives.size(); i++) {
		tinygltf::Primitive primitive = mesh.primitives[i];
		tinygltf::Accessor index_accessor = model.accessors[primitive.indices];
		if (primitive.material > -1) {
			const auto& material = model.materials[primitive.material];
			int texture_index = material.pbrMetallicRoughness.baseColorTexture.index;
			if (texture_index > -1) {
				// TODO: handle texcoord index
				glActiveTexture(GL_TEXTURE0 + texture_index);
				glBindTexture(GL_TEXTURE_2D, m_tex_id[texture_index]);
				m_shader_program->set_texture("u_texture", texture_index);
			}
		}
		
		for (const auto& attrib: primitive.attributes) {
			glBindBuffer(GL_ARRAY_BUFFER, m_vbos[attrib.second]);
		}

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebos[primitive.indices]);
		m_shader_program->set_mat4("u_model", transform);
		glDrawElements(primitive.mode, index_accessor.count, index_accessor.componentType, BUFFER_OFFSET(index_accessor.byteOffset));
	}
	glBindVertexArray(0);
}

void ModelScene::draw_model_nodes(tinygltf::Model& model, tinygltf::Node& node, glm::mat4 parent_transform)
{
	// get transform
	glm::quat rotation = glm::quat(1.0, 0.0, 0.0, 0.0);
	if (node.rotation.size() > 0) {
		rotation = glm::quat(node.rotation[3], node.rotation[0], node.rotation[1], node.rotation[2]);
	}

	glm::vec3 translation = glm::vec3(1.0);
	if (node.translation.size() > 0) {
		translation = glm::vec3(node.translation[0], node.translation[1], node.translation[2]);
	}
	glm::vec3 scale = glm::vec3(1.0);
	if (node.scale.size() > 0) {
		scale = glm::vec3(node.scale[0], node.scale[1], node.scale[2]);
	}

	glm::mat4 transform = glm::mat4(1.0);
	if (node.matrix.size() > 0) {
		const auto& mat = node.matrix;
		transform = glm::mat4(
			glm::vec4(mat[0], mat[1], mat[2], mat[3]),
			glm::vec4(mat[4], mat[5], mat[6], mat[7]),
			glm::vec4(mat[8], mat[9], mat[10], mat[11]),
			glm::vec4(mat[12], mat[13], mat[14], mat[15])
		);
	} else {
		transform = glm::translate(transform, translation);
		transform = glm::scale(transform, scale);
		transform = transform * glm::mat4_cast(rotation);
	}

	transform = parent_transform * transform;

	if ((node.mesh >= 0) && (node.mesh < model.meshes.size())) {
		draw_mesh(model, node.mesh, transform);
	}

	for (size_t i = 0; i < node.children.size(); i++) {
		draw_model_nodes(model, model.nodes[node.children[i]], transform);
	}
}

void ModelScene::draw_model(tinygltf::Model& model)
{
	const tinygltf::Scene& scene = model.scenes[model.defaultScene];
	for (size_t i = 0; i < scene.nodes.size(); i++) {
		draw_model_nodes(model, model.nodes[scene.nodes[i]], glm::mat4(1.0));
	}
}

void ModelScene::debug_model(tinygltf::Model& model)
{
	for (int i = 0; i < model.nodes.size(); i++) {
		const auto& node = model.nodes[i];
		std::cout << "index: "<< i << " node.name: " << node.name << '\n';
		if (node.mesh > -1) {
			std::cout << "\tnode.mesh: " << node.mesh << '\n'; 
		}
		if (node.rotation.size() > 0) {
			std::cout << "node.rotation: ";
			for (const auto& e : node.rotation) {
				 std::cout << e << ' ';
			}
			std::cout << '\n';
		}
	}
}

}
}