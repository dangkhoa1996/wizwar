#include "test_scene.hpp"

namespace Engine
{
namespace Scene
{

TestScene::TestScene()
{
    m_geometries.reserve(MAX_GEOMETRIES);
    m_lights.reserve(MAX_LIGHT_SOURCES);
}

TestScene::~TestScene()
{

}

void TestScene::init()
{
    Geometry cube = GeometryFactory::get_instance().create_cube();
    cube.m_transform.set_position(glm::vec3(0.0f, 5.0f, 0.0f));
    cube.m_material.m_diffuse = glm::vec3(0.38f, 0.72f, 0.56f);

    Geometry cone = GeometryFactory::get_instance().create_cone();
    cone.m_transform.set_position(glm::vec3(1.0f, 0.0f, 3.0f));
    cone.m_material.m_diffuse = glm::vec3(0.09f, 0.74f, 0.03f);

    Geometry cylinder = GeometryFactory::get_instance().create_cylinder();
    cylinder.m_transform.set_position(glm::vec3(0.0f, 0.0f, -4.0f));
    cylinder.m_material.m_diffuse = glm::vec3(0.45f, 0.34f, 0.01f);

    Geometry plane = GeometryFactory::get_instance().create_plane();
    plane.m_transform.set_position(glm::vec3(4.0, -4.0f, 0.0f));
    plane.m_material.m_diffuse = glm::vec3(0.32f, 0.02f, 0.07f);
    
    Geometry round_plane = GeometryFactory::get_instance().create_round_plane();
    round_plane.m_transform.set_position(glm::vec3(2.0f, -2.0f, -3.0f));
    round_plane.m_material.m_diffuse = glm::vec3(0.6f, 0.56f, 0.003f);

    // add light

    // add texture

    // render a complete scenery
    m_geometries.push_back(cube);
    m_geometries.push_back(cone);
    m_geometries.push_back(cylinder);
    m_geometries.push_back(plane);
    m_geometries.push_back(round_plane);

    DirectionalLight light(glm::vec3(1.0f, 1.0f, 0.0f), 
    glm::vec3(0.03f, 0.02f, 0.76f), glm::vec3(-0.4f, -1.0f, -1.0f), true);
    m_lights.push_back(light);
    Renderer::get_instance().add_directional_lights(m_lights);

    m_camera = new FreeCamera();
    Renderer::get_instance().set_camera(m_camera);
    Renderer::get_instance().add_material(m_geometries);
    
}

void TestScene::process_input()
{
    m_camera->move();
    m_camera->update_rotation();
    m_camera->update_scroll();
}

void TestScene::update(double delta_time)
{
    m_camera->update(delta_time);
    m_geometries[1].m_transform.rotate(glm::vec3(0.0f, 1.0f, 0.0f), 0.01f);
}

void TestScene::render(double delta_time)
{
    Renderer::get_instance().draw(m_geometries, delta_time);
}

}

}