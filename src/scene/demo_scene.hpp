/*
#pragma once
#include "scene/scene.h"
#include "model/model.hpp"
#include "basic_shape.hpp"

#include "geometry/plane.hpp"
#include "geometry/cube.hpp"
#include "geometry/cone.hpp"
#include "geometry/cylinder.hpp"
#include "light.hpp"
#include "material.hpp"
#include "skybox.hpp"
#include "character_movement_controller.hpp"
#include "debug_vector_line.h"
#include "animation/animation_model.hpp"
#include "shadow.hpp"
#include "pv_uniform_buffer.hpp"


#include "physics/physic.hpp"

#include <glm/glm.hpp>
#include <GLFW/glfw3.h>
#include <vector>

#include <physx/PxPhysicsAPI.h>

namespace Engine
{

namespace Scene 
{

class DemoScene : public IScene 
{
public:
    DemoScene();
    ~DemoScene();

    void init(TextureLibrary& texture_library) override;
    void update(double delta_time) override;
    void draw(double delta_time) override;
    void process_input(GLFWwindow*& window, double elapsed_time) override;

    ICamera* m_free_camera;
    ICamera* m_target_camera;

    ShaderProgram* basic_shader;
    ShaderProgram* light_shader;
    ShaderProgram* image_based_lighting_shader;
    
    glm::mat4 cube_model_mat;
    GLuint cube_vao;
    GLuint cube_vbo;

    Plane* m_plane;
    Cube* m_cube;
    Cube* m_wall;

    Cube* m_assist_cube;
    Cube* m_parent_cube;
    
    Skybox* m_skybox;

    std::vector<DirectionalLight> m_directional_light_list;

    CubeMapId_t m_specular_cubemap_id;
    CubeMapId_t m_diffuse_cubemap_id;

    PV_UniformBuffer* m_pv_uniform_buffer;
    DebugVectorLine* m_debug_line;

    size_t light_arr_size;
   
    Shadow* m_shadow;
    
    AnimationModel* m_character;
    CharacterMovementController* m_character_movement_controller;

    Cone* m_cone;
    Cylinder* m_cylinder;

    void init_light(ShaderProgram* shader_program);
    void init_physic();
    void pass_directional_light_to_shader(ShaderProgram* shader_program);

};

} // namespace Scene

} // namespace Engine

*/