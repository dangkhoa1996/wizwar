#pragma once
#include "wave_scene.hpp"
#include "systems/input/keyboard_input.hpp"

#ifndef ERROR_DETECT
#define ERROR_DETECT                                                     \
    auto error = glGetError();                                           \
    if (error != GL_NO_ERROR)                                            \
    {                                                                    \
        std::cout << "file: " << __FILE__ << " error " << error << " line: " << __LINE__ << '\n'; \
    }
#endif


// #ifndef STB_IMAGE_IMPLEMENTATION
// #define STB_IMAGE_IMPLEMENTATION
// #endif

#include "stb_image.h"

namespace Engine
{

namespace Scene
{

float WaveScene::clip_height = 1.65f;
float WaveScene::pillar_y = -2.0f;

float WaveScene::dir_1 = 1.837f;
float WaveScene::dir_2 = 5.123f;
float WaveScene::dir_3 = 1.257f;

float WaveScene::len_1 = 47.692f;
float WaveScene::len_2 = 53.846f;
float WaveScene::len_3 = 20.85f;

float WaveScene::amp_1 = 0.31f;
float WaveScene::amp_2 = 0.046f;
float WaveScene::amp_3 = 0.30f;

float WaveScene::speed_1 = 0.05f;
float WaveScene::speed_2 = 0.04f;
float WaveScene::speed_3 = 0.07f;

float WaveScene::tex_dir_1 = 4.301f;
float WaveScene::tex_dir_2 = 1.922f;
float WaveScene::tex_dir_3 = 0.824f;
float WaveScene::tex_dir_4 = 2.288f;
float WaveScene::tex_dir_5 = 1.281f;

float WaveScene::tex_len_1 = 10.194f;
float WaveScene::tex_len_2 = 3.641f;
float WaveScene::tex_len_3 = 8.495f;
float WaveScene::tex_len_4 = 8.010f;
float WaveScene::tex_len_5 = 10.194f;

float WaveScene::tex_amp_1 = 3.98f;
float WaveScene::tex_amp_2 = 3.81f;
float WaveScene::tex_amp_3 = 1.189f;
float WaveScene::tex_amp_4 = 1.48f;
float WaveScene::tex_amp_5 = 1.043f;

float WaveScene::tex_speed_1 = 22.087f;
float WaveScene::tex_speed_2 = 5.583f;
float WaveScene::tex_speed_3 = 4.612f;
float WaveScene::tex_speed_4 = 14.806f;
float WaveScene::tex_speed_5 = 10.608f;

float WaveScene::light_x = 7.0f;
float WaveScene::light_y = -60.297f;
float WaveScene::light_z = 13.187f;

float WaveScene::light_r = 0.192f;
float WaveScene::light_g = 0.33f;
float WaveScene::light_b = 0.491f;


WaveScene::WaveScene(): m_pv_uniform_buffer(new PV_UniformBuffer()) {}

WaveScene::~WaveScene() {}

void WaveScene::init()
{
    DirectionalLight light(
        glm::vec3(1.0f, 1.0f, 1.0f), // color
        glm::vec3(0.2f, 0.4f, 0.01f), // ambient
        glm::vec3(-1.0f, -5.0f, -1.0f), // direction
        true    // enabled
    );

    m_lights.push_back(light);

    m_global_camera = new FreeCamera();
    m_wave_system = new WaveSystem();

    m_land = GeometryFactory::get_instance().create_plane();
    m_land.m_material.m_diffuse = glm::vec3(0.6f, 0.6f, 0.6f);
    m_land.m_transform.scale(glm::vec3(10.0f, 10.0f, 10.0f));
    m_land.m_transform.rotate(glm::vec3(0.0f, 0.0f, 1.0f), glm::pi<float>() / 2.0f);
    m_land.m_transform.translate(glm::vec3(5.0f, -2.0f, 5.0f));

    m_pillar = GeometryFactory::get_instance().create_cube();
    m_pillar.m_material.m_diffuse = glm::vec3(0.9f, 0.2f, 0.2f);
    m_pillar.m_transform.scale(glm::vec3(2.0f, 5.0f, 2.0f));
    m_pillar.m_transform.translate(glm::vec3(5.0f, -2.0f, 5.0f));
    
    m_land_shader_program = new ShaderProgram(
        Shader { VERTEX, "../shader/vertex/plane.vs" },
        Shader { FRAGMENT, "../shader/fragment/plane.fs" }
    );

    m_land_shader_program->attach();
    m_land_shader_program->bind_attrib_location(0, "a_pos");
    m_land_shader_program->bind_attrib_location(1, "a_normal");
    m_land_shader_program->bind_attrib_location(2, "a_texcoord");
    m_land_shader_program->link();

    m_pillar_shader_program = new ShaderProgram(
        Shader { VERTEX, "../shader/vertex/basic_shape.vs" },
        Shader { FRAGMENT, "../shader/fragment/basic_shape.fs" }
    );

    m_pillar_shader_program->attach();
    m_pillar_shader_program->bind_attrib_location(0, "a_pos");
    m_pillar_shader_program->bind_attrib_location(1, "a_normal");
    m_pillar_shader_program->bind_attrib_location(2, "a_texcoord");
    m_pillar_shader_program->link();

    // m_plane = GeometryFactory::get_instance().create_plane();
    // m_plane.m_material.m_diffuse = glm::vec3(1.0f, 0.0f, 0.0f);
    // m_plane.m_transform.translate(glm::vec3(0.7f, 0.6f, 0.0f));
    // m_plane.m_transform.rotate(glm::vec3(0.0f, 1.0f, 0.0f), 90.0f);
    // m_plane.m_transform.scale(glm::vec3(0.0f, 0.3f, 0.2f));

    m_refrac_ui_plane = GeometryFactory::get_instance().create_plane();
    m_refrac_ui_plane.m_transform.translate(glm::vec3(0.7f, 0.65f, 0.0f));

    // combine rotation
    const float angle1 = 45.f * PI / 180.f;
    const float angle2 = (0.5f + 45.f / 180.f) * PI;
    glm::quat rot1 {cos(angle2), 0.0f, 0.0f, sin(angle2) };
    glm::quat rot2 {cos(angle1), 0.0f, sin(angle1), 0.0f};
    m_refrac_ui_plane.m_transform.m_rotation = rot1 * rot2;
    m_refrac_ui_plane.m_transform.scale(glm::vec3(0.0f, 0.2f, 0.3f));

    m_reflect_ui_plane = GeometryFactory::get_instance().create_plane();
    m_reflect_ui_plane.m_transform.translate(glm::vec3(0.7f, 0.1f, 0.0f));
    m_reflect_ui_plane.m_transform.m_rotation = rot1 * rot2;
    m_reflect_ui_plane.m_transform.scale(glm::vec3(0.0f, 0.2f, 0.3f));

    sp_plane = new ShaderProgram(
        Shader { VERTEX, "../shader/vertex/ui_plane.vs"},
        Shader { FRAGMENT, "../shader/fragment/ui_plane.fs" });
    
    sp_plane->attach();
    sp_plane->bind_attrib_location(0, "a_pos");
    sp_plane->bind_attrib_location(1, "a_normal");
    sp_plane->bind_attrib_location(2, "a_texcoord");
    sp_plane->link();

    m_pv_uniform_buffer->create_uniform_buffer();
    m_pv_uniform_buffer->bind_buffer_to_shader_program(m_wave_system->m_shader_program->m_program, "pv_mat");
    m_pv_uniform_buffer->bind_buffer_to_shader_program(m_wave_system->m_bump_shader_program->m_program, "pv_mat");
    m_pv_uniform_buffer->bind_buffer_to_shader_program(m_wave_system->m_normal_shader_program->m_program, "pv_mat");
    m_pv_uniform_buffer->bind_buffer_to_shader_program(m_land_shader_program->m_program, "pv_mat");
    m_pv_uniform_buffer->bind_buffer_to_shader_program(m_pillar_shader_program->m_program, "pv_mat");
    // m_pv_uniform_buffer->bind_buffer_to_shader_program(sp_plane->m_program, "pv_mat");

    m_wave_system->m_shader_program->use();
    m_wave_system->m_shader_program->set_int("directional_lights_size", m_lights.size());
    m_wave_system->m_shader_program->un_use();

    m_pillar_shader_program->use();
    m_pillar_shader_program->set_int("directional_light_size", m_lights.size());
    m_pillar_shader_program->un_use();
    
    for (int i = 0; i < m_lights.size(); i++) {
        light.pass_to_shader(*m_wave_system->m_shader_program, i);
        light.pass_to_shader(*m_pillar_shader_program, i);
    }

    m_clip_plane_refract = glm::vec4(0.0f, -1.0f, 0.0f, -1.0f);
    m_clip_plane_reflect = glm::vec4(0.0f, 1.0f, 0.0f, -1.0f);

    init_textures();
    init_renderbuffer();
    init_framebuffers();
}

void WaveScene::init_textures()
{
    glGenTextures(1, &m_grid_texture);
    glBindTexture(GL_TEXTURE_2D, m_grid_texture);

    int width, height, channels;
    unsigned char *img = stbi_load("../textures/grid.jpg", &width, &height, &channels, 0);
    if (img == NULL)
    {
        std::cout << "Error in loading in image\n";
    }
    std::cout << "image loaded " << width << ' ' << height << ' ' << channels << '\n';

    glTexImage2D(
        GL_TEXTURE_2D,    // target
        0,                // level
        GL_RGB,          // internal format
        width,    // width
        height,    // height
        0,                // border
        GL_RGB,          // format
        GL_UNSIGNED_BYTE, // type
        img             // data
    );

    // glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, img);
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glBindTexture(GL_TEXTURE_2D, 0);

    stbi_image_free(img);

    glGenTextures(1, &m_refraction_texture);
    glBindTexture(GL_TEXTURE_2D, m_refraction_texture);
    std::cout << "refract w: " << m_global_camera->m_screen_width << " h: " << m_global_camera->m_screen_height << '\n';
    glTexImage2D(
        GL_TEXTURE_2D,    // target
        0,                // level
        GL_RGB,          // internal format
        m_global_camera->m_screen_width,    // width
        m_global_camera->m_screen_height,    // height
        0,                // border
        GL_RGB,          // format
        GL_UNSIGNED_BYTE, // type
        NULL             // data
    );

    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glBindTexture(GL_TEXTURE_2D, 0);

    glGenTextures(1, &m_reflection_texture);
    glBindTexture(GL_TEXTURE_2D, m_reflection_texture);
    std::cout << "reflect w: " << m_global_camera->m_screen_width << " h: " << m_global_camera->m_screen_height << '\n';
    glTexImage2D(
        GL_TEXTURE_2D,    // target
        0,                // level
        GL_RGB,          // internal format
        m_global_camera->m_screen_width,    // width
        m_global_camera->m_screen_height,    // height
        0,                // border
        GL_RGB,          // format
        GL_UNSIGNED_BYTE, // type
        NULL             // data
    );

    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void WaveScene::init_framebuffers()
{
    glGenFramebuffers(1, &m_refraction_framebuffer);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_refraction_framebuffer);
    glFramebufferTexture2D(
        GL_FRAMEBUFFER,       //target
        GL_COLOR_ATTACHMENT0, // attachment
        GL_TEXTURE_2D,        // texturetarget
        m_refraction_texture,           // texture
        0                     // level
    );
    glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_refraction_renderbuffer);


    glGenFramebuffers(1, &m_reflection_framebuffer);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_reflection_framebuffer);
    glFramebufferTexture2D(
        GL_FRAMEBUFFER,       //target
        GL_COLOR_ATTACHMENT0, // attachment
        GL_TEXTURE_2D,        // texturetarget
        m_reflection_texture,           // texture
        0                     // level
    );
    glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_reflection_renderbuffer);
}

void WaveScene::init_renderbuffer()
{
    glGenRenderbuffers(1, &m_refraction_renderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, m_refraction_renderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, m_global_camera->m_screen_width, m_global_camera->m_screen_height);

    glGenRenderbuffers(1, &m_reflection_renderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, m_reflection_renderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, m_global_camera->m_screen_width, m_global_camera->m_screen_height);
}

void WaveScene::set_camera_width_and_height(double width, double height)
{
    m_global_camera->m_screen_width = width;
    m_global_camera->m_screen_height = height;
}

void WaveScene::update(double delta_time)
{
    if (KeyboardInput::m_is_cam_locked) 
    {
        m_global_camera->update(delta_time);
    }
    m_wave_system->update_wave_params(0, dir_1, amp_1, len_1, speed_1);
    m_wave_system->update_wave_params(1, dir_2, amp_2, len_2, speed_2);
    m_wave_system->update_wave_params(2, dir_3, amp_3, len_3, speed_3);

    m_wave_system->update_tex_wave_params(0, tex_dir_1, tex_amp_1, tex_len_1, tex_speed_1);
    m_wave_system->update_tex_wave_params(1, tex_dir_2, tex_amp_2, tex_len_2, tex_speed_2);
    m_wave_system->update_tex_wave_params(2, tex_dir_3, tex_amp_3, tex_len_3, tex_speed_3);
    m_wave_system->update_tex_wave_params(3, tex_dir_4, tex_amp_4, tex_len_4, tex_speed_4);
    m_wave_system->update_tex_wave_params(4, tex_dir_5, tex_amp_5, tex_len_5, tex_speed_5);

    auto& light = m_lights[0];
    light.m_direction.x = light_x;
    light.m_direction.y = light_y;
    light.m_direction.z = light_z;

    light.m_color.x = light_r;
    light.m_color.y = light_g;
    light.m_color.z = light_b;

    light.pass_to_shader(*m_wave_system->m_shader_program, 0);
    light.pass_to_shader(*m_pillar_shader_program, 0);
    m_wave_system->update_waves(delta_time);
    m_clip_plane_refract.w = clip_height;
    m_clip_plane_reflect.w = clip_height;
    m_pillar.m_transform.m_position.y = pillar_y;
}

void WaveScene::process_input()
{   
    if (KeyboardInput::m_is_cam_locked) 
    {
        m_global_camera->move();
        m_global_camera->update_rotation();
        m_global_camera->update_scroll();
    }
}

void WaveScene::render(double delta_time)
{   
    // m_pv_uniform_buffer->set_buffer_data(m_global_camera->m_projection, m_global_camera->m_view);
    // render refraction texture
    // std::cout << m_global_camera->m_screen_width << ' ' << m_global_camera->m_screen_height << '\n'; 
    glViewport(0, 0, m_global_camera->m_screen_width, m_global_camera->m_screen_height);
    glPolygonMode(GL_FRONT, GL_FILL);
    glEnable(GL_CLIP_PLANE0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_refraction_framebuffer);
    glClearColor(0.4f, 0.4f, 0.4f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    
    m_land_shader_program->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_grid_texture);
    m_land_shader_program->set_vec4("u_clip_plane", m_clip_plane_refract);
    m_land_shader_program->set_texture("u_texture", 0);
    m_land.draw(*m_land_shader_program);
    m_land_shader_program->un_use();
    glBindTexture(GL_TEXTURE_2D, 0);

    m_pillar_shader_program->use();
    m_pillar_shader_program->set_int("directional_lights_size", m_lights.size());
    m_pillar_shader_program->set_vec4("u_clip_plane", m_clip_plane_refract);
    m_pillar.m_material.pass_to_shader(*m_pillar_shader_program);
    m_pillar.draw(*m_pillar_shader_program);
    m_pillar_shader_program->un_use();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // render reflection texture
    glViewport(0, 0, m_global_camera->m_screen_width, m_global_camera->m_screen_height);
    glPolygonMode(GL_FRONT, GL_FILL);
    glEnable(GL_CLIP_PLANE0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_reflection_framebuffer);
    glClearColor(0.4f, 0.4f, 0.4f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    distance = 2.0f * m_global_camera->m_position.y;
    m_global_camera->m_position.y -= distance;
    m_global_camera->m_pitch = - m_global_camera->m_pitch;
    m_global_camera->update(delta_time);
    m_pv_uniform_buffer->set_buffer_data(m_global_camera->m_projection, m_global_camera->m_view);
    
    m_land_shader_program->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_grid_texture);
    m_land_shader_program->set_vec4("u_clip_plane", m_clip_plane_reflect);
    m_land_shader_program->set_texture("u_texture", 0);
    m_land.draw(*m_land_shader_program);
    m_land_shader_program->un_use();
    glBindTexture(GL_TEXTURE_2D, 0);

    m_pillar_shader_program->use();
    m_pillar_shader_program->set_int("directional_lights_size", m_lights.size());
    m_pillar_shader_program->set_vec4("u_clip_plane", m_clip_plane_reflect);
    m_pillar.m_material.pass_to_shader(*m_pillar_shader_program);
    m_pillar.draw(*m_pillar_shader_program);
    m_pillar_shader_program->un_use();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    m_global_camera->m_position.y += distance;
    m_global_camera->m_pitch = -m_global_camera->m_pitch;
    m_global_camera->update(delta_time);
    m_pv_uniform_buffer->set_buffer_data(m_global_camera->m_projection, m_global_camera->m_view);

    // render 3d scene
    glViewport(0, 0, m_global_camera->m_screen_width, m_global_camera->m_screen_height);
    glClearColor(0.4f, 0.4f, 0.4f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_CLIP_PLANE0);
    m_pillar_shader_program->use();
    m_pillar_shader_program->set_int("directional_lights_size", m_lights.size());
    m_pillar.m_material.pass_to_shader(*m_pillar_shader_program);
    m_pillar.draw(*m_pillar_shader_program);
    m_pillar_shader_program->un_use();

    m_land_shader_program->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_grid_texture);
    m_land_shader_program->set_texture("u_texture", 0);
    m_land.draw(*m_land_shader_program);
    m_land_shader_program->un_use();
    glBindTexture(GL_TEXTURE_2D, 0);
    
    m_wave_system->render_waves(m_global_camera, m_refraction_texture, m_reflection_texture);
    // glBindTexture(GL_TEXTURE_2D, 0);

    // render ui
    // sp_plane->use();
    // glActiveTexture(GL_TEXTURE0);
    // glBindTexture(GL_TEXTURE_2D, m_wave_system->m_bump_tex);
    // sp_plane->set_texture("u_texture", 0);
    // m_plane.draw(*sp_plane);
    // sp_plane->un_use();
    // glBindTexture(GL_TEXTURE_2D, 0);

    sp_plane->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_refraction_texture);
    sp_plane->set_texture("u_texture", 0);
    m_refrac_ui_plane.draw(*sp_plane);
    sp_plane->un_use();
    glBindTexture(GL_TEXTURE_2D, 0);

    sp_plane->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_reflection_texture);
    sp_plane->set_texture("u_texture", 0);
    m_reflect_ui_plane.draw(*sp_plane);
    sp_plane->un_use();
    glBindTexture(GL_TEXTURE_2D, 0);

    draw_config_ui();
}


void WaveScene::draw_config_ui()
{
    // ImGui::SetNextWindowSize(ImVec2(100, 100));
    static const float MIN_WAVE_LEN = 0.0f;
    static const float MAX_WAVE_LEN = 300.0f;
    static const float MIN_AMP = 0.0f;
    static const float MAX_AMP = 2.0f;
    static const float TWO_PI = 2 * glm::pi<float>();
    ImGui::NewFrame();
    {
        ImGui::Begin("Wave Parameters");
        ImGui::Text("Wave 1");
        // static float dir_1 = 0.0f;
        ImGui::SliderFloat("Wave Dir 1", &dir_1, 0.0f, TWO_PI, "%.3f", 1.0f);
        ImGui::SliderFloat("Wave Length 1", &len_1, MIN_WAVE_LEN, MAX_WAVE_LEN, "%.3f", 1.0f);
        ImGui::SliderFloat("Wave Amplitude 1", &amp_1, MIN_AMP, MAX_AMP, "%.5f", 1.0f);
        ImGui::SliderFloat("Wave Speed 1", &speed_1, 0.0f, 0.5f, "%.4f", 1.0f);
        ImGui::Separator();
        
        ImGui::Text("Wave 2");
        ImGui::SliderFloat("Wave Dir 2", &dir_2, 0.0f, TWO_PI, "%.3f", 1.0f);
        ImGui::SliderFloat("Wave Length 2", &len_2, MIN_WAVE_LEN, MAX_WAVE_LEN, "%.3f", 1.0f);
        ImGui::SliderFloat("Wave Amplitude 2", &amp_2, MIN_AMP, MAX_AMP, "%.5f", 1.0f);
        ImGui::SliderFloat("Wave Speed 2", &speed_2, 0.0f, 0.5f, "%.4f", 1.0f);
        ImGui::Separator();

        ImGui::Text("Wave 3");
        ImGui::SliderFloat("Wave Dir 3", &dir_3, 0.0f, TWO_PI, "%.3f", 1.0f);
        ImGui::SliderFloat("Wave Length 3", &len_3, MIN_WAVE_LEN, MAX_WAVE_LEN, "%.3f", 1.0f);
        ImGui::SliderFloat("Wave Amplitude 3", &amp_3, MIN_AMP, MAX_AMP, "%.5f", 1.0f);;
        ImGui::SliderFloat("Wave Speed 3", &speed_3, 0.0f, 0.5f, "%.4f", 1.0f);
        ImGui::End();
    }
    {
        ImGui::Begin("Texture Waves");
        ImGui::Text("Tex Wave 1");
        ImGui::SliderFloat("Tex Wave Dir 1", &tex_dir_1, 0.0f, TWO_PI, "%.3f", 1.0f);
        ImGui::SliderFloat("Tex Wave Length 1", &tex_len_1, MIN_WAVE_LEN, MAX_WAVE_LEN, "%.3f", 1.0f);
        ImGui::SliderFloat("Tex Wave Amplitude 1", &tex_amp_1, MIN_AMP, MAX_AMP, "%.5f", 1.0f);;
        ImGui::SliderFloat("Tex Wave Speed 1", &tex_speed_1, 0.0f, MAX_WAVE_LEN, "%.3f", 1.0f);

        ImGui::Text("Tex Wave 2");
        ImGui::SliderFloat("Tex Wave Dir 2", &tex_dir_2, 0.0f, TWO_PI, "%.3f", 1.0f);
        ImGui::SliderFloat("Tex Wave Length 2", &tex_len_2, MIN_WAVE_LEN, MAX_WAVE_LEN, "%.3f", 1.0f);
        ImGui::SliderFloat("Tex Wave Amplitude 2", &tex_amp_2, MIN_AMP, MAX_AMP, "%.5f", 1.0f);;
        ImGui::SliderFloat("Tex Wave Speed 2", &tex_speed_2, 0.0f, MAX_WAVE_LEN, "%.3f", 1.0f);

        ImGui::Text("Tex Wave 3");
        ImGui::SliderFloat("Tex Wave Dir 3", &tex_dir_3, 0.0f, TWO_PI, "%.3f", 1.0f);
        ImGui::SliderFloat("Tex Wave Length 3", &tex_len_3, MIN_WAVE_LEN, MAX_WAVE_LEN, "%.3f", 1.0f);
        ImGui::SliderFloat("Tex Wave Amplitude 3", &tex_amp_3, MIN_AMP, MAX_AMP, "%.5f", 1.0f);;
        ImGui::SliderFloat("Tex Wave Speed 3", &tex_speed_3, 0.0f, MAX_WAVE_LEN, "%.3f", 1.0f);

        ImGui::Text("Tex Wave 4");
        ImGui::SliderFloat("Tex Wave Dir 4", &tex_dir_4, 0.0f, TWO_PI, "%.3f", 1.0f);
        ImGui::SliderFloat("Tex Wave Length 4", &tex_len_4, MIN_WAVE_LEN, MAX_WAVE_LEN, "%.3f", 1.0f);
        ImGui::SliderFloat("Tex Wave Amplitude 4", &tex_amp_4, MIN_AMP, MAX_AMP, "%.5f", 1.0f);;
        ImGui::SliderFloat("Tex Wave Speed 4", &tex_speed_4, 0.0f, MAX_WAVE_LEN, "%.3f", 1.0f);

        ImGui::Text("Tex Wave 5");
        ImGui::SliderFloat("Tex Wave Dir 5", &tex_dir_5, 0.0f, TWO_PI, "%.3f", 1.0f);
        ImGui::SliderFloat("Tex Wave Length 5", &tex_len_5, MIN_WAVE_LEN, MAX_WAVE_LEN, "%.3f", 1.0f);
        ImGui::SliderFloat("Tex Wave Amplitude 5", &tex_amp_5, MIN_AMP, MAX_AMP, "%.5f", 1.0f);;
        ImGui::SliderFloat("Tex Wave Speed 5", &tex_speed_5, 0.0f, MAX_WAVE_LEN, "%.3f", 1.0f);
        ImGui::End();
    }
    {
        ImGui::Begin("Light Parameters");
        ImGui::SliderFloat("Light X", &light_x, -100.f, 100.f, "%.3f", 1.0f);
        ImGui::SliderFloat("Light Y", &light_y, -100.f, 100.f, "%.3f", 1.0f);
        ImGui::SliderFloat("Light Z", &light_z, -100.f, 100.f, "%.3f", 1.0f);

        ImGui::SliderFloat("Light R", &light_r, 0.f, 1.f, "%.3f", 1.0f);
        ImGui::SliderFloat("Light G", &light_g, 0.f, 1.f, "%.3f", 1.0f);
        ImGui::SliderFloat("Light B", &light_b, 0.f, 1.f, "%.3f", 1.0f);

        ImGui::SliderFloat("Clip Height", &clip_height, -10.f, 10.f, "%.2f", 1.0f);
        ImGui::SliderFloat("Pillar Y", &pillar_y, -10.f, 10.f, "%.2f", 1.0f);


        ImGui::End();
    }

    ImGui::Render();

}


}
}