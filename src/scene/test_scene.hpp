#pragma once
#include <vector>

#include "scene.h"

#include "systems/render/shader_program.hpp"
#include "systems/render/renderer.hpp"
#include "systems/render/light.hpp"

#include "geometry/geometry.hpp"
#include "geometry/geometry_factory.hpp"

#include "camera/free_camera.h"

namespace Engine
{
namespace Scene
{

class TestScene : public IScene 
{
public:
    TestScene();
    ~TestScene();

    virtual void init() override;
    virtual void update(double delta_time) override;
    virtual void process_input() override;
    virtual void render(double elapsed_time) override;

    ICamera* m_camera;
    std::vector<Geometry> m_geometries;
    std::vector<DirectionalLight> m_lights;
    ShaderProgram* m_shader_program;

    const int MAX_GEOMETRIES = 100;
    const int MAX_LIGHT_SOURCES = 20;
};

}

}