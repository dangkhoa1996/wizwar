#pragma once
#include "scene.h"
#include "json.hpp"

#include "geometry/geometry.hpp"
#include "geometry/geometry_factory.hpp"

#include "camera/abstract_camera.h"
#include "camera/target_camera.h"
#include "camera/free_camera.h"

#include "systems/render/renderer.hpp"
#include "systems/render/light.hpp"


namespace Engine
{
namespace Scene
{

class GraphScene : public IScene
{
public:
    GraphScene();
    ~GraphScene();

    virtual void init() override;
    virtual void update(double delta_time) override;
    virtual void process_input() override;
    virtual void render(double delta_time) override;

    std::vector<Geometry> geometries;
    ICamera* m_global_camera;

    /*
        each scene will have one global camera

    */
};


} // namespace Scene
    
} // namespace Engine
