#include "graph_scene.hpp"
#include <fstream>
#include <iostream>
#include <vector>

using json = nlohmann::json;

namespace Engine
{

namespace Scene
{
    
GraphScene::GraphScene()
{

}

GraphScene::~GraphScene()
{

}

void GraphScene::init()
{
    std::ifstream scene_file("../scene/test_scene.json");
    json scene_json;
    scene_file >> scene_json;
    auto scene_objects = scene_json["object"];
    json::iterator it = scene_objects.begin();
    
    // could be turned into factory class
    for (it; it != scene_objects.end(); it++) 
    {
        Geometry geom;
        if (it->at("type") == "GEOM") {
            auto shape_type = it->at("shape");
            if (shape_type == "CUBE") {
                geom = GeometryFactory::get_instance().create_cube();
            }
            else if (shape_type == "CONE") {
                geom = GeometryFactory::get_instance().create_cone();
            }
            else if (shape_type == "CYLINDER") {
                geom = GeometryFactory::get_instance().create_cylinder();
            }
            else if (shape_type == "ROUND_PLANE") {
                geom = GeometryFactory::get_instance().create_round_plane();
            }
        }
        // position
        // glm::vec3 pos;
        // glm::vec3 scale;
        // glm::quat rotation;
        auto json_pos = it->at("position");
        glm::vec3 pos(json_pos[0], json_pos[1], json_pos[2]);
        // scale
        auto json_scale = it->at("scale");
        glm::vec3 scale(json_scale[0], json_scale[1], json_scale[2]);
        // rotation
        auto json_rotation = it->at("rotation");
        glm::quat rotation(json_rotation[3], json_rotation[0], json_rotation[1], json_rotation[2]);
        
        Transform trans(pos, scale, rotation);
        geom.set_transform(trans);
        
        auto json_diffuse = it->find("diffuse");
        if (json_diffuse != it->end()) {
            glm::vec3 diffuse(json_diffuse->at(0), json_diffuse->at(1), json_diffuse->at(2));
            Material mat(diffuse);
            geom.set_material(mat);
        }

        geometries.push_back(geom);
    }
    // pass the geom to the renderer
    m_global_camera = new FreeCamera();
    Renderer::get_instance().set_camera(m_global_camera);
}

void GraphScene::process_input()
{
    m_global_camera->move();
}

void GraphScene::render(double delta_time)
{
    Renderer::get_instance().draw(geometries, delta_time);
}

void GraphScene::update(double delta_time)
{
    m_global_camera->update(delta_time);
}

}

}