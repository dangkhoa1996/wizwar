#pragma once
#include <vector>
#include "scene.h"

#include "systems/render/shader_program.hpp"
#include "systems/render/light.hpp"

#include "geometry/geometry.hpp"
#include "geometry/geometry_factory.hpp"

#include "camera/free_camera.h"

#include "systems/render/wave/wave_system.hpp"
#include "systems/render/pv_uniform_buffer.hpp"

#include "imgui.h"

namespace Engine
{
namespace Scene
{

class WaveScene : public IScene 
{
public:
    WaveScene();
    ~WaveScene();

    virtual void init() override;
    virtual void update(double delta_time) override;
    virtual void process_input() override;
    virtual void render(double delta_time) override;
    virtual void set_camera_width_and_height(double width, double height) override;

    void draw_config_ui();

    void init_textures();
    void init_renderbuffer();
    void init_framebuffers();

    ICamera* m_global_camera;
    // std::vector<Geometry> m_geometries;

    Geometry m_plane;
    Geometry m_refrac_ui_plane;
    Geometry m_reflect_ui_plane;
    Geometry m_land;
    Geometry m_pillar;

    std::vector<DirectionalLight> m_lights;
    PV_UniformBuffer* m_pv_uniform_buffer;

    GLuint m_grid_texture;
    // GLuint m_pillar_texture;
    GLuint m_refraction_texture;
    GLuint m_refraction_framebuffer;
    GLuint m_refraction_renderbuffer;

    GLuint m_reflection_texture;
    GLuint m_reflection_framebuffer;
    GLuint m_reflection_renderbuffer;

    WaveSystem* m_wave_system;
    ShaderProgram* sp_plane;
    ShaderProgram* m_land_shader_program;
    ShaderProgram* m_pillar_shader_program;

    const int MAX_GEOMETRIES = 100;
    const int MAX_LIGHT_SOURCES = 20;
    const float PI = 3.1415927f;

    glm::vec4 m_clip_plane_refract;
    glm::vec4 m_clip_plane_reflect;
    float distance;
private:
    static float clip_height;
    static float pillar_y;
    static float light_x;
    static float light_y;
    static float light_z;

    static float light_r;
    static float light_g;
    static float light_b;

    static float dir_1;
    static float dir_2;
    static float dir_3;

    static float amp_1;
    static float amp_2;
    static float amp_3;

    static float len_1;
    static float len_2;
    static float len_3;

    static float speed_1;
    static float speed_2;
    static float speed_3;

    static float tex_dir_1;
    static float tex_dir_2;
    static float tex_dir_3;
    static float tex_dir_4;
    static float tex_dir_5;

    static float tex_amp_1;
    static float tex_amp_2;
    static float tex_amp_3;
    static float tex_amp_4;
    static float tex_amp_5;

    static float tex_len_1;
    static float tex_len_2;
    static float tex_len_3;
    static float tex_len_4;
    static float tex_len_5;

    static float tex_speed_1;
    static float tex_speed_2;
    static float tex_speed_3;
    static float tex_speed_4;
    static float tex_speed_5;
    // static float dir_2;
};

}

}