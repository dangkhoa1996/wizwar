#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "../Camera/abstract_camera.h"

namespace Engine 
{

namespace Scene
{

class IScene
{
public:
    IScene() {};
    ~IScene() {};

    virtual void init() {}
    virtual void update(double elapsed_time) {}
    virtual void process_input() {}
    virtual void render(double elapsed_time) {}
    virtual void pre_render() {};
    virtual void set_camera_width_and_height(double width, double height) {};
};

} // namespace Scene

} // namespace Engine
