#pragma once
#include <glm/glm.hpp>

#include "scene.h"
#include "json.hpp"

#include "geometry/geometry.hpp"
#include "geometry/geometry_factory.hpp"

#include "camera/abstract_camera.h"
#include "camera/target_camera.h"
#include "camera/free_camera.h"

#include "systems/render/light.hpp"
#include "systems/render/shader_program.hpp"
#include "systems/render/pv_uniform_buffer.hpp"
#include "systems/render/renderer.hpp"



#include "tiny_gltf.h"

namespace Engine
{
namespace Scene
{

class ModelScene : public IScene
{
public:
    ModelScene();
    ~ModelScene();

    virtual void init() override;
    virtual void update(double delta_time) override;
    virtual void process_input() override;
    virtual void render(double delta_time) override;

    bool load_model(const char* filename);
    void bind_mesh(tinygltf::Model& model, int mesh_index);
    void bind_model_nodes(tinygltf::Model& model, tinygltf::Node& node);
    void bind_model(tinygltf::Model &model);

    void draw_mesh(tinygltf::Model& model, int mesh_index, glm::mat4 parent_transform);
    void draw_model_nodes(tinygltf::Model& model, tinygltf::Node& node, glm::mat4 parent_transform);
    void draw_model(tinygltf::Model& model);


    void debug_model(tinygltf::Model& model);
    void init_shader();

    glm::mat4 m_model_mat;
    std::map<int, GLuint> m_tex_id;
    std::map<int, GLuint> m_vaos;
    std::map<int, GLuint> m_vbos;
    std::map<int, GLuint> m_ebos;

    tinygltf::Model m_model;
    ShaderProgram* m_shader_program;
    PV_UniformBuffer* m_pv_uniform_buffer;
    ICamera* m_current_camera;

    DirectionalLight m_dir_light;

    std::vector<Geometry> geometries;
    ICamera* m_global_camera;
};
} // namespace Scene  
} // namespace Engine
