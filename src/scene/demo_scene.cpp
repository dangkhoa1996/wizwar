/*
#include "demo_scene.hpp"
#include <iostream>
#include <random>
#include <sstream>
#include <glm/gtx/string_cast.hpp>

#define PI 3.14159265

namespace Engine
{


namespace Scene
{

DemoScene::DemoScene() : light_arr_size(10)
{
    m_directional_light_list.reserve(light_arr_size);
    m_target_camera = new TargetCamera();
    m_pv_uniform_buffer = new PV_UniformBuffer();
}

DemoScene::~DemoScene()
{
    // delete m_model;
    delete m_plane;
    delete m_cube;
    delete m_shadow;
    delete m_character;
    delete m_character_movement_controller;
    delete m_debug_line;

    shutdown();
}

void DemoScene::init()
{
    init_physic();

    // create uniform buffer
    m_pv_uniform_buffer->create_uniform_buffer();
   
    basic_shader = new ShaderProgram(
        Shader { ShaderType::VERTEX, "../shader/vertex/basic_shape.vs" },
        Shader { ShaderType::FRAGMENT, "../shader/fragment/basic_shape.fs" } 
    );

    basic_shader->attach();
    basic_shader->bind_attrib_location(0, "a_pos");
    basic_shader->bind_attrib_location(1, "a_normal");
    basic_shader->bind_attrib_location(2, "a_texcoord");
    basic_shader->link();

    m_pv_uniform_buffer->bind_buffer_to_shader_program(basic_shader->m_program, "pv_mat");


    init_light(light_shader);
    pass_directional_light_to_shader(basic_shader);
    // pass_material_to_shader(basic_shader);
 
    m_specular_cubemap_id = texture_library.add_cube_map("skybox",
        std::vector<std::string>{ 
            "../img/skybox/hip_miramar/miramar_lf.tga",
            "../img/skybox/hip_miramar/miramar_rt.tga",
            "../img/skybox/hip_miramar/miramar_up.tga",
            "../img/skybox/hip_miramar/miramar_dn.tga",
            "../img/skybox/hip_miramar/miramar_ft.tga",
            "../img/skybox/hip_miramar/miramar_bk.tga",
        });
    m_diffuse_cubemap_id = texture_library.add_cube_map("sor",
        std::vector<std::string>{ 
            "../img/skybox/sor_cwd/cwd_lf.jpg",
            "../img/skybox/sor_cwd/cwd_rt.jpg",
            "../img/skybox/sor_cwd/cwd_up.jpg",
            "../img/skybox/sor_cwd/cwd_dn.jpg",
            "../img/skybox/sor_cwd/cwd_ft.jpg",
            "../img/skybox/sor_cwd/cwd_bk.jpg",
        });

    m_skybox = new Skybox();
    m_skybox->use_cube_map("sor", texture_library);

    m_shadow = new Shadow();

    basic_shader->use();
    basic_shader->set_mat4("u_shadow_mat", m_shadow->shadow_matrix);
    basic_shader->set_texture("depth_texture", 3);
    basic_shader->un_use();
    basic_shader->set_texture("u_tex", 1);
    basic_shader->set_bool("has_texture", true);
    basic_shader->set_bool("has_light", true);
    basic_shader->un_use();

    m_character = new AnimationModel();
    m_character->load("../models/chibi_anim.fbx", texture_library);
    auto character_mesh_shader_program = m_character->get_mesh_shader_program();

    character_mesh_shader_program->use();
    character_mesh_shader_program->set_mat4("u_shadow_mat", m_shadow->shadow_matrix);
    character_mesh_shader_program->set_texture("depth_texture", 3);
    character_mesh_shader_program->un_use();

    pass_directional_light_to_shader(character_mesh_shader_program);

    auto character_shadow_shader_program = m_character->get_shadow_shader_program();
    character_shadow_shader_program->use();
    character_shadow_shader_program->set_mat4("u_vp_mat", m_shadow->vp);
    character_shadow_shader_program->un_use();
    m_pv_uniform_buffer->bind_buffer_to_shader_program(m_character->get_mesh_shader_program()->m_program, "pv_mat");

    m_character_movement_controller = new CharacterMovementController(m_character);

    m_plane = new Plane();
    m_plane->init(basic_shader);
    m_plane->translate(0.0f, -5.5f, 0.0f);
    m_plane->scale(50.0f, 1.0f, 200.0f);
    m_plane->rotate(90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
    m_plane->m_material.diffuse = glm::vec3(1.0, 0.0, 0.0);
    m_plane->m_material.specular = glm::vec3(0.3f);

    
    m_cube = new Cube();
    m_cube->init(basic_shader);
    m_cube->translate(10.0f, 40.0f, -20.0f);
    m_cube->scale(2.0f, 2.0f, 2.0f);
    m_cube->m_material.diffuse = glm::vec3(0.5f, 0.77f, 0.0f);
    m_cube->m_material.specular = glm::vec3(0.3f);

    m_wall = new Cube();
    m_wall->init(basic_shader);
    m_wall->translate(0.0f, 0.0f, 10.0f);
    m_wall->scale(50.0f, 10.0f, 10.0f);
    m_wall->m_material.diffuse = glm::vec3(0.4f, 0.0f, 0.65f);
    m_wall->m_material.specular = glm::vec3(0.3f);

    m_assist_cube = new Cube();
    m_assist_cube->init(basic_shader);
    m_assist_cube->translate(0.0f, 0.0f, -3.0f);
    m_assist_cube->scale(5.0f, 5.0f, 5.0f);
    m_assist_cube->m_material.diffuse = glm::vec3(0.2, 0.8, 0.4);
    m_assist_cube->m_material.specular = glm::vec3(0.3f);

    m_parent_cube = new Cube();
    m_parent_cube->init(basic_shader);
    m_parent_cube->scale(10.0f, 10.0f, 10.0f);
    m_parent_cube->m_material.diffuse = glm::vec3(0.6, 0.1, 0.8);
    m_parent_cube->m_material.specular = glm::vec3(0.4f);

    m_cone = new Cone();
    m_cone->init(basic_shader);
    m_cone->translate(30.0, 0.0, 0.0);
    m_cone->scale(20.0, 30.0, 20.0);
    m_cone->m_color = glm::vec3(0.3, 0.67, 0.2);

    m_cylinder = new Cylinder();
    m_cylinder->init(basic_shader);
    m_cylinder->translate(-10.0, 0.0, 0.0);
    m_cylinder->scale(10.0, 20.0, 10.0);
    m_cylinder->m_color = glm::vec3(0.1, 0.4, 0.8);

    std::cout << "cone pos: " << glm::to_string(m_cone->m_position) << '\n'
                << "cylinder pos: " << glm::to_string(m_cylinder->m_position) << '\n';
}

void DemoScene::init_light(ShaderProgram* shader_program)
{
    m_directional_light_list.emplace_back(DirectionalLight {
        glm::vec3(0.2f),
        glm::vec3(0.8f),
        glm::vec3(-100.0f, -200.0f, -100.0f),
        true
    });
}

void DemoScene::init_physic()
{
    init_pxphysic();
    PxShape* shape = g_physics->createShape(PxBoxGeometry(1.0, 1.0, 1.0), *g_material);

    PxTransform local_transform{PxVec3(10.0f, 40.0f, -20.0f)};
    PxRigidDynamic* body = g_physics->createRigidDynamic(local_transform);
    body->attachShape(*shape);
    PxRigidBodyExt::updateMassAndInertia(*body, 1.0f);
    g_scene->addActor(*body);

    PxRigidStatic* wall = g_physics->createRigidStatic(PxTransform(PxVec3(0.0, 0.0, 10.0)));
    PxShape* wall_shape = g_physics->createShape(PxBoxGeometry(50.0f, 10.0f, 10.0f), *g_material);
    wall_shape->setLocalPose(PxTransform(PxVec3(0.0, 0.0, 0.0)));
    wall->attachShape(*wall_shape);
    g_scene->addActor(*wall);

    wall_shape->release();
    shape->release();

    PxRigidStatic* ground = PxCreatePlane(*g_physics, PxPlane(0, 1, 0, 4.5), *g_material);
    g_scene->addActor(*ground);
}

void DemoScene::pass_directional_light_to_shader(ShaderProgram* shader_program)
{
    shader_program->use();
    shader_program->set_int("directional_lights_size", m_directional_light_list.size());
    shader_program->un_use();

    for (size_t i = 0; i < m_directional_light_list.size(); i++) {
        m_directional_light_list[i].pass_to_shader(shader_program, i);
    }
}

void DemoScene::process_input(GLFWwindow*& window, double elapsed_time) 
{
    m_character_movement_controller->process_input(window, m_target_camera, elapsed_time);
}

void DemoScene::update(double delta_time)
{
    m_character->m_base_model->set_transform();
    m_character->update(delta_time);

    auto base_model = m_character->m_base_model;
    m_target_camera->m_parent_model = base_model->m_model_transform * glm::inverse(base_model->m_rotate_mat);
    m_target_camera->update(delta_time);
    
    
    m_parent_cube->rotate(2.0f, glm::vec3(0.0f, 1.0f, 0.0f));
    m_parent_cube->translate(0.0f, 0.0f, 0.01f);
    m_parent_cube->m_model = m_parent_cube->m_scale_mat * m_parent_cube->m_translate_mat * 
            glm::inverse(m_parent_cube->m_rotate_mat);
    

    m_assist_cube->m_model = glm::mat4(1.0f);
    m_assist_cube->scale(5.0f, 5.0f, 5.0f);
    m_assist_cube->translate(0.0f, 2.0f, -5.0f);

    m_assist_cube->m_model = base_model->m_model_transform * m_assist_cube->m_model;
    
    m_cube->update(delta_time);

    step_physics();

    PxU32 num_actors = g_scene->getNbActors(PxActorTypeFlag::eRIGID_DYNAMIC | PxActorTypeFlag::eRIGID_STATIC );
    std::vector<PxRigidActor*> actors(num_actors);
    if (num_actors) {
        g_scene->getActors(PxActorTypeFlag::eRIGID_DYNAMIC | PxActorTypeFlag::eRIGID_STATIC, 
                reinterpret_cast<PxActor**>(&actors[0]), num_actors);
    }

    PxShape* shapes[128];
    const PxU32 num_shapes = actors[0]->getNbShapes();
    actors[0]->getShapes(shapes, num_shapes);
    const bool sleeping = actors[0]->is<PxRigidDynamic>() ? actors[0]->is<PxRigidDynamic>()->isSleeping() : false;

    const PxMat44 shapePose(PxShapeExt::getGlobalPose(*shapes[0], *actors[0]));
    PxVec4 col0 = shapePose.column0;
    PxVec4 col1 = shapePose.column1;
    PxVec4 col2 = shapePose.column2;
    PxVec4 col3 = shapePose.column3;
    m_cube->m_model = glm::mat4(col0.x, col0.y, col0.z, col0.w,
                                col1.x, col1.y, col1.z, col1.w,
                                col2.x, col2.y, col2.z, col2.w,
                                col3.x, col3.y, col3.z, col3.w);
    m_pv_uniform_buffer->set_buffer_data(m_target_camera->m_projection, m_target_camera->m_view);

}

void DemoScene::draw(double elapsed_time)
{
    // m_debug_line->draw(*m_target_camera);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    // render depth map
    //draw scene
    m_shadow->m_shader_program->use();
    glBindFramebuffer(GL_FRAMEBUFFER, m_shadow->depth_fbo);
    glViewport(0, 0, m_shadow->DEPTH_TEXTURE_WIDTH, m_shadow->DEPTH_TEXTURE_HEIGHT);
    glClearDepth(1.0f);
    glClear(GL_DEPTH_BUFFER_BIT);

    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonOffset(2.0f, 4.0f);

    m_plane->draw(m_shadow->m_shader_program);
    m_cube->draw(m_shadow->m_shader_program);
    m_wall->draw(m_shadow->m_shader_program);
    m_assist_cube->draw(m_shadow->m_shader_program);

    m_character->draw_shadow(*m_target_camera);
    
    m_shadow->m_shader_program->un_use();

    glDisable(GL_POLYGON_OFFSET_FILL);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    glViewport(0, 0, m_target_camera->m_screen_width, m_target_camera->m_screen_height);
    
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, m_shadow->depth_texture);

    basic_shader->use();
    basic_shader->set_texture("depth_texture", 3);
    basic_shader->set_vec3("u_camera_position", m_target_camera->m_position);
    glDepthFunc(GL_LEQUAL);

    m_character->m_base_model->m_mesh_shader_prog->use();
    m_character->m_base_model->m_mesh_shader_prog->set_texture("depth_texture", 3);


    m_character->draw(*m_target_camera);

    m_cube->pass_material_to_shader(basic_shader);
    m_cube->draw(basic_shader);
    
    // m_wall->pass_material_to_shader(basic_shader);
    // m_wall->draw(basic_shader);

    m_assist_cube->pass_material_to_shader(basic_shader);
    m_assist_cube->draw(basic_shader);

    m_cone->draw(basic_shader);
    m_cylinder->draw(basic_shader);
}

} // namespace Scene

} // namespace Engine

*/