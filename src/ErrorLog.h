#pragma once
#include <GL/glew.h>
#include <string>

namespace Engine
{
    class ErrorLog
    {
    public:
        ErrorLog();
        ~ErrorLog();

        static ErrorLog& getInstance()
        {
            if (!p_Instance)
            {
                p_Instance = new ErrorLog();
            }
            return *p_Instance;
        }

        void findError(const std::string& where, int line);

    private:
        static ErrorLog* p_Instance;
    };
}