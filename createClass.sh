echo "I was thinking of something cool here !" 
if [ -z "$1" ]; then
    echo "empty class name\n"
else 
    cd ./include
    echo "create header file $1.h\n"
    touch "$1.h"
    cd ../src
    echo "create src file $1.cpp\n"
    touch "$1.cpp"
fi
